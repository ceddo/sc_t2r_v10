/*
 * Copyright 2017-2018 Sony Semiconductor Solutions Corporation.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
 * Solutions Corporation.
 * No part of this file may be copied, modified, sold, and distributed in any
 * form or by any means without prior explicit permission in writing from
 * Sony Semiconductor Solutions Corporation.
 *
 */
#ifndef OUTPUT_TENSOR_PARSER_H_
#define OUTPUT_TENSOR_PARSER_H_

#include <stdint.h>
#include <vector>
#include<tuple>
#include "utils.h"
#include "classification_interface.h"
#include "objectdetection_interface.h"
#define bytes_to_u16(MSB,LSB) (((unsigned int) ((unsigned char) MSB)) & 255)<<8 | (((unsigned char) LSB)&255)
#define extract_byte(MSB,LSB) (((((unsigned char) MSB)<<6) & 0xc0) | ((((unsigned char) LSB) & 0xfc) >> 2))

namespace imx500 {
	/**
	 * Tensor Parser Namespace
	 */
	namespace output_tensor_parser {

		struct InputDataInfo {
			uint8_t* addr;
			uint16_t lineNum;
			uint16_t  pitch;
			size_t size;
		};

		struct OutputTensorInfo {
			float* address;
			size_t totalSize;
			uint32_t tensorNum;
			uint32_t* tensorDataNum;
		};

		struct Dimensions {
			uint16_t size;
			uint8_t serializationIndex;
			uint8_t padding;
		};

		struct OutputTensorApParams {
			uint8_t id;
			char* name;
			uint16_t numOfDimensions;
			uint8_t bitsPerElement;
			std::vector<Dimensions> vecDim;
			uint16_t shift;
			float scale;
			uint8_t format;
		};

		struct FrameOutputInfo {
			utils::DnnHeader* dnnHeaderInfo = NULL;
			std::string networkType;
			std::vector<OutputTensorApParams>* outputApParams = NULL;
			OutputTensorInfo* outputBodyInfo = NULL;
		};

		int32_t parseOutputTensor(const struct InputDataInfo* inputDataInfo, struct FrameOutputInfo* frameDataInfo);
		int32_t cleanupFrameData(FrameOutputInfo* frameOutputTensorInfo);
		int32_t createClassificationData(FrameOutputInfo* frameOutputInfo, classification_interface::ClassificationOutputTensor* classificationOutput);
		int32_t createObjectDetectionData(FrameOutputInfo* frameOutputInfo, objectdetection_interface::ObjectDetectionOutputTensor* objectDetectionOutput);

	} // namespace output_tensor_parser
} // namespace imx500
#endif  // TENSOR_PARSER_H_
