/*
* Copyright 2017-2018 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/

#include <stdio.h>
#include <stdint.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include "common_define.h"
#include "i2c_access.h"

#include <sys/ioctl.h>
#include <linux/types.h>

#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

#include <time.h>
#include "image_loader.h"
#include "imx500lib_sample.h"
#include "dbg_log.h"


/* SPI Transfer Measurement Enable / Disable */
#define IMX500SF_MEASUREMENT_ENABLE                          (0)

/* SPI Transfer Debug */
//define IMX500SF_SPI_DEBUG

/* IMX 500 Register & Setting Param */
#define IMX500SF_REG_ADDR_DD_ST_TRANS_CMD                   (0xD000)
#define IMX500SF_REG_SIZE_DD_ST_TRANS_CMD                   (1)
#define IMX500SF_REG_LOADER_LOAD_ST_TRANS_CMD               (0x00)
#define IMX500SF_REG_MAINFW_LOAD_ST_TRANS_CMD               (0x01)
#define IMX500SF_REG_WO_NW_TO_W_NW_ST_TRANS_CMD             (0x02)
#define IMX500SF_REG_W_NW_TO_WO_NW_ST_TRANS_CMD             (0x03)

#define IMX500SF_REG_ADDR_DD_UPDATE_CMD                     (0xD001)
#define IMX500SF_REG_SIZE_DD_UPDATE_CMD                     (1)
#define IMX500SF_REG_DD_UPDATE_CMD_UPDATE_L2MEM             (0x00)
#define IMX500SF_REG_DD_UPDATE_CMD_UPDATE_FLASH             (0x01)

#define IMX500SF_REG_ADDR_DD_LOAD_MODE                   (0xD002)
#define IMX500SF_REG_SIZE_DD_LOAD_MODE                   (1)
#define IMX500SF_REG_FROMAP_DD_LOAD_MODE                 (0x00)
#define IMX500SF_REG_FROMFLASH_DD_LOAD_MODE              (0x01)

#define IMX500SF_REG_ADDR_DD_IMAGE_TYPE                  (0xD003)
#define IMX500SF_REG_SIZE_DD_IMAGE_TYPE                  (1)
#define IMX500SF_REG_LOADER_DD_IMAGE_TYPE                 (0x00)
#define IMX500SF_REG_MAIN_DD_IMAGE_TYPE                   (0x01)
#define IMX500SF_REG_NW_DD_IMAGE_TYPE                     (0x02)

#define IMX500SF_REG_ADDR_DD_DOWNLOAD_DIV_NUM            (0xD004)
#define IMX500SF_REG_SIZE_DD_DOWNLOAD_DIV_NUM            (1)

#define IMX500SF_REG_ADDR_DD_DOWNLOAD_FILE_SIZE          (0xD008)
#define IMX500SF_REG_SIZE_DD_DOWNLOAD_FILE_SIZE          (4)

#define IMX500SF_REG_ADDR_DD_UPDATE_CMD                  (0xD001)
#define IMX500SF_REG_SIZE_DD_UPDATE_CMD                  (1)

#define IMX500SF_REG_ADDR_DD_FLASH_ADDR                  (0xD00C)
#define IMX500SF_REG_SIZE_DD_FLASH_ADDR                  (4)

#define IMX500SF_REG_ADDR_DD_CMD_INT                     (0x3080)
#define IMX500SF_REG_SIZE_DD_CMD_INT                     (1)
#define IMX500SF_REG_TRANS_CMD_DD_CMD_INT                (0)
#define IMX500SF_REG_UPDATE_CMD_DD_CMD_INT               (1)

#define IMX500SF_REG_ADDR_DD_CMD_REPLY_STS               (0xD014)
#define IMX500SF_REG_SIZE_DD_CMD_REPLY_STS               (1)
#define IMX500SF_REG_DD_CMD_RPLY_STS_TRNS_READY          (0x00)
#define IMX500SF_REG_DD_CMD_RPLY_STS_TRNS_DONE           (0x01)
#define IMX500SF_REG_DD_CMD_RPLY_STS_UPDATE_READY        (0x10)
#define IMX500SF_REG_DD_CMD_RPLY_STS_UPDATE_DONE         (0x11)

#define IMX500SF_REG_ADDR_DD_DOWNLOAD_STS               (0xD015)
#define IMX500SF_REG_SIZE_DD_DOWNLOAD_STS               (1)
#define IMX500SF_REG_READY_DD_DOWNLOAD_STS              (0x00)
#define IMX500SF_REG_DOWNLOADING_DD_DOWNLOAD_STS        (0x01)

#define IMX500SF_REG_ADDR_DD_REF_STS_REG                 (0xD010)
#define IMX500SF_REG_SIZE_DD_REF_STS_REG                 (1)

#define IMX500SF_REG_ADDR_DD_SYS_STATE                   (0xD02A)
#define IMX500SF_REG_SIZE_DD_SYS_STATE                   (1)
#define IMX500SF_REG_DD_SYS_STATE_STANDBY_WONET          (0)
#define IMX500SF_REG_DD_SYS_STATE_STANDBY_WNET           (2)


#define IMX500SF_POLLING_TIMEOUT        (0xFFFFFFFF)
#define IMX500SF_NW_TRANS_SIZE_UNIT     (1024*1024)

/* SPI Master Driver Configuration Parameter */
#define IMX500SF_SPI_DRV_MAX_SPEED  50000000
#define IMX500SF_SPI_DRV_MODE       SPI_MODE_3
#define IMX500SF_SPI_DRV_TRANS_BIT_WIDTH    8
#define IMX500SF_SPI_DRV_DELAY              0
#define IMX500SF_SPI_CS_CHANGE              0

//#define IMX500SF_SPI_ONE_TRANS_SIZE 32768
#define IMX500SF_SPI_ONE_TRANS_SIZE 4096
#define IMG_LDR_SPI_DEVICE_NAME_LEN 15      /* "/dev/spidev0.0" + NULL */
#define IMG_LDR_I2C_DEVICE_NAME_LEN 11      /* "/dev/i2c-0" + NULL */

extern option_table table[];

namespace imx500 {
    namespace control {
        namespace imageloader {
            typedef struct tagIMX500SF_MODULE_INFO {
                int fd;
                int debug;
            } IMX500SF_MODULE_INFO;
            static const char *device = "/dev/spidev0.0";
            static IMX500SF_MODULE_INFO  moduleInfo;

            static E_RESULT_CODE IMX500SF_SpiBoot_ProcFW(int fd, uint32_t imageType,uint32_t transCmd, uint8_t *pTransbuf,uint32_t size);
            static E_RESULT_CODE IMX500SF_SpiBoot_ProcNW(int fd, uint8_t *pTransbuf,uint32_t size);
            static E_RESULT_CODE IMX500SF_TransferSpi(int fd, uint8_t *pTransbuf,uint32_t transSize);
            static E_RESULT_CODE Imx500SF_FlashBoot_Proc(uint32_t imageType,uint32_t transCmd, uint32_t flashAddr);
            static E_RESULT_CODE IMX500SF_ChangeStandbyWtoStandbyWo(void);
            static E_RESULT_CODE IMX500SF_FlashUpdate_ProcFW(int fd,uint32_t imageType,uint32_t flashAddr,uint8_t *pTransbuf,uint32_t size);
            static E_RESULT_CODE IMX500SF_FlashUpdate_ProcNW(int fd,uint32_t flashAddr,uint8_t *pTransbuf,uint32_t size);
            /**
             * @brief open.
             */
            
            E_RESULT_CODE Imx500SF_Open(IMX500SF_ModuleHandle *pHandle)
            {
                uint8_t mode = IMX500SF_SPI_DRV_MODE;
                uint8_t bits = IMX500SF_SPI_DRV_TRANS_BIT_WIDTH;
                int32_t ioctrl_ret = 0;
                uint32_t speed =  IMX500SF_SPI_DRV_MAX_SPEED;
                int fd;
                char spi_device_name[IMG_LDR_SPI_DEVICE_NAME_LEN];

                *pHandle = (void *)&moduleInfo;
                moduleInfo.debug = 1;
                snprintf(spi_device_name, sizeof(spi_device_name), "%s", device);
                fd = open(spi_device_name, O_RDWR);
                if (fd < 0) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] SPIDEV OPEN ERROR !!! \n");
                    return E_OTHER;
                }
                moduleInfo.fd = fd;
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] SPI Device %x\n", moduleInfo.fd);

                /* spi mode */
                ioctrl_ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
                if (ioctrl_ret == -1) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] SPI_IOC_WR_MODE ERROR !!! \n");
                    close(fd);
                    return E_OTHER;
                }
                #ifdef IMX500SF_SPI_DEBUG
                ioctrl_ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
                if (ioctrl_ret == -1){
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] SPI_IOC_RD_MODE ERROR !!! \n");
                    close(fd);
                    return E_OTHER;
                }
                else {
                    DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][IMAGE_LOADER] SPI_IOC_RD_MODE::mode ::%x\n", mode);
                }
                #endif
                /* bits per word */
                ioctrl_ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
                if (ioctrl_ret == -1){
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] SPI_IOC_WR_BITS_PER_WORD ERROR !!!");
                    close(fd);
                    return E_OTHER;
                }
                #ifdef IMX500SF_SPI_DEBUG
                ioctrl_ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
                if (ioctrl_ret == -1) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] SPI_IOC_RD_BITS_PER_WORD ERROR !!!");
                    close(fd);
                    return E_OTHER;
                }
                else {
                    DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][IMAGE_LOADER] SPI_IOC_RD_BITS_PER_WORD::bits ::%x\n", bits);
                }
                #endif

                /* max speed hz */
                ioctrl_ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
                if (ioctrl_ret == -1) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] SPI_IOC_WR_MAX_SPEED_HZ ERROR !!! \n");
                    close(fd);
                    return E_OTHER;
                }

                #ifdef IMX500SF_SPI_DEBUG
                ioctrl_ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
                if (ioctrl_ret == -1) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] SPI_IOC_RD_MAX_SPEED_HZ ERROR !!! \n");
                    close(fd);
                }
                else {
                    DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][IMAGE_LOADER] MAX_SPEED_HZ: %d Hz (%d KHz)\n", speed, speed/1000);
                }
                #endif
                return E_OK;
            }
        
            E_RESULT_CODE Imx500SF_Close(IMX500SF_ModuleHandle pHandle)
            {
                IMX500SF_MODULE_INFO *pModuleInfo;
                pModuleInfo = (IMX500SF_MODULE_INFO *)pHandle;
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] SPI Device %x\n", pModuleInfo->fd);
                close(pModuleInfo->fd);
                return E_OK;
            }

            E_RESULT_CODE Imx500SF_SpiBoot_LoaderFW(IMX500SF_ModuleHandle pHandle,
                                                                uint8_t *pLoaderFW,
                                                                uint32_t size)
            {
                E_RESULT_CODE ret = E_OK;
                IMX500SF_MODULE_INFO *pModuleInfo;
                pModuleInfo = (IMX500SF_MODULE_INFO *)pHandle;
                
                DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][IMAGE_LOADER] Imx500SF_SpiBoot_LoaderFW size %d\n",size);

                ret = IMX500SF_SpiBoot_ProcFW(pModuleInfo->fd,IMX500SF_REG_LOADER_DD_IMAGE_TYPE,IMX500SF_REG_LOADER_LOAD_ST_TRANS_CMD,pLoaderFW,size);
                if (ret != E_OK) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] Imx500SF_SpiBoot_LoaderFW ERROR !!! \n");
                }            
                return ret;
            }

            E_RESULT_CODE Imx500SF_SpiBoot_MainFW(IMX500SF_ModuleHandle	pHandle,	
                                                        uint8_t *pMainFw,
                                                        uint32_t size)
            {
                E_RESULT_CODE ret = E_OK;
                IMX500SF_MODULE_INFO *pModuleInfo;
                pModuleInfo = (IMX500SF_MODULE_INFO *)pHandle;

                DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][IMAGE_LOADER] Imx500SF_SpiBoot_MainFW size %d\n",size);

                ret = IMX500SF_SpiBoot_ProcFW(pModuleInfo->fd,IMX500SF_REG_MAIN_DD_IMAGE_TYPE,IMX500SF_REG_MAINFW_LOAD_ST_TRANS_CMD,pMainFw,size);
                if (ret != E_OK) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] Imx500SF_SpiBoot_MainFW ERROR !!! \n");
                }            
                return ret;
            }

            E_RESULT_CODE Imx500SF_SpiBoot_NetworkWeights(IMX500SF_ModuleHandle	pHandle,
                                                            uint8_t *pNetworkWeights,
                                                            uint32_t size)
            {
                E_RESULT_CODE ret = E_OK;
                IMX500SF_MODULE_INFO *pModuleInfo;
                pModuleInfo = (IMX500SF_MODULE_INFO *)pHandle;

                DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][IMAGE_LOADER] Imx500SF_SpiBoot_NetworkWeights size %d\n",size);

                ret = IMX500SF_SpiBoot_ProcNW(pModuleInfo->fd,pNetworkWeights,size);
                if (ret != E_OK) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] Imx500SF_SpiBoot_MainFW ERROR !!! \n");
                }            
                return ret;
            }

            static E_RESULT_CODE IMX500SF_SpiBoot_ProcNW(int fd,uint8_t *pTransbuf,uint32_t size)
            {
                E_RESULT_CODE ret = E_OK;
                uint32_t regVal;
                uint8_t  divNum = 0;
                uint32_t timer_cnt = 0;
                uint8_t *pCurBuf = NULL;
                uint32_t residualSize = 0;
                uint32_t transSize = 0;
                uint8_t refStsCnt = 0;
                uint32_t spiTransactionSize = 0;
                uint8_t i= 0;
    #if  IMX500SF_MEASUREMENT_ENABLE
                struct timespec startTime, endTime;
    #endif
                /* current CMD_REPLY_STS_CNT */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_REF_STS_REG, &regVal, IMX500SF_REG_SIZE_DD_REF_STS_REG);
                refStsCnt = static_cast<uint8_t>(regVal);

                /* Determining the number of divisions */
                if ((size % IMX500SF_NW_TRANS_SIZE_UNIT) == 0) {
                    divNum = (size/IMX500SF_NW_TRANS_SIZE_UNIT) - 1;  
                } else {
                    divNum = size/IMX500SF_NW_TRANS_SIZE_UNIT;
                }

                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_LOAD_MODE, IMX500SF_REG_FROMAP_DD_LOAD_MODE, IMX500SF_REG_SIZE_DD_LOAD_MODE);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_IMAGE_TYPE, IMX500SF_REG_NW_DD_IMAGE_TYPE, IMX500SF_REG_SIZE_DD_IMAGE_TYPE);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_DOWNLOAD_DIV_NUM, divNum, IMX500SF_REG_SIZE_DD_DOWNLOAD_DIV_NUM);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_DOWNLOAD_FILE_SIZE, size, IMX500SF_REG_SIZE_DD_DOWNLOAD_FILE_SIZE);

                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_ST_TRANS_CMD, IMX500SF_REG_WO_NW_TO_W_NW_ST_TRANS_CMD, IMX500SF_REG_SIZE_DD_ST_TRANS_CMD);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_CMD_INT, IMX500SF_REG_TRANS_CMD_DD_CMD_INT, IMX500SF_REG_SIZE_DD_CMD_INT);

                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] IMX500SF_SpiBoot_ProcNW refStsCnt %x divNum %x\n",refStsCnt,divNum);
                
                /* wait change status */ /*@ 無限ループ */
                while (timer_cnt < IMX500SF_POLLING_TIMEOUT) {
                    I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_REF_STS_REG, &regVal, IMX500SF_REG_SIZE_DD_REF_STS_REG);
                    if (static_cast<uint8_t>(regVal) != refStsCnt) {
                        break;
                    }
                    timer_cnt++;
                    if (timer_cnt >= IMX500SF_POLLING_TIMEOUT) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] STATE Change ERROR \n");
                        return E_OTHER; 
                    }
                }
                //PRINTF("[IMX500LIB][IMAGE_LOADER] timer_cnt  %x\n", timer_cnt);
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_REF_STS_REG  %02X\n", regVal);
                /* update ref status count */
                refStsCnt = static_cast<uint8_t>(regVal);

                /* check reply status */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_CMD_REPLY_STS, &regVal, IMX500SF_REG_SIZE_DD_CMD_REPLY_STS);
                if (regVal != IMX500SF_REG_DD_CMD_RPLY_STS_TRNS_READY) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] nework boot error(reply status NG): imageType=%d, reply status=0x%02X\n", IMX500SF_REG_NW_DD_IMAGE_TYPE, regVal);
                    return E_OTHER;
                }
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_CMD_REPLY_STS  %02X\n", regVal);
                
                residualSize = size;
                pCurBuf = pTransbuf;

                for (i = 0; i <= divNum; i++) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] %02X divid Trans  residualSize %d\n", i, residualSize);
                    /* Check DOWNLOAD Status */
                    while (1) {
                        I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_DOWNLOAD_STS, &regVal, IMX500SF_REG_SIZE_DD_DOWNLOAD_STS);
                        if (regVal == IMX500SF_REG_READY_DD_DOWNLOAD_STS) {
                            break;
                        }
                    }
                    I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_DOWNLOAD_STS, &regVal, IMX500SF_REG_SIZE_DD_DOWNLOAD_STS);
                    DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_DOWNLOAD_STS  %02X\n", regVal);
                    
                    if (residualSize >= IMX500SF_NW_TRANS_SIZE_UNIT) {
                        spiTransactionSize = IMX500SF_NW_TRANS_SIZE_UNIT;
                    } else {
                        spiTransactionSize = residualSize;
                    }
    #if  IMX500SF_MEASUREMENT_ENABLE
                    clock_gettime(CLOCK_REALTIME, &startTime);
    #endif
                    DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] residualSize:%d   spiTransactionSize:%d\n", residualSize,spiTransactionSize);
                    while (1) {
                        if (spiTransactionSize == 0) {
                            break;
                        }
                        if (spiTransactionSize >= IMX500SF_SPI_ONE_TRANS_SIZE) {
                            transSize = IMX500SF_SPI_ONE_TRANS_SIZE;
                        } else {
                            transSize = spiTransactionSize;
                        }               
                        ret = IMX500SF_TransferSpi(fd,pCurBuf,transSize);
                        if (ret != E_OK) {
                            return ret;
                        }
                        if (transSize >= IMX500SF_SPI_ONE_TRANS_SIZE) {
                            pCurBuf = pCurBuf + IMX500SF_SPI_ONE_TRANS_SIZE;
                            spiTransactionSize = spiTransactionSize - IMX500SF_SPI_ONE_TRANS_SIZE;
                        } else {
                            spiTransactionSize = 0;
                        }               
                    }
                    
                    /* Update Residual Size */
                    if (residualSize >= IMX500SF_NW_TRANS_SIZE_UNIT) {
                        residualSize = residualSize - IMX500SF_NW_TRANS_SIZE_UNIT;
                    } else {
                        residualSize = 0;
                    }
    #if IMX500SF_MEASUREMENT_ENABLE
                    clock_gettime(CLOCK_REALTIME, &endTime);
                    DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "Proc Time = ");
                    if (endTime.tv_nsec < startTime.tv_nsec) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "%10ld.%09ld", endTime.tv_sec - startTime.tv_sec - 1
                                ,endTime.tv_nsec + 1000000000 - startTime.tv_nsec);
                    } else {
                        DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "%10ld.%09ld", endTime.tv_sec - startTime.tv_sec
                                ,endTime.tv_nsec - startTime.tv_nsec);
                    }
                    DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "(second)\n");
    #endif
                }
                /* wait change status */ /*@ 無限ループ */
                while (timer_cnt < IMX500SF_POLLING_TIMEOUT) {
                    I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_REF_STS_REG, &regVal, IMX500SF_REG_SIZE_DD_REF_STS_REG);
                    if (static_cast<uint8_t>(regVal) != refStsCnt) {
                        break;
                    }
                    timer_cnt++;
                    if (timer_cnt >= IMX500SF_POLLING_TIMEOUT) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] STATE Change ERROR \n");
                        return E_OTHER; 
                    }
                }
                //PRINTF("[IMX500LIB][IMAGE_LOADER] timer_cnt  %x\n", timer_cnt);
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_REF_STS_REG  %02X\n", regVal);
                /* check reply status */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_CMD_REPLY_STS, &regVal, IMX500SF_REG_SIZE_DD_CMD_REPLY_STS);
                if (regVal != IMX500SF_REG_DD_CMD_RPLY_STS_TRNS_DONE) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] nework boot error(reply status NG): imageType=%d, reply status=0x%02X\n", IMX500SF_REG_NW_DD_IMAGE_TYPE, regVal);
                    return E_OTHER;
                }
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_CMD_REPLY_STS  %02X\n", regVal);
                return E_OK;
            }
            
            static E_RESULT_CODE IMX500SF_SpiBoot_ProcFW(int fd,uint32_t imageType,uint32_t transCmd, uint8_t *pTransbuf,uint32_t size) 
            {
                E_RESULT_CODE ret = E_OK;
                uint32_t regVal;
                uint32_t timer_cnt = 0;
                uint8_t *pCurBuf = NULL;
                uint32_t residualSize = 0;
                uint32_t transSize = 0;
                uint8_t refStsCnt = 0;

//                PRINTF("[IMX500LIB][IMAGE_LOADER] IMX500SF_SpiBoot_ProcFW %X\n", fd);

                /* current CMD_REPLY_STS_CNT */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_REF_STS_REG, &regVal, IMX500SF_REG_SIZE_DD_REF_STS_REG);
                refStsCnt = static_cast<uint8_t>(regVal);

                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_LOAD_MODE, IMX500SF_REG_FROMAP_DD_LOAD_MODE, IMX500SF_REG_SIZE_DD_LOAD_MODE);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_IMAGE_TYPE, imageType, IMX500SF_REG_SIZE_DD_IMAGE_TYPE);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_DOWNLOAD_DIV_NUM, 0, IMX500SF_REG_SIZE_DD_DOWNLOAD_DIV_NUM);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_DOWNLOAD_FILE_SIZE, size, IMX500SF_REG_SIZE_DD_DOWNLOAD_FILE_SIZE);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_ST_TRANS_CMD, transCmd, IMX500SF_REG_SIZE_DD_ST_TRANS_CMD);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_CMD_INT, IMX500SF_REG_TRANS_CMD_DD_CMD_INT, IMX500SF_REG_SIZE_DD_CMD_INT);

                /* wait change status */ /*@ 無限ループ */
                while (timer_cnt < IMX500SF_POLLING_TIMEOUT) {
                    I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_REF_STS_REG, &regVal,IMX500SF_REG_SIZE_DD_REF_STS_REG);
                    if (static_cast<uint8_t>(regVal) != refStsCnt) {
                        break;
                    }
                    //SENSOR_USE_TIMER(10000000);  /* wait 10ms */
                    timer_cnt++;
                    if (timer_cnt >= IMX500SF_POLLING_TIMEOUT) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] STATE Change ERROR \n");
                        return E_OTHER; 
                    }
                }
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_REF_STS_REG  %02X\n", regVal);
                
                /* update ref status count */
                refStsCnt = static_cast<uint8_t>(regVal);

                /* check reply status */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_CMD_REPLY_STS, &regVal, IMX500SF_REG_SIZE_DD_CMD_REPLY_STS);
                if (regVal != IMX500SF_REG_DD_CMD_RPLY_STS_TRNS_READY) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] boot error(reply status NG): imageType=%d, reply status=0x%02X\n", imageType, regVal);
                    return E_OTHER;
                }
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_CMD_REPLY_STS  %02X\n", regVal);

                /* SPI Transfer */
                residualSize = size;
                pCurBuf = pTransbuf;
                while (1) {
                    if (residualSize == 0) {
                        break;
                    }
                    if (residualSize >= IMX500SF_SPI_ONE_TRANS_SIZE) {
                        transSize = IMX500SF_SPI_ONE_TRANS_SIZE;
                    } else {
                        transSize = residualSize;
                    }               
                    ret = IMX500SF_TransferSpi(fd,pCurBuf,transSize);
                    if (ret != E_OK) {
                        return ret;
                    }
                    if (transSize >= IMX500SF_SPI_ONE_TRANS_SIZE) {
                        pCurBuf = pCurBuf + IMX500SF_SPI_ONE_TRANS_SIZE;
                        residualSize = residualSize - IMX500SF_SPI_ONE_TRANS_SIZE;
                    } else {
                        residualSize = 0;
                    }               
                }

                /* wait change status */ /*@ 無限ループ */
                while (timer_cnt < IMX500SF_POLLING_TIMEOUT) {
                    I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_REF_STS_REG, &regVal, IMX500SF_REG_SIZE_DD_REF_STS_REG);
                    if (static_cast<uint8_t>(regVal) != refStsCnt) {
                        break;
                    }
                    timer_cnt++;
                    if (timer_cnt >= IMX500SF_POLLING_TIMEOUT) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] STATE Change ERROR \n");
                        return E_OTHER; 
                    }
                }
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_REF_STS_REG  %02X\n", regVal);

                /* check reply status */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_CMD_REPLY_STS, &regVal, IMX500SF_REG_SIZE_DD_CMD_REPLY_STS);
                if (regVal != IMX500SF_REG_DD_CMD_RPLY_STS_TRNS_DONE) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] boot error(reply status NG): imageType=%d, reply status=0x%02X\n", imageType, regVal);
                    return E_OTHER;
                }
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_CMD_REPLY_STS  %02X\n", regVal);
                return E_OK;
            }

            static E_RESULT_CODE IMX500SF_TransferSpi(int fd, uint8_t *pTransbuf,uint32_t transSize)
            {
                E_RESULT_CODE ret = E_OK;
                int32_t ioctrl_ret = 0;
                struct spi_ioc_transfer tr;
                char i2c_device_name[IMG_LDR_I2C_DEVICE_NAME_LEN];

                if ((table[E_MODULE_TYPE].value == E_MODULE_TYPE_KIND_TYPE2) &&
                    (table[E_TYPE2_GPIO_ADAPTER].value == 0)) {
                    if (transSize > 8192) // Sanity check
                    {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] ERROR : transSize > 8192 @IMX500SF_TransferSpi !!! \n");
                        return E_OTHER;
                    }

                    snprintf(i2c_device_name, sizeof(i2c_device_name), "/dev/i2c-0");
                    int i2c_fd = open(i2c_device_name, O_RDWR);
                    if (i2c_fd < 0)
                        return E_DRIVER_ERROR;
                    if (ioctl(i2c_fd, 0x0706, 0x40) < 0)  // 0x0706 is I2C_SLAVE_FORCE, 0x40 is FPGA's I2C addr
                    {
                        close(i2c_fd);
                        return E_DRIVER_ERROR;
                    }

                    unsigned char msg[8194];
                    int len, length;

                    length = 1 + 2;
                    msg[0] = 0x00;
                    msg[1] = 0x07;  // LUCID_FPGA_REG_ADDR_SPI_COMMAND
                    msg[2] = 0x10;
                    len = write(i2c_fd, msg, length);
                    if (len != length)
                    {
                        close(i2c_fd);
                        return E_DRIVER_ERROR;
                    }

                    length = transSize + 2;
                    msg[0] = 0x00;
                    msg[1] = 0x08;  // LUCID_FPGA_REG_ADDR_SPI_WR_DATA
                    for (uint32_t i = 0; i < transSize; i++)
                        msg[i+2] = pTransbuf[i];
                    len = write(i2c_fd, msg, length);
                    if (len != length)
                    {
                        close(i2c_fd);
                        return E_DRIVER_ERROR;
                    }

                    length = 1 + 2;
                    msg[0] = 0x00;  // LUCID_FPGA_REG_ADDR_SPI_COMMAND
                    msg[1] = 0x07;
                    msg[2] = 0x00;
                    len = write(i2c_fd, msg, length);
                    if (len != length)
                    {
                        close(i2c_fd);
                        return E_DRIVER_ERROR;
                    }
                    close(i2c_fd);
                } else {
                    memset(&tr, 0, sizeof(struct spi_ioc_transfer));
                    tr.tx_buf = (unsigned long)pTransbuf;
                    tr.rx_buf = (unsigned long)NULL;
                    tr.len = transSize;
                    tr.delay_usecs = 0;
                    tr.speed_hz = IMX500SF_SPI_DRV_MAX_SPEED;
                    tr.bits_per_word = IMX500SF_SPI_DRV_TRANS_BIT_WIDTH;
                    tr.cs_change = IMX500SF_SPI_CS_CHANGE;

                    ioctrl_ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
                    if (ioctrl_ret < 1) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] IOCTRL ERROR !!! \n");
                        ret = E_OTHER;
                    }
                }
                return ret;
            }

            E_RESULT_CODE Imx500SF_FlashBoot_LoaderFW(IMX500SF_ModuleHandle pHandle, uint32_t flashAddr)
            {
                E_RESULT_CODE ret = E_OK;
                (void)pHandle;
                
                DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][IMAGE_LOADER] Imx500SF_FlashBoot_LoaderFW flashAddr %d\n",flashAddr);

                ret = Imx500SF_FlashBoot_Proc(IMX500SF_REG_LOADER_DD_IMAGE_TYPE,IMX500SF_REG_LOADER_LOAD_ST_TRANS_CMD,flashAddr);
                if (ret != E_OK) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] Imx500SF_FlashBoot_LoaderFW ERROR !!! \n");
                }            
                return ret;
            }
            
            
            E_RESULT_CODE  Imx500SF_FlashBoot_MainFW(IMX500SF_ModuleHandle pHandle,	uint32_t flashAddr)
            {
                E_RESULT_CODE ret = E_OK;
                (void)pHandle;
                
                DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][IMAGE_LOADER] Imx500SF_FlashBoot_MainFW flashAddr 0x%x\n",flashAddr);
                ret = Imx500SF_FlashBoot_Proc(IMX500SF_REG_MAIN_DD_IMAGE_TYPE,IMX500SF_REG_MAINFW_LOAD_ST_TRANS_CMD,flashAddr);
                if (ret != E_OK) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] Imx500SF_FlashBoot_MainFW ERROR !!! \n");
                }            
                return ret;
            }
            
            
            E_RESULT_CODE  Imx500SF_FlashBoot_NetworkWeights(IMX500SF_ModuleHandle pHandle,uint32_t flashAddr)
            {
                E_RESULT_CODE ret = E_OK;
                (void)pHandle;
                
                DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][IMAGE_LOADER] Imx500SF_FlashBoot_NetworkWeights flashAddr 0x%x\n",flashAddr);
                ret = Imx500SF_FlashBoot_Proc(IMX500SF_REG_NW_DD_IMAGE_TYPE,IMX500SF_REG_WO_NW_TO_W_NW_ST_TRANS_CMD,flashAddr);
                if (ret != E_OK) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] Imx500SF_FlashBoot_NetworkWeights ERROR !!! \n");
                }            
                return ret;
            }

            static E_RESULT_CODE Imx500SF_FlashBoot_Proc(uint32_t imageType,uint32_t transCmd, uint32_t flashAddr)
            {
                uint32_t regVal;
                uint8_t refStsCnt = 0;
                uint32_t timer_cnt = 0;

                /* current CMD_REPLY_STS_CNT */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_REF_STS_REG, &regVal, IMX500SF_REG_SIZE_DD_REF_STS_REG);
                refStsCnt =  static_cast<uint8_t>(regVal);
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_REF_STS_REG %02X\n",regVal);

                /* set parameter */
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_ST_TRANS_CMD, transCmd, IMX500SF_REG_SIZE_DD_ST_TRANS_CMD);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_LOAD_MODE, IMX500SF_REG_FROMFLASH_DD_LOAD_MODE, IMX500SF_REG_SIZE_DD_LOAD_MODE);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_IMAGE_TYPE, imageType, IMX500SF_REG_SIZE_DD_IMAGE_TYPE);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_FLASH_ADDR, flashAddr, IMX500SF_REG_SIZE_DD_FLASH_ADDR);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_CMD_INT, IMX500SF_REG_TRANS_CMD_DD_CMD_INT, IMX500SF_REG_SIZE_DD_CMD_INT);

                
                /* wait change status */ /*@ 無限ループ */
                while (timer_cnt < IMX500SF_POLLING_TIMEOUT) {
                    I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_REF_STS_REG, &regVal, IMX500SF_REG_SIZE_DD_REF_STS_REG);
                    if (static_cast<uint8_t>(regVal) != refStsCnt) {
                        break;
                    }
                    timer_cnt++;
                    if (timer_cnt >= IMX500SF_POLLING_TIMEOUT) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] STATE Change ERROR \n");
                        return E_OTHER; 
                    }
                }
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_REF_STS_REG %02X\n",regVal);
                /* check reply status */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_CMD_REPLY_STS, &regVal, IMX500SF_REG_SIZE_DD_CMD_REPLY_STS);
                if (regVal != IMX500SF_REG_DD_CMD_RPLY_STS_TRNS_DONE) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] boot error(reply status NG): imageType=%d, reply status=0x%02X\n", imageType, regVal);
                    return E_OTHER;
                }
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_CMD_REPLY_STS  %02X\n", regVal);
                return E_OK;
            }
        
            E_RESULT_CODE  Imx500SF_FlashUpdate_LoaderFW(IMX500SF_ModuleHandle pHandle, uint32_t flashAddr, uint8_t * pLoaderFW,uint32_t size)
            {
                E_RESULT_CODE ret = E_OK;
                IMX500SF_MODULE_INFO *pModuleInfo;
                pModuleInfo = (IMX500SF_MODULE_INFO *)pHandle;
                uint32_t regVal;

                DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][IMAGE_LOADER] Imx500SF_FlashUpdate_LoaderFW flashAddr 0x%x\n",flashAddr);
                
                /* Current Sate Check */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_SYS_STATE, &regVal, IMX500SF_REG_SIZE_DD_SYS_STATE);
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] Current State :%d\n",regVal);

                if (regVal == IMX500SF_REG_DD_SYS_STATE_STANDBY_WNET) {
                    // State Change
                    ret = IMX500SF_ChangeStandbyWtoStandbyWo();
                    if (ret != E_OK) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER]  Imx500SF_FlashUpdate_LoaderFW ERROR \n");  
                        return ret;
                    }
                } else if (regVal != IMX500SF_REG_DD_SYS_STATE_STANDBY_WONET) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER]  Imx500SF_FlashUpdate_LoaderFW Invalid State %x\n",regVal);
                    return E_OTHER;
                } else {
                    /* Do Nothing */
                }
                ret = IMX500SF_FlashUpdate_ProcFW(pModuleInfo->fd,IMX500SF_REG_LOADER_DD_IMAGE_TYPE,flashAddr,pLoaderFW,size);
                if (ret != E_OK) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] Imx500SF_FlashUpdate_LoaderFW ERROR !!! \n");
                }            
                return ret;
            }

            E_RESULT_CODE Imx500SF_FlashUpdate_MainFW(IMX500SF_ModuleHandle pHandle, uint32_t flashAddr, uint8_t *pMainFW, uint32_t size)
            {
                E_RESULT_CODE ret = E_OK;
                IMX500SF_MODULE_INFO *pModuleInfo;
                pModuleInfo = (IMX500SF_MODULE_INFO *)pHandle;
                uint32_t regVal;

                DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][IMAGE_LOADER] Imx500SF_FlashUpdate_MainFW flashAddr 0x%x\n",flashAddr);
                
                /* Current Sate Check */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_SYS_STATE, &regVal, IMX500SF_REG_SIZE_DD_SYS_STATE);
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] Current State :%d\n",regVal);

                if (regVal == IMX500SF_REG_DD_SYS_STATE_STANDBY_WNET) {
                    // State Change
                    ret = IMX500SF_ChangeStandbyWtoStandbyWo();
                    if (ret != E_OK) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER]  Imx500SF_FlashUpdate_MainFW ERROR \n");  
                        return ret;
                    }
                } else if (regVal != IMX500SF_REG_DD_SYS_STATE_STANDBY_WONET) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER]  Imx500SF_FlashUpdate_MainFW Invalid State %x\n",regVal);
                    return E_OTHER;
                } else {
                    /* Do Nothing */
                }
                ret = IMX500SF_FlashUpdate_ProcFW(pModuleInfo->fd,IMX500SF_REG_MAIN_DD_IMAGE_TYPE,flashAddr, pMainFW, size);
                if (ret != E_OK) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] Imx500SF_FlashUpdate_MainFW ERROR !!! \n");
                }            
                return ret;
            }

            E_RESULT_CODE  Imx500SF_FlashUpdate_NetworkWeights(IMX500SF_ModuleHandle pHandle, uint32_t flashAddr, uint8_t *pNetworkWeights, uint32_t size)
            {
                E_RESULT_CODE ret = E_OK;
                IMX500SF_MODULE_INFO *pModuleInfo;
                pModuleInfo = (IMX500SF_MODULE_INFO *)pHandle;
                uint32_t regVal;

                DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][IMAGE_LOADER] Imx500SF_FlashUpdate_NetworkWeights flashAddr 0x%x\n",flashAddr);
                
                /* Current Sate Check */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_SYS_STATE, &regVal, IMX500SF_REG_SIZE_DD_SYS_STATE);
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] Current State :%d\n",regVal);

                if (regVal == IMX500SF_REG_DD_SYS_STATE_STANDBY_WNET) {
                    // State Change
                    ret = IMX500SF_ChangeStandbyWtoStandbyWo();
                    if (ret != E_OK) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER]  Imx500SF_FlashUpdate_NetworkWeights ERROR \n");  
                        return ret;
                    }
                } else if (regVal != IMX500SF_REG_DD_SYS_STATE_STANDBY_WONET) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER]  Imx500SF_FlashUpdate_NetworkWeights Invalid State %x\n",regVal);
                    return E_OTHER;
                } else {
                    /* Do Nothing */
                }
                ret = IMX500SF_FlashUpdate_ProcNW(pModuleInfo->fd,flashAddr, pNetworkWeights, size);
                if (ret != E_OK) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] Imx500SF_FlashUpdate_NetworkWeights ERROR !!! \n");
                }            
                return ret;
            }

            static E_RESULT_CODE IMX500SF_ChangeStandbyWtoStandbyWo(void) 
            {
                uint32_t regVal;
                uint32_t timer_cnt = 0;
                uint8_t refStsCnt = 0;

                /* current CMD_REPLY_STS_CNT */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_REF_STS_REG, &regVal, IMX500SF_REG_SIZE_DD_REF_STS_REG);
                refStsCnt = static_cast<uint8_t>(regVal);

                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_REF_STS_REG=0x%02X\n", regVal);

                //I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_ST_TRANS_CMD, IMX500SF_REG_W_NW_TO_WO_NW_ST_TRANS_CMD, IMX500SF_REG_SIZE_DD_ST_TRANS_CMD);
                //I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_CMD_INT, IMX500SF_REG_TRANS_CMD_DD_CMD_INT, IMX500SF_REG_SIZE_DD_CMD_INT);

                I2C_ACCESS_WRITE(0xD000, 0x03, 1);
                I2C_ACCESS_WRITE(0x3080, 0x00, 1);

                /* wait change status */ /*@ 無限ループ */
                while (timer_cnt < IMX500SF_POLLING_TIMEOUT) {
                    I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_REF_STS_REG, &regVal,IMX500SF_REG_SIZE_DD_REF_STS_REG);
                    if (static_cast<uint8_t>(regVal) != refStsCnt) {
                        break;
                    }
                    //SENSOR_USE_TIMER(10000000);  /* wait 10ms */
                    timer_cnt++;
                    if (timer_cnt >= IMX500SF_POLLING_TIMEOUT) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] STATE Change ERROR \n");
                        return E_OTHER; 
                    }
                }
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_CMD_REPLY_STS, &regVal, IMX500SF_REG_SIZE_DD_CMD_REPLY_STS);
                if (regVal != IMX500SF_REG_DD_CMD_RPLY_STS_TRNS_DONE) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] Standby WNet -> WoNet error(reply status NG): reply status=0x%02X\n", regVal);
                    return E_OTHER;
                }
                return E_OK;
            }

            static E_RESULT_CODE IMX500SF_FlashUpdate_ProcFW(int fd,uint32_t imageType,uint32_t flashAddr,uint8_t *pTransbuf,uint32_t size) 
            {
                E_RESULT_CODE ret = E_OK;
                uint32_t regVal;
                uint32_t timer_cnt = 0;
                uint8_t *pCurBuf = NULL;
                uint32_t residualSize = 0;
                uint32_t transSize = 0;
                uint8_t refStsCnt = 0;

                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] IMX500SF_FlashUpdate_ProcFW %X\n", fd);

                /* current CMD_REPLY_STS_CNT */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_REF_STS_REG, &regVal, IMX500SF_REG_SIZE_DD_REF_STS_REG);
                refStsCnt = static_cast<uint8_t>(regVal);

                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_LOAD_MODE, IMX500SF_REG_FROMAP_DD_LOAD_MODE, IMX500SF_REG_SIZE_DD_LOAD_MODE);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_IMAGE_TYPE, imageType, IMX500SF_REG_SIZE_DD_IMAGE_TYPE);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_DOWNLOAD_DIV_NUM, 0, IMX500SF_REG_SIZE_DD_DOWNLOAD_DIV_NUM);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_DOWNLOAD_FILE_SIZE, size, IMX500SF_REG_SIZE_DD_DOWNLOAD_FILE_SIZE);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_FLASH_ADDR, flashAddr, IMX500SF_REG_SIZE_DD_FLASH_ADDR);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_UPDATE_CMD, IMX500SF_REG_DD_UPDATE_CMD_UPDATE_FLASH, IMX500SF_REG_SIZE_DD_UPDATE_CMD);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_CMD_INT, IMX500SF_REG_UPDATE_CMD_DD_CMD_INT, IMX500SF_REG_SIZE_DD_CMD_INT);

                /* wait change status */ /*@ 無限ループ */
                while (timer_cnt < IMX500SF_POLLING_TIMEOUT) {
                    I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_REF_STS_REG, &regVal,IMX500SF_REG_SIZE_DD_REF_STS_REG);
                    if (static_cast<uint8_t>(regVal) != refStsCnt) {
                        break;
                    }
                    timer_cnt++;
                    if (timer_cnt >= IMX500SF_POLLING_TIMEOUT) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] STATE Change ERROR \n");
                        return E_OTHER; 
                    }
                }
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_REF_STS_REG  %02X\n", regVal);
                
                /* update ref status count */
                refStsCnt = static_cast<uint8_t>(regVal);

                /* check reply status */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_CMD_REPLY_STS, &regVal, IMX500SF_REG_SIZE_DD_CMD_REPLY_STS);
                if (regVal != IMX500SF_REG_DD_CMD_RPLY_STS_UPDATE_READY) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] boot error(reply status NG): imageType=%d, reply status=0x%02X\n", imageType, regVal);
                    return E_OTHER;
                }
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_CMD_REPLY_STS  %02X\n", regVal);

                /* SPI Transfer */
                residualSize = size;
                pCurBuf = pTransbuf;
                while (1) {
                    if (residualSize == 0) {
                        break;
                    }
                    if (residualSize >= IMX500SF_SPI_ONE_TRANS_SIZE) {
                        transSize = IMX500SF_SPI_ONE_TRANS_SIZE;
                    } else {
                        transSize = residualSize;
                    }               
                    ret = IMX500SF_TransferSpi(fd,pCurBuf,transSize);
                    if (ret != E_OK) {
                        return ret;
                    }
                    if (transSize >= IMX500SF_SPI_ONE_TRANS_SIZE) {
                        pCurBuf = pCurBuf + IMX500SF_SPI_ONE_TRANS_SIZE;
                        residualSize = residualSize - IMX500SF_SPI_ONE_TRANS_SIZE;
                    } else {
                        residualSize = 0;
                    }               
                }

                /* wait change status */ /*@ 無限ループ */
                while (timer_cnt < IMX500SF_POLLING_TIMEOUT) {
                    I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_REF_STS_REG, &regVal, IMX500SF_REG_SIZE_DD_REF_STS_REG);
                    if (static_cast<uint8_t>(regVal) != refStsCnt) {
                        break;
                    }
                    timer_cnt++;
                    if (timer_cnt >= IMX500SF_POLLING_TIMEOUT) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] STATE Change ERROR \n");
                        return E_OTHER; 
                    }
                }
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_REF_STS_REG  %02X\n", regVal);

                /* check reply status */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_CMD_REPLY_STS, &regVal, IMX500SF_REG_SIZE_DD_CMD_REPLY_STS);
                if (regVal != IMX500SF_REG_DD_CMD_RPLY_STS_UPDATE_DONE) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] update error(reply status NG): imageType=%d, reply status=0x%02X\n", imageType, regVal);
                    return E_OTHER;
                }
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_CMD_REPLY_STS  %02X\n", regVal);
                return E_OK;
            }

            static E_RESULT_CODE IMX500SF_FlashUpdate_ProcNW(int fd,uint32_t flashAddr,uint8_t *pTransbuf,uint32_t size)
            {
                E_RESULT_CODE ret = E_OK;
                uint32_t regVal;
                uint8_t  divNum = 0;
                uint32_t timer_cnt = 0;
                uint8_t *pCurBuf = NULL;
                uint32_t residualSize = 0;
                uint32_t transSize = 0;
                uint8_t refStsCnt = 0;
                uint32_t spiTransactionSize = 0;
                uint8_t i= 0;
    #if  IMX500SF_MEASUREMENT_ENABLE
                struct timespec startTime, endTime;
    #endif
                /* current CMD_REPLY_STS_CNT */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_REF_STS_REG, &regVal, IMX500SF_REG_SIZE_DD_REF_STS_REG);
                refStsCnt = static_cast<uint8_t>(regVal);

                /* Determining the number of divisions */
                if ((size % IMX500SF_NW_TRANS_SIZE_UNIT) == 0) {
                    divNum = (size/IMX500SF_NW_TRANS_SIZE_UNIT) - 1;  
                } else {
                    divNum = size/IMX500SF_NW_TRANS_SIZE_UNIT;
                }

                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_LOAD_MODE, IMX500SF_REG_FROMAP_DD_LOAD_MODE, IMX500SF_REG_SIZE_DD_LOAD_MODE);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_IMAGE_TYPE, IMX500SF_REG_NW_DD_IMAGE_TYPE, IMX500SF_REG_SIZE_DD_IMAGE_TYPE);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_DOWNLOAD_DIV_NUM, divNum, IMX500SF_REG_SIZE_DD_DOWNLOAD_DIV_NUM);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_DOWNLOAD_FILE_SIZE, size, IMX500SF_REG_SIZE_DD_DOWNLOAD_FILE_SIZE);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_FLASH_ADDR, flashAddr, IMX500SF_REG_SIZE_DD_FLASH_ADDR);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_UPDATE_CMD, IMX500SF_REG_DD_UPDATE_CMD_UPDATE_FLASH, IMX500SF_REG_SIZE_DD_UPDATE_CMD);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_CMD_INT, IMX500SF_REG_UPDATE_CMD_DD_CMD_INT, IMX500SF_REG_SIZE_DD_CMD_INT);


                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] IMX500SF_FlashUpdate_ProcNW refStsCnt %x divNum %x\n",refStsCnt,divNum);
                
                /* wait change status */ /*@ 無限ループ */
                while (timer_cnt < IMX500SF_POLLING_TIMEOUT) {
                    I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_REF_STS_REG, &regVal, IMX500SF_REG_SIZE_DD_REF_STS_REG);
                    if (static_cast<uint8_t>(regVal) != refStsCnt) {
                        break;
                    }
                    timer_cnt++;
                    if (timer_cnt >= IMX500SF_POLLING_TIMEOUT) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] STATE Change ERROR \n");
                        return E_OTHER; 
                    }
                }
                //PRINTF("[IMX500LIB][IMAGE_LOADER] timer_cnt  %x\n", timer_cnt);
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_REF_STS_REG  %02X\n", regVal);
                /* update ref status count */
                refStsCnt = static_cast<uint8_t>(regVal);

                /* check reply status */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_CMD_REPLY_STS, &regVal, IMX500SF_REG_SIZE_DD_CMD_REPLY_STS);
                if (regVal != IMX500SF_REG_DD_CMD_RPLY_STS_UPDATE_READY) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] nework Flash Update error(reply status NG): imageType=%d, reply status=0x%02X\n", IMX500SF_REG_NW_DD_IMAGE_TYPE, regVal);
                    return E_OTHER;
                }
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_CMD_REPLY_STS  %02X\n", regVal);
                
                residualSize = size;
                pCurBuf = pTransbuf;

                for (i = 0; i <= divNum; i++) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] %02X divid Trans  residualSize %d\n", i, residualSize);
                    /* Check DOWNLOAD Status */
                    while (1) {
                        I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_DOWNLOAD_STS, &regVal, IMX500SF_REG_SIZE_DD_DOWNLOAD_STS);
                        if (regVal == IMX500SF_REG_READY_DD_DOWNLOAD_STS) {
                            break;
                        }
                    }
                    I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_DOWNLOAD_STS, &regVal, IMX500SF_REG_SIZE_DD_DOWNLOAD_STS);
                    DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_DOWNLOAD_STS  %02X\n", regVal);
                    
                    if (residualSize >= IMX500SF_NW_TRANS_SIZE_UNIT) {
                        spiTransactionSize = IMX500SF_NW_TRANS_SIZE_UNIT;
                    } else {
                        spiTransactionSize = residualSize;
                    }
    #if  IMX500SF_MEASUREMENT_ENABLE
                    clock_gettime(CLOCK_REALTIME, &startTime);
    #endif
                    DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] residualSize:%d   spiTransactionSize:%d\n", residualSize,spiTransactionSize);
                    while (1) {
                        if (spiTransactionSize == 0) {
                            break;
                        }
                        if (spiTransactionSize >= IMX500SF_SPI_ONE_TRANS_SIZE) {
                            transSize = IMX500SF_SPI_ONE_TRANS_SIZE;
                        } else {
                            transSize = spiTransactionSize;
                        }               
                        ret = IMX500SF_TransferSpi(fd,pCurBuf,transSize);
                        if (ret != E_OK) {
                            return ret;
                        }
                        if (transSize >= IMX500SF_SPI_ONE_TRANS_SIZE) {
                            pCurBuf = pCurBuf + IMX500SF_SPI_ONE_TRANS_SIZE;
                            spiTransactionSize = spiTransactionSize - IMX500SF_SPI_ONE_TRANS_SIZE;
                        } else {
                            spiTransactionSize = 0;
                        }               
                    }
                    
                    /* Update Residual Size */
                    if (residualSize >= IMX500SF_NW_TRANS_SIZE_UNIT) {
                        residualSize = residualSize - IMX500SF_NW_TRANS_SIZE_UNIT;
                    } else {
                        residualSize = 0;
                    }
    #if IMX500SF_MEASUREMENT_ENABLE
                    clock_gettime(CLOCK_REALTIME, &endTime);
                    DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "Proc Time = ");
                    if (endTime.tv_nsec < startTime.tv_nsec) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "%10ld.%09ld", endTime.tv_sec - startTime.tv_sec - 1
                                ,endTime.tv_nsec + 1000000000 - startTime.tv_nsec);
                    } else {
                        DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "%10ld.%09ld", endTime.tv_sec - startTime.tv_sec
                                ,endTime.tv_nsec - startTime.tv_nsec);
                    }
                    DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "(second)\n");
    #endif
                }
                /* wait change status */ /*@ 無限ループ */
                while (timer_cnt < IMX500SF_POLLING_TIMEOUT) {
                    I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_REF_STS_REG, &regVal, IMX500SF_REG_SIZE_DD_REF_STS_REG);
                    if (static_cast<uint8_t>(regVal) != refStsCnt) {
                        break;
                    }
                    timer_cnt++;
                    if (timer_cnt >= IMX500SF_POLLING_TIMEOUT) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] STATE Change ERROR \n");
                        return E_OTHER; 
                    }
                }
                //PRINTF("[IMX500LIB][IMAGE_LOADER] timer_cnt  %x\n", timer_cnt);
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_REF_STS_REG  %02X\n", regVal);
                /* check reply status */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_CMD_REPLY_STS, &regVal, IMX500SF_REG_SIZE_DD_CMD_REPLY_STS);
                if (regVal != IMX500SF_REG_DD_CMD_RPLY_STS_UPDATE_DONE) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] nework update error(reply status NG): imageType=%d, reply status=0x%02X\n", IMX500SF_REG_NW_DD_IMAGE_TYPE, regVal);
                    return E_OTHER;
                }
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_CMD_REPLY_STS  %02X\n", regVal);
                return E_OK;
            }
            
            E_RESULT_CODE  Imx500SF_NetworkWeightUpdate_Spi(IMX500SF_ModuleHandle pHandle, uint8_t *pNetworkWeights, uint32_t size)
            {
                E_RESULT_CODE ret = E_OK;
                uint32_t regVal;
                uint8_t  divNum = 0;
                uint32_t timer_cnt = 0;
                uint8_t *pCurBuf = NULL;
                uint32_t residualSize = 0;
                uint32_t transSize = 0;
                uint8_t refStsCnt = 0;
                uint32_t spiTransactionSize = 0;
                uint8_t i= 0;
    #if  IMX500SF_MEASUREMENT_ENABLE
                struct timespec startTime, endTime;
    #endif
                IMX500SF_MODULE_INFO *pModuleInfo;
                pModuleInfo = (IMX500SF_MODULE_INFO *)pHandle;
                int fd = pModuleInfo->fd; 
                
                DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][IMAGE_LOADER] Imx500SF_NetworkWeightUpdate_Spi \n");

                /* current CMD_REPLY_STS_CNT */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_REF_STS_REG, &regVal, IMX500SF_REG_SIZE_DD_REF_STS_REG);
                refStsCnt = static_cast<uint8_t>(regVal);

                /* Determining the number of divisions */
                if ((size % IMX500SF_NW_TRANS_SIZE_UNIT) == 0) {
                    divNum = (size/IMX500SF_NW_TRANS_SIZE_UNIT) - 1;  
                } else {
                    divNum = size/IMX500SF_NW_TRANS_SIZE_UNIT;
                }

                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_LOAD_MODE, IMX500SF_REG_FROMAP_DD_LOAD_MODE, IMX500SF_REG_SIZE_DD_LOAD_MODE);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_IMAGE_TYPE, IMX500SF_REG_NW_DD_IMAGE_TYPE, IMX500SF_REG_SIZE_DD_IMAGE_TYPE);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_DOWNLOAD_DIV_NUM, divNum, IMX500SF_REG_SIZE_DD_DOWNLOAD_DIV_NUM);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_DOWNLOAD_FILE_SIZE, size, IMX500SF_REG_SIZE_DD_DOWNLOAD_FILE_SIZE);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_UPDATE_CMD, IMX500SF_REG_DD_UPDATE_CMD_UPDATE_L2MEM, IMX500SF_REG_SIZE_DD_UPDATE_CMD);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_CMD_INT, IMX500SF_REG_UPDATE_CMD_DD_CMD_INT, IMX500SF_REG_SIZE_DD_CMD_INT);

                /* wait change status */ /*@ 無限ループ */
                while (timer_cnt < IMX500SF_POLLING_TIMEOUT) {
                    I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_REF_STS_REG, &regVal, IMX500SF_REG_SIZE_DD_REF_STS_REG);
                    if (static_cast<uint8_t>(regVal) != refStsCnt) {
                        break;
                    }
                    timer_cnt++;
                    if (timer_cnt >= IMX500SF_POLLING_TIMEOUT) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] STATE Change ERROR \n");
                        return E_OTHER; 
                    }
                }
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_REF_STS_REG  %02X\n", regVal);
                /* update ref status count */
                refStsCnt = static_cast<uint8_t>(regVal);
                /* check reply status */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_CMD_REPLY_STS, &regVal, IMX500SF_REG_SIZE_DD_CMD_REPLY_STS);
                if (regVal != IMX500SF_REG_DD_CMD_RPLY_STS_UPDATE_READY) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] Imx500SF_NetworkWeightUpdate_Spi error(reply status NG): imageType=%d, reply status=0x%02X\n", IMX500SF_REG_NW_DD_IMAGE_TYPE, regVal);
                    return E_OTHER;
                }
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_CMD_REPLY_STS  %02X\n", regVal);
                
                residualSize = size;
                pCurBuf = pNetworkWeights;

                for (i = 0; i <= divNum; i++) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] %02X divid Trans  residualSize %d\n", i, residualSize);
                    /* Check DOWNLOAD Status */
                    while (1) {
                        I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_DOWNLOAD_STS, &regVal, IMX500SF_REG_SIZE_DD_DOWNLOAD_STS);
                        if (regVal == IMX500SF_REG_READY_DD_DOWNLOAD_STS) {
                            break;
                        }
                    }
                    I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_DOWNLOAD_STS, &regVal, IMX500SF_REG_SIZE_DD_DOWNLOAD_STS);
                    DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_DOWNLOAD_STS  %02X\n", regVal);
                    
                    if (residualSize >= IMX500SF_NW_TRANS_SIZE_UNIT) {
                        spiTransactionSize = IMX500SF_NW_TRANS_SIZE_UNIT;
                    } else {
                        spiTransactionSize = residualSize;
                    }
    #if  IMX500SF_MEASUREMENT_ENABLE
                    clock_gettime(CLOCK_REALTIME, &startTime);
    #endif
                    DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] residualSize:%d   spiTransactionSize:%d\n", residualSize,spiTransactionSize);
                    while (1) {
                        if (spiTransactionSize == 0) {
                            break;
                        }
                        if (spiTransactionSize >= IMX500SF_SPI_ONE_TRANS_SIZE) {
                            transSize = IMX500SF_SPI_ONE_TRANS_SIZE;
                        } else {
                            transSize = spiTransactionSize;
                        }               
                        ret = IMX500SF_TransferSpi(fd,pCurBuf,transSize);
                        if (ret != E_OK) {
                            return ret;
                        }
                        if (transSize >= IMX500SF_SPI_ONE_TRANS_SIZE) {
                            pCurBuf = pCurBuf + IMX500SF_SPI_ONE_TRANS_SIZE;
                            spiTransactionSize = spiTransactionSize - IMX500SF_SPI_ONE_TRANS_SIZE;
                        } else {
                            spiTransactionSize = 0;
                        }               
                    }
                    
                    /* Update Residual Size */
                    if (residualSize >= IMX500SF_NW_TRANS_SIZE_UNIT) {
                        residualSize = residualSize - IMX500SF_NW_TRANS_SIZE_UNIT;
                    } else {
                        residualSize = 0;
                    }
    #if IMX500SF_MEASUREMENT_ENABLE
                    clock_gettime(CLOCK_REALTIME, &endTime);
                    DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "Proc Time = ");
                    if (endTime.tv_nsec < startTime.tv_nsec) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "%10ld.%09ld", endTime.tv_sec - startTime.tv_sec - 1
                                ,endTime.tv_nsec + 1000000000 - startTime.tv_nsec);
                    } else {
                        DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "%10ld.%09ld", endTime.tv_sec - startTime.tv_sec
                                ,endTime.tv_nsec - startTime.tv_nsec);
                    }
                    DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "(second)\n");
    #endif
                }
                /* wait change status */ /*@ 無限ループ */
                while (timer_cnt < IMX500SF_POLLING_TIMEOUT) {
                    I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_REF_STS_REG, &regVal, IMX500SF_REG_SIZE_DD_REF_STS_REG);
                    if (static_cast<uint8_t>(regVal) != refStsCnt) {
                        break;
                    }
                    timer_cnt++;
                    if (timer_cnt >= IMX500SF_POLLING_TIMEOUT) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] STATE Change ERROR \n");
                        return E_OTHER; 
                    }
                }
                //PRINTF("[IMX500LIB][IMAGE_LOADER] timer_cnt  %x\n", timer_cnt);
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_REF_STS_REG  %02X\n", regVal);
                /* check reply status */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_CMD_REPLY_STS, &regVal, IMX500SF_REG_SIZE_DD_CMD_REPLY_STS);
                if (regVal != IMX500SF_REG_DD_CMD_RPLY_STS_UPDATE_DONE) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] nework update error(reply status NG): imageType=%d, reply status=0x%02X\n", IMX500SF_REG_NW_DD_IMAGE_TYPE, regVal);
                    return E_OTHER;
                }
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_CMD_REPLY_STS  %02X\n", regVal);
                return E_OK;
            }  

            E_RESULT_CODE  Imx500SF_NetworkWeightUpdate_Flash(IMX500SF_ModuleHandle pHandle, uint32_t flashAddr)
            {
                uint32_t regVal;
                uint8_t refStsCnt = 0;
                uint32_t timer_cnt = 0;
                (void)pHandle;

                DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][IMAGE_LOADER] Imx500SF_NetworkWeightUpdate_Flash \n");

                /* current CMD_REPLY_STS_CNT */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_REF_STS_REG, &regVal, IMX500SF_REG_SIZE_DD_REF_STS_REG);
                refStsCnt = static_cast<uint8_t>(regVal);
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_REF_STS_REG  %02X\n", regVal);
                
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_LOAD_MODE, IMX500SF_REG_FROMFLASH_DD_LOAD_MODE, IMX500SF_REG_SIZE_DD_LOAD_MODE);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_IMAGE_TYPE, IMX500SF_REG_NW_DD_IMAGE_TYPE, IMX500SF_REG_SIZE_DD_IMAGE_TYPE);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_FLASH_ADDR, flashAddr, IMX500SF_REG_SIZE_DD_FLASH_ADDR);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_UPDATE_CMD, IMX500SF_REG_DD_UPDATE_CMD_UPDATE_L2MEM, IMX500SF_REG_SIZE_DD_UPDATE_CMD);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_CMD_INT, IMX500SF_REG_UPDATE_CMD_DD_CMD_INT, IMX500SF_REG_SIZE_DD_CMD_INT);

                /* wait change status */ /*@ 無限ループ */
                while (timer_cnt < IMX500SF_POLLING_TIMEOUT) {
                    I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_REF_STS_REG, &regVal, IMX500SF_REG_SIZE_DD_REF_STS_REG);
                    if (static_cast<uint8_t>(regVal) != refStsCnt) {
                        break;
                    }
                    timer_cnt++;
                    if (timer_cnt >= IMX500SF_POLLING_TIMEOUT) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] STATE Change ERROR \n");
                        return E_OTHER; 
                    }
                }
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_REF_STS_REG  %02X\n", regVal);
                /* check reply status */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_CMD_REPLY_STS, &regVal, IMX500SF_REG_SIZE_DD_CMD_REPLY_STS);
                if (regVal != IMX500SF_REG_DD_CMD_RPLY_STS_UPDATE_DONE) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] NetworkWeightUpdate_Flash error(reply status NG): reply status=0x%02X\n", regVal);
                    return E_OTHER;
                }
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_CMD_REPLY_STS  %02X\n", regVal);
                return E_OK;
            }

            E_RESULT_CODE  Imx500SF_MainFwUpdate_Spi(IMX500SF_ModuleHandle pHandle, uint8_t *pMainFw, uint32_t size)
            {
                E_RESULT_CODE ret = E_OK;
                uint32_t regVal;
                uint32_t timer_cnt = 0;
                uint32_t transSize = 0;
                uint32_t residualSize = 0;
                uint8_t *pCurBuf = NULL;
                uint8_t refStsCnt = 0;

                IMX500SF_MODULE_INFO *pModuleInfo;
                pModuleInfo = (IMX500SF_MODULE_INFO *)pHandle;
                int fd = pModuleInfo->fd; 
                
                DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][IMAGE_LOADER] Imx500SF_MainFwUpdate_Spi \n");

                /* current CMD_REPLY_STS_CNT */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_REF_STS_REG, &regVal, IMX500SF_REG_SIZE_DD_REF_STS_REG);
                refStsCnt = static_cast<uint8_t>(regVal);

                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_LOAD_MODE, IMX500SF_REG_FROMAP_DD_LOAD_MODE, IMX500SF_REG_SIZE_DD_LOAD_MODE);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_IMAGE_TYPE, IMX500SF_REG_MAIN_DD_IMAGE_TYPE, IMX500SF_REG_SIZE_DD_IMAGE_TYPE);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_DOWNLOAD_DIV_NUM, 0, IMX500SF_REG_SIZE_DD_DOWNLOAD_DIV_NUM);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_DOWNLOAD_FILE_SIZE, size, IMX500SF_REG_SIZE_DD_DOWNLOAD_FILE_SIZE);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_UPDATE_CMD, IMX500SF_REG_DD_UPDATE_CMD_UPDATE_L2MEM, IMX500SF_REG_SIZE_DD_UPDATE_CMD);
                I2C_ACCESS_WRITE(IMX500SF_REG_ADDR_DD_CMD_INT, IMX500SF_REG_UPDATE_CMD_DD_CMD_INT, IMX500SF_REG_SIZE_DD_CMD_INT);

                /* wait change status */ /*@ 無限ループ */
                while (timer_cnt < IMX500SF_POLLING_TIMEOUT) {
                    I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_REF_STS_REG, &regVal, IMX500SF_REG_SIZE_DD_REF_STS_REG);
                    if (static_cast<uint8_t>(regVal) != refStsCnt) {
                        break;
                    }
                    timer_cnt++;
                    if (timer_cnt >= IMX500SF_POLLING_TIMEOUT) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] STATE Change ERROR \n");
                        return E_OTHER; 
                    }
                }
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_REF_STS_REG  %02X\n", regVal);
                /* update ref status count */
                refStsCnt = static_cast<uint8_t>(regVal);
                /* check reply status */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_CMD_REPLY_STS, &regVal, IMX500SF_REG_SIZE_DD_CMD_REPLY_STS);
                if (regVal != IMX500SF_REG_DD_CMD_RPLY_STS_UPDATE_READY) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] Imx500SF_MainFwUpdate_Spi error(reply status NG): imageType=%d, reply status=0x%02X\n", IMX500SF_REG_MAIN_DD_IMAGE_TYPE, regVal);
                    return E_OTHER;
                }
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_CMD_REPLY_STS  %02X\n", regVal);

                /* Check DOWNLOAD Status */
                while (1) {
                    I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_DOWNLOAD_STS, &regVal, IMX500SF_REG_SIZE_DD_DOWNLOAD_STS);
                    if (regVal == IMX500SF_REG_READY_DD_DOWNLOAD_STS) {
                        break;
                    }
                }
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_DOWNLOAD_STS, &regVal, IMX500SF_REG_SIZE_DD_DOWNLOAD_STS);
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_DOWNLOAD_STS  %02X\n", regVal);

                /* SPI Transfer */
                residualSize = size;
                pCurBuf = pMainFw;
                while (1) {
                    if (residualSize == 0) {
                        break;
                    }
                    if (residualSize >= IMX500SF_SPI_ONE_TRANS_SIZE) {
                        transSize = IMX500SF_SPI_ONE_TRANS_SIZE;
                    } else {
                        transSize = residualSize;
                    }               
                    ret = IMX500SF_TransferSpi(fd,pCurBuf,transSize);
                    if (ret != E_OK) {
                        return ret;
                    }
                    if (transSize >= IMX500SF_SPI_ONE_TRANS_SIZE) {
                        pCurBuf = pCurBuf + IMX500SF_SPI_ONE_TRANS_SIZE;
                        residualSize = residualSize - IMX500SF_SPI_ONE_TRANS_SIZE;
                    } else {
                        residualSize = 0;
                    }
                }

                /* wait change status */ /*@ 無限ループ */
                timer_cnt = 0;
                while (timer_cnt < IMX500SF_POLLING_TIMEOUT) {
                    I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_REF_STS_REG, &regVal, IMX500SF_REG_SIZE_DD_REF_STS_REG);
                    if (static_cast<uint8_t>(regVal) != refStsCnt) {
                        break;
                    }
                    timer_cnt++;
                    if (timer_cnt >= IMX500SF_POLLING_TIMEOUT) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] STATE Change ERROR \n");
                        return E_OTHER; 
                    }
                }
                //PRINTF("[IMX500LIB][IMAGE_LOADER] timer_cnt  %x\n", timer_cnt);
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_REF_STS_REG  %02X\n", regVal);
                /* check reply status */
                I2C_ACCESS_READ(IMX500SF_REG_ADDR_DD_CMD_REPLY_STS, &regVal, IMX500SF_REG_SIZE_DD_CMD_REPLY_STS);
                if (regVal != IMX500SF_REG_DD_CMD_RPLY_STS_UPDATE_DONE) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][IMAGE_LOADER] nework update error(reply status NG): imageType=%d, reply status=0x%02X\n", IMX500SF_REG_NW_DD_IMAGE_TYPE, regVal);
                    return E_OTHER;
                }
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][IMAGE_LOADER] DD_CMD_REPLY_STS  %02X\n", regVal);
                return E_OK;
            }
        }
    }
}
