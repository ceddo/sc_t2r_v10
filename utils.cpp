/*
* Copyright 2017-2018 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/
#include <stdint.h>
#include <iostream>
#include "utils.h"
#include "dbg_log.h"


#define bytes_to_uint16(MSB,LSB) (((uint16_t) ((unsigned char) MSB)) & 255)<<8 | (((unsigned char) LSB)&255) 
#define bytes_to_int16(MSB,LSB) (((int16_t) ((unsigned char) MSB)) & 255)<<8 | (((unsigned char) LSB)&255) 
#define extract_byte(MSB,LSB) (((((unsigned char) MSB)<<6) & 0xc0) | ((((unsigned char) LSB) & 0xfc) >> 2))

namespace imx500 {
	namespace utils {

		/* Extract Packet Header Data */
		int extract_packet_header(const uint16_t* src, PacketHeader* packetHeader) {
			packetHeader->vc = (*src) & 0xc0;
			packetHeader->dataType = (*src) & 0x3f;
			packetHeader->wordCount = (*(src + 2) << 8 | *(src + 1)) * 8 / 10;
			return 0;
		}

		/* Extract Dnn Header Data */
		int extract_dnn_header(const uint8_t* src, DnnHeader* dnnHeader) {

            if (dnnHeader == NULL) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[UTIL] dnn_header is NULL\n");
				return -1;
			}
			
			dnnHeader->frameValid = *(src++);
			dnnHeader->frameCount = *(src++);

			dnnHeader->maxLineLen =  (*(src+1) << 8 |*(src));
                        src+=2;
			dnnHeader->apParamSize = (*(src+1) << 8 |*(src));
                        src+=2;
			dnnHeader->networkId = (*(src+1) << 8 |*(src));
                        src+=2;
			dnnHeader->tensorType = *(src++);
			DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[UTIL] dnn_header:\n");
			DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[UTIL] frameValid=%d, frameCount=%d, mll=%d, apParamSize=%d, nwId=%d, tensorType=%d\n",
						  dnnHeader->frameValid,
						  dnnHeader->frameCount,
						  dnnHeader->maxLineLen,
						  dnnHeader->apParamSize,
						  dnnHeader->networkId,
						  dnnHeader->tensorType);
			return 0;
		}
	}
} //namespace imx50::utils
