/*
* Copyright 2017-2018 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/
#ifndef SENSOR_PARAM_MANAGER_H_
#define SENSOR_PARAM_MANAGER_H_

#include <stdint.h>
#include "return_code.h"
#include "sensor_control.h"


/*
* Input Tensor Namespace
*/
namespace imx500 {
    namespace param_manager {

        E_RESULT_CODE SetInck(uint32_t inckFreq, uint32_t mipiDataRate, uint8_t imgOnlyMode, uint8_t flashType);
        E_RESULT_CODE SetDefaultParam();
        E_RESULT_CODE SetValiableParam(uint8_t raw_full_size_ena, uint8_t frame_rate, uint8_t binning_opt);
        E_RESULT_CODE SetCroppingDnnImg(uint16_t left, uint16_t top, uint16_t width, uint16_t height);
        E_RESULT_CODE SetCroppingRawImg(uint16_t left, uint16_t top, uint16_t width, uint16_t height);
        E_RESULT_CODE SetScalingRawImg(uint16_t width, uint16_t height);
        E_RESULT_CODE SetOutSizeDnnCH(uint16_t xOutSize);
        E_RESULT_CODE SetFrameRate(uint8_t frmRate);

    } /* namespace param_manager */
} /* namespace imx500 */

#endif /* SENSOR_PARAM_MANAGER */
