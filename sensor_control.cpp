/*
* Copyright 2017-2018 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/

#include <stdint.h>
#include "return_code.h"
#include "common_define.h"
#include "i2c_access.h"
#include "sensor_param_manager.h"
#include "sensor_control.h"
#include "sensor_control_private.h"
#include "image_loader.h"
#include "imx500lib_sample.h"
#include "dbg_log.h"


/* define MACROs */
#ifndef IMX500LIB_TIMER_DISABLE
#include "senscord/osal.h"
#define SENSOR_USE_TIMER    (senscord::osal::OSSleep)
#define POLLING_TIMEOUT     (100)       /* 1sec */
#else
#define SENSOR_USE_TIMER(n)     for(int i = 0; i < (n); i++){}
#define POLLING_TIMEOUT     (0xFFFFFFFF)
#endif /* IMX500LIB_TIMER_DISABLE */

#define CMD_INT_TRANS_CMD               (0x00)  /* Set DD_ST_TRANS_CMD */
#define TRNS_CMD_LOADER                 (0x00)  /* Standby(Program empty) -> Standby(w/o Network) [Loader load] */
#define TRNS_CMD_MAINFW                 (0x01)  /* Standby(Program empty) -> Standby(w/o Network) [MainFW load] */
#define TRNS_CMD_NETWK                  (0x02)  /* Standby(w/o Network) -> Standby(w/ Network) */
#define LOAD_MODE_FLASH                 (0x01)  /* Flash load */
#define IMAGE_TYPE_LOADER               (0x00)
#define IMAGE_TYPE_MAINFW               (0x01)
#define IMAGE_TYPE_NETWK                (0x02)
#define CMD_RPLY_STS_TRNS_DONE          (0x01)
#define DWP_AP_CPOINT_MODE_EN           (0x01)
#define STREAM_START                    (0x01)
#define STREAM_STOP                     (0x00)
#define SW_RESET                        (0x01)


namespace imx500 {
    namespace control {

        /* structures */
        typedef struct tagControlInfo {
            SensoStatus     m_status;
            uint8_t         m_refStsCnt;
        } ControlInfo;

        /* static variables */
        static ControlInfo s_controlInfo = {SENSOR_INIT, 0};

        /* static function */
        static E_RESULT_CODE FlashBootKick(uint8_t trnsCmd, uint8_t imgType, uint32_t flashAddr);
        static E_RESULT_CODE DisplayVersion();

#ifdef EXEC_ON_SSP300_ENV
        /**
         * @brief init. (for SSP300)
         */
        void init(void* pDrvObj) {

            DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][CTRL] init \n");
            i2c_access::I2cOpen(pDrvObj);
        }
#else /*  EXEC_ON_SSP300_ENV */
        /* Raspi */
        void init(void* pDrvObj) {

            DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][CTRL] init \n");
            i2c_access::I2cOpen(NULL);
        }
#endif /*  EXEC_ON_SSP300_ENV */
        
        
        /**
         * @brief open.
         */
        E_RESULT_CODE open(BootParam* param) {

            E_RESULT_CODE return_code;
            IMX500SF_ModuleHandle handle;
            DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][CTRL] open\n");

            /* Check paaram */
            if (param == NULL) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] Failed open(invalid param)\n");
                return E_INVALID_PARAM;
            }

            /* Check status */
            if (s_controlInfo.m_status != SENSOR_INIT) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] Failed open(invalid state) : current state=0x%02X\n", s_controlInfo.m_status);
                return E_INVALID_STATE;
            }

            /* Set INCK */
            return_code = param_manager::SetInck(param->m_inckFreq, param->m_mipiDataRate, param->m_imgOnlyEna, param->m_flashType);
            if (return_code != E_OK) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] Failed open(set INCK error) : ret=0x%08X\n", return_code);
                return return_code;
            }

            if(param->m_imgOnlyEna == 0) {

                if (param->m_bootMode == 0) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][CTRL] Flash Boot\n");
                    /* --- boot start ----------------------------------------- */
                    s_controlInfo.m_refStsCnt = 0;
                    /* loader */
                    DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][CTRL] Flash Boot LoaderFW\n");
                    return_code = FlashBootKick(TRNS_CMD_LOADER, IMAGE_TYPE_LOADER, param->m_flashAddrLoader);
                    if (return_code != E_OK) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] Failed open(loader load error) : ret=0x%08X\n", return_code);
                        return return_code;
                    }
                    /* mainfw */
                    DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][CTRL] Flash Boot MainFW\n");
                    return_code = FlashBootKick(TRNS_CMD_MAINFW, IMAGE_TYPE_MAINFW, param->m_flashAddrMainFw);
                    if (return_code != E_OK) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] Failed open(mainfw load error) : ret=0x%08X\n", return_code);
                        return return_code;
                    }
                    /* netwk */
                    DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][CTRL] Flash Boot NetworkWeights\n");
                    return_code = FlashBootKick(TRNS_CMD_NETWK, IMAGE_TYPE_NETWK, param->m_flashAddrNetWk);
                    if (return_code != E_OK) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] Failed open(netwk load error) : ret=0x%08X\n", return_code);
                        return return_code;
                    }
                    /* --- boot end ------------------------------------------- */

                } else if (param->m_bootMode == 1) {

                    DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][CTRL] SPI Boot\n");
                    /* Open driver */
                    return_code = imageloader::Imx500SF_Open(&handle);
                    if (return_code != E_OK) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] Error driver open(ret=0x%2X)\n", return_code);
                        return E_DRIVER_ERROR;
                    }
                    /* Check param */
                    if ((param->m_imageSizeLoader == 0) || (param->m_imageSizeMainFw == 0) || (param->m_imageSizeNetWk == 0)) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] Failed open(invalid imageSize=%d, %d, %d)\n",
                                param->m_imageSizeLoader,
                                param->m_imageSizeMainFw,
                                param->m_imageSizeNetWk);
                        return E_INVALID_PARAM;
                    }
                    if ((param->m_pImageDataLoader == NULL) || (param->m_pImageDataMainFw == NULL) || (param->m_pImageDataNetWk == NULL)) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] Failed open(invalid Address =%d, %d, %d)\n",
                                (int32_t)param->m_pImageDataLoader,
                                (int32_t)param->m_pImageDataMainFw,
                                (int32_t)param->m_pImageDataNetWk);
                        return E_INVALID_PARAM;
                    }
                    
                    /* Load loader FW */
                    return_code = imageloader::Imx500SF_SpiBoot_LoaderFW(handle, param->m_pImageDataLoader, param->m_imageSizeLoader);
                    if (return_code != E_OK) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] Error SPI boot loader FW(ret=0x%2X)\n", return_code);
                        return E_DEVICE_ERROR;
                    }

                    /* Load Main FW */
                    return_code = imageloader::Imx500SF_SpiBoot_MainFW(handle, param->m_pImageDataMainFw, param->m_imageSizeMainFw);
                    if (return_code != E_OK) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] Error SPI boot main FW(ret=0x%2X)\n", return_code);
                        return E_DEVICE_ERROR;
                    }

                    /* Load Network Weights */
                    return_code = imageloader::Imx500SF_SpiBoot_NetworkWeights(handle, param->m_pImageDataNetWk, param->m_imageSizeNetWk);
                    if (return_code != E_OK) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] Error SPI boot network weight(ret=0x%2X)\n", return_code);
                        return E_DEVICE_ERROR;
                    }

                    /* Close driver */
                    return_code = imageloader::Imx500SF_Close(handle);
                    if (return_code != E_OK) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] Error driver close(ret=0x%2X)\n", return_code);
                        return E_DRIVER_ERROR;
                    }
                }

                if(param->m_flashUpdateEna == 1) {

                    DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][CTRL] FLASH_UPDATE \n");
                    /* Open driver */
                    return_code = imageloader::Imx500SF_Open(&handle);
                    if (return_code != E_OK) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] Error driver open(ret=0x%2X)\n", return_code);
                        return E_DRIVER_ERROR;
                    }

                    return_code = imageloader::Imx500SF_FlashUpdate_LoaderFW(handle,param->m_flashAddrLoader, param->m_pImageDataLoader, param->m_imageSizeLoader);
                    if (return_code != E_OK) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] Error Flash Update boot loader FW(ret=0x%2X)\n", return_code);
                        return E_DEVICE_ERROR;
                    }
                    
                    return_code = imageloader::Imx500SF_FlashUpdate_MainFW(handle,param->m_flashAddrMainFw, param->m_pImageDataMainFw, param->m_imageSizeMainFw);
                    if (return_code != E_OK) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] Error Flash Update Main FW(ret=0x%2X)\n", return_code);
                        return E_DEVICE_ERROR;
                    }
                    
                    return_code = imageloader::Imx500SF_FlashUpdate_NetworkWeights(handle,param->m_flashAddrNetWk, param->m_pImageDataNetWk, param->m_imageSizeNetWk);
                    if (return_code != E_OK) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] Error Flash Update NetworkWeights (ret=0x%2X)\n", return_code);
                        return E_DEVICE_ERROR;
                    }
                    /* Close driver */
                    return_code = imageloader::Imx500SF_Close(handle);
                    if (return_code != E_OK) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] Error driver close(ret=0x%2X)\n", return_code);
                        return E_DRIVER_ERROR;
                    }
                }

                /* Dewarp Contro Point */
                if (param->m_pImageDataDwpCp != NULL) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][CTRL] SPI LOAD DWP_CP \n");

                    /* Open driver */
                    return_code = imageloader::Imx500SF_Open(&handle);
                    if (return_code != E_OK) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] Error driver open(ret=0x%2X)\n", return_code);
                        return E_DRIVER_ERROR;
                    }

                    return_code = imageloader::Imx500SF_MainFwUpdate_Spi(handle, param->m_pImageDataDwpCp, param->m_imageSizeDwpCp);
                    if (return_code != E_OK) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] Error SPI Update DWP CP(ret=0x%2X)\n", return_code);
                        return E_DEVICE_ERROR;
                    }

                    /* Close driver */
                    return_code = imageloader::Imx500SF_Close(handle);
                    if (return_code != E_OK) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] Error driver close(ret=0x%2X)\n", return_code);
                        return E_DRIVER_ERROR;
                    }

                    /* Set DWP_AP_CPOINT_MODE */
                    I2C_ACCESS_WRITE(REG_ADDR_DWP_AP_CPOINT_MODE, DWP_AP_CPOINT_MODE_EN, REG_SIZE_DWP_AP_CPOINT_MODE);
                }
            }

            /* display version information */
            return_code = DisplayVersion();
            if (return_code != E_OK) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] Error display version(ret=0x%2X)\n", return_code);
                return E_DRIVER_ERROR;
            }

            /* state transition */
            s_controlInfo.m_status = SENSOR_STNDBY;

            return E_OK;
        }

        /**
         * @brief start.
         */
        E_RESULT_CODE start() {

            DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][CTRL] start\n");

            /* Check status */
            if (s_controlInfo.m_status != SENSOR_STNDBY) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] Failed start(invalid state) : current state=0x%02X\n", s_controlInfo.m_status);
                return E_INVALID_STATE;
            }

            /* Set MODE_SEL */
            I2C_ACCESS_WRITE(REG_ADDR_MODE_SEL, STREAM_START, REG_SIZE_MODE_SEL);

            /* state transition */
            s_controlInfo.m_status = SENSOR_STREAM;

            return E_OK;
        }

        /**
         * @brief stop.
         */
        E_RESULT_CODE stop() {

            DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][CTRL] stop\n");

            /* Check status */
            if (s_controlInfo.m_status != SENSOR_STREAM) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] Failed stop(invalid state) : current state=0x%02X\n", s_controlInfo.m_status);
                return E_INVALID_STATE;
            }

            /* Set MODE_SEL */
            I2C_ACCESS_WRITE(REG_ADDR_MODE_SEL, STREAM_STOP, REG_SIZE_MODE_SEL);

            /* state transition */
            s_controlInfo.m_status = SENSOR_STNDBY;

            return E_OK;
        }

        /**
         * @brief close.
         */
        E_RESULT_CODE close() {

            DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[IMX500LIB][CTRL] close\n");

            /* Check status */
            if (s_controlInfo.m_status != SENSOR_STNDBY) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] Failed close(invalid state) : current state=0x%02X\n", s_controlInfo.m_status);
                return E_INVALID_STATE;
            }

            /* Set SW Reset */
            I2C_ACCESS_WRITE(REG_ADDR_SW_RESET, SW_RESET, REG_SIZE_SW_RESET);
            SENSOR_USE_TIMER(10000000);  /* wait 10ms */

            /* state transition */
            s_controlInfo.m_status = SENSOR_INIT;

            return E_OK;
        }

        /**
         * @brief get_status.
         */
        SensoStatus get_status() {

            return s_controlInfo.m_status;
        }

        /**
         * @brief FlashBootKick.
         */
        static E_RESULT_CODE FlashBootKick(uint8_t trnsCmd, uint8_t imgType, uint32_t flashAddr) {

            E_RESULT_CODE return_code = E_OK;
            uint32_t rcv_data = 0;
            uint32_t timer_cnt = 0;

            /* set parameter */
            I2C_ACCESS_WRITE(REG_ADDR_DD_ST_TRANS_CMD, trnsCmd, REG_SIZE_DD_ST_TRANS_CMD);
            I2C_ACCESS_WRITE(REG_ADDR_DD_LOAD_MODE, LOAD_MODE_FLASH, REG_SIZE_DD_LOAD_MODE);
            I2C_ACCESS_WRITE(REG_ADDR_DD_IMAGE_TYPE, imgType, REG_SIZE_DD_IMAGE_TYPE);
            I2C_ACCESS_WRITE(REG_ADDR_DD_FLASH_ADDR, flashAddr, REG_SIZE_DD_FLASH_ADDR);

            /* set command (notify IMX500) */
            I2C_ACCESS_WRITE(REG_ADDR_DD_CMD_INT, CMD_INT_TRANS_CMD, REG_SIZE_DD_CMD_INT);

            /* wait change status */
            while (timer_cnt < POLLING_TIMEOUT) {
                I2C_ACCESS_READ(REG_ADDR_DD_REF_STS_REG, &rcv_data, REG_SIZE_DD_REF_STS_REG);
                if (rcv_data != s_controlInfo.m_refStsCnt) {
                    break;
                }
                SENSOR_USE_TIMER(10000000);  /* wait 10ms */
                timer_cnt++;
            }
            if (timer_cnt >= POLLING_TIMEOUT) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] boot error(timeout): img type=%d\n", imgType);
                return E_DEVICE_ERROR;
            }

            /* update ref status count */
            s_controlInfo.m_refStsCnt = static_cast<uint8_t>(rcv_data);

            /* check reply status */
            I2C_ACCESS_READ(REG_ADDR_DD_CMD_REPLY_STS, &rcv_data, REG_SIZE_DD_CMD_REPLY_STS);
            if (rcv_data != CMD_RPLY_STS_TRNS_DONE) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][CTRL] boot error(reply status NG): img type=%d, reply status=0x%02X\n", imgType, rcv_data);
                return_code = E_DEVICE_ERROR;
            }

            return return_code;
        }

        /**
         * @brief DisplayVersion.
         */
        static E_RESULT_CODE DisplayVersion() {

            E_RESULT_CODE return_code = E_OK;
            uint32_t tmp_ver;
            uint32_t device_id[16];

            DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "\n-----------------------------------------------------------\n");
            DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "IMX500 version information\n");
            DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "-----------------------------------------------------------\n");

            /* ES version */
            DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "ES ver                : ");
            I2C_ACCESS_READ(REG_ADDR_ES_VERSION, &tmp_ver, REG_SIZE_ES_VERSION);
            if (tmp_ver == 0) {
                DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "undefined\n");
            }
            else if (tmp_ver == 0x10) {
                DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "MP\n");
            }
            else {
                DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "ES%d\n", tmp_ver);
            }

            /* Device ID */
            DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "Device ID             : ");
            for (uint8_t i = 0; i < 16; i++) {
                I2C_ACCESS_READ(REG_ADDR_DEVICE_ID, &device_id[i], REG_SIZE_DEVICE_ID);
                DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "%02X", device_id[i]);
                if (((i & 3) == 3) && (i != 15)) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "-");
                }
            }
            DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "\n");

            /* Loader version */
            I2C_ACCESS_READ(REG_ADDR_LOADER_VERSION, &tmp_ver, REG_SIZE_LOADER_VERSION);
            DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "Loader ver            : %02X.%02X.%02X\n", (tmp_ver >> 16) & 0xFF, (tmp_ver >> 8) & 0xFF, tmp_ver & 0xFF);

            /* Main FW version */
            I2C_ACCESS_READ(REG_ADDR_MAIN_FW_VERSION, &tmp_ver, REG_SIZE_MAIN_FW_VERSION);
            DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "Main FW ver           : %02X.%02X.%02X\n", (tmp_ver >> 16) & 0xFF, (tmp_ver >> 8) & 0xFF, tmp_ver & 0xFF);

            /* SCPU version */
            I2C_ACCESS_READ(REG_ADDR_SCPU_FW_VERSION, &tmp_ver, REG_SIZE_SCPU_FW_VERSION);
            DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "    + SCPU ver        : %02X.%02X.%02X\n", (tmp_ver >> 16) & 0xFF, (tmp_ver >> 8) & 0xFF, tmp_ver & 0xFF);

            /* DCPU version */
            I2C_ACCESS_READ(REG_ADDR_DCPU_FW_VERSION, &tmp_ver, REG_SIZE_DCPU_FW_VERSION);
            DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "    + DCPU ver        : %02X.%02X.%02X\n", (tmp_ver >> 16) & 0xFF, (tmp_ver >> 8) & 0xFF, tmp_ver & 0xFF);

            /* ICPU version */
            I2C_ACCESS_READ(REG_ADDR_ICPU_FW_VERSION, &tmp_ver, REG_SIZE_ICPU_FW_VERSION);
            DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "    + ICPU ver        : %02X.%02X.%02X\n", (tmp_ver >> 16) & 0xFF, (tmp_ver >> 8) & 0xFF, tmp_ver & 0xFF);

            /* CRAM Param version */
            I2C_ACCESS_READ(REG_ADDR_CRAM_PARAM_VERSION, &tmp_ver, REG_SIZE_CRAM_PARAM_VERSION);
            DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "    + CRAM Param ver  : %02X.%02X.%02X\n", (tmp_ver >> 16) & 0xFF, (tmp_ver >> 8) & 0xFF, tmp_ver & 0xFF);

            /* DWP CP version */
            I2C_ACCESS_READ(REG_ADDR_DWP_CP_VERSION, &tmp_ver, REG_SIZE_DWP_CP_VERSION);
            DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "    + DWP CP ver      : %02X.%02X.%02X\n", (tmp_ver >> 16) & 0xFF, (tmp_ver >> 8) & 0xFF, tmp_ver & 0xFF);

            /* Network ID */
            I2C_ACCESS_READ(REG_ADDR_NETWORK_ID, &tmp_ver, REG_SIZE_NETWORK_ID);
            DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "Network ID            : %02X%02X%02X\n", (tmp_ver >> 16) & 0xFF, (tmp_ver >> 8) & 0xFF, tmp_ver & 0xFF);

            /* Model version */
            I2C_ACCESS_READ(REG_ADDR_MODEL_VERSION, &tmp_ver, REG_SIZE_MODEL_VERSION);
            DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "Model ver             : %02X.%02X\n", (tmp_ver >> 8) & 0xFF, tmp_ver & 0xFF);

            /* Converter version */
            I2C_ACCESS_READ(REG_ADDR_CONVERTER_VERSION, &tmp_ver, REG_SIZE_CONVERTER_VERSION);
            DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "Converter ver         : %02X.%02X.%02X\n", (tmp_ver >> 16) & 0xFF, (tmp_ver >> 8) & 0xFF, tmp_ver & 0xFF);

            /* AP Params revision */
            I2C_ACCESS_READ(REG_ADDR_AP_PARAMS_REVISION, &tmp_ver, REG_SIZE_AP_PARAMS_REVISION);
            DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "    + AP Param rev    : %02X\n", tmp_ver & 0xFF);

            /* DNN Params revision */
            I2C_ACCESS_READ(REG_ADDR_DNN_PARAMS_REVISION, &tmp_ver, REG_SIZE_DNN_PARAMS_REVISION);
            DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "    + DNN Param rev   : %02X\n", tmp_ver & 0xFF);

            /* CFG Blob1 revision */
            I2C_ACCESS_READ(REG_ADDR_CFG_BLOB1_REVISION, &tmp_ver, REG_SIZE_CFG_BLOB1_REVISION);
            DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "    + CFG Blob1 rev   : %02X\n", tmp_ver & 0xFF);

            /* CFG Blob2 revision */
            I2C_ACCESS_READ(REG_ADDR_CFG_BLOB2_REVISION, &tmp_ver, REG_SIZE_CFG_BLOB2_REVISION);
            DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "    + CFG Blob2 rev   : %02X\n", tmp_ver & 0xFF);

            /* SDSP-A revision */
            I2C_ACCESS_READ(REG_ADDR_SDSP_A_REVISION, &tmp_ver, REG_SIZE_SDSP_A_REVISION);
            DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "    + SDSP-A rev      : %02X\n", tmp_ver & 0xFF);

            /* SDSP-C revision */
            I2C_ACCESS_READ(REG_ADDR_SDSP_C_REVISION, &tmp_ver, REG_SIZE_SDSP_C_REVISION);
            DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "    + SDSP-C rev      : %02X\n", tmp_ver & 0xFF);

            /* Network Weights revision */
            I2C_ACCESS_READ(REG_ADDR_NETWORK_WEIGHTS_REVISION, &tmp_ver, REG_SIZE_NETWORK_WEIGHTS_REVISION);
            DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "    + Net Weights rev : %02X\n", tmp_ver & 0xFF);

            DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "-----------------------------------------------------------\n\n");
            return return_code;
        }

    } /* namespace control */
} /* namespace imx500 */
