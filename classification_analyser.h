/*
 * Copyright 2017-2018 Sony Semiconductor Solutions Corporation.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
 * Solutions Corporation.
 * No part of this file may be copied, modified, sold, and distributed in any
 * form or by any means without prior explicit permission in writing from
 * Sony Semiconductor Solutions Corporation.
 *
 */

#ifndef CLASSIFICATION_ANALYSER_H
#define CLASSIFICATION_ANALYSER_H

#include "output_tensor_parser.h"

#define NUM_OF_CLASSES 1001
namespace imx500 {
	namespace classification_analyser {
		
		struct ClassificationItem {
			int index = 0;
			float score = 0;
		};

		struct ClassificationData {
			std::vector<ClassificationItem> v_classItem;
		};


		int32_t analyseClassificationOutput(const struct imx500::classification_interface::ClassificationOutputTensor, struct ClassificationData* outputCLassificationData, uint16_t numOfScores);

    }
}// namespace classification_analyser
#endif //CLASSIFICATION_ANALYSER_H
