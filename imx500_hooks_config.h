/*
* Copyright 2018-2019 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/

#ifndef IMX500_HOOKS_CONFIG_H_
#define IMX500_HOOKS_CONFIG_H_

#include <stdint.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

struct hooks_config {
    char computerVisionModelsFolder[1023];
    int bSaveRawTensorOutputFiles;
    char rawTensorSavePath[1023];
    int displayImageExtension;
    int bDoFlashUpdate;
    int frameRate;
};

extern struct hooks_config HooksConfig;

#ifdef __cplusplus
}
#endif


#endif
