/*
* Copyright 2018-2019 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/

#ifndef IMX500_HOOKS_H_
#define IMX500_HOOKS_H_

#include <stdint.h>
#include <stdio.h>
#include <vector>
#include <stdlib.h>
#include <string>

#ifdef __cplusplus
extern "C" {
#endif

namespace IMX500Hooks {

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Constants
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    //Options to determine what format the frame for an image will be saved in and the format of the raw image bytes that are handed to the callback
    enum E_DISPLAY_IMAGE_EXTENSION {
        E_JPEG,
        E_PNG,
        E_BMP
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Custom Objects
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    //System configuration object
    struct Config {
        //The folder under the "image" directory where the network.fpk file is located. E.g., image/{VALUE}/network.fpk
        char computerVisionModelsFolder[1023];
        //If true, raw output tensor files will be saved
        bool bSaveRawTensorOutputFiles;
        //Where the raw tensor files will be saved
        char rawTensorSavePath[1023];
        //If true, the computer vision models specified by computerVisionModelsFolder will be loaded to the flash memory of the IMX500
        bool bDoFlashUpdate;
        //The format of all saved images captured from the frame, as well as the format of the raw bytes handed back to "Hooks::onFrame"
        E_DISPLAY_IMAGE_EXTENSION displayImageExtension;
        //The maximum number of frames per second that the IMX500 will capture. Valid range: 1 ~ 30
        int frameRate;
    };    

    //Representation of an object detected by the IMX500
    struct DetectedObject {
        //ID of the object
        int id;
        //How confident the IMX500 was that the detection was accurate 0.0 ~ 0.999. This is the highest confidence scored for this object in the current frame.
        float confidence;
    };
    
    //Frame information formatted for use by the end user
    struct ImageData {
        //Height of the frame image in pixels
        float height;
        //Width of the frame image in pixels
        float width;
        //A list of objects detected by the IMX500 this frame
        std::vector<DetectedObject> detectedObjects;
        //The raw image bytes from this frame. The format of this image is determined by Config::displayImageExtension
        std::vector<u_char> imageBytes;
    };

    typedef struct {
        int id;
        std::string label;
    }trained_object;
    extern std::vector<trained_object> TrainedObjects;

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Callback
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    //Base class for the end user's callbacks
    class Hooks {
        public:

        //Base class use only! DO NOT TOUCH.
        static Hooks* instance;

        /**
         * @brief Callback for the end user once the frame has been processed and its information
         * parsed into an ImageData object.
         * 
         * @param imgData The parsed image data from the frame
        */
        virtual void onFrame(ImageData imgData);
        
        /**
         * @brief SDK USE ONLY! Provides the instance of the end user's app so we can trigger their
         * callback.
         * 
         * @return The end user's callback app instance.
        */
        static Hooks* getInstance() { return instance; };

        protected:
        //Constructor
        Hooks();
    };
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Public Methods
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * @brief Sets all of the system configuration variables using the argument. The end user will call
     * this at the start of the application.
     * 
     * @param config A configuration object. See the type definition for more information.
    */
    void setConfig(struct Config config);
    /**
     * @brief When the IMX500 performs object detection on a frame, it does so by cropping a portion
     * of it's field of view and performing image detection on it. This method gets the position
     * and size of the crop.
     * 
     * @param leftEdgePixelPosition The number of pixels from the left edge of the full size image
     * where the crop begins.
     * @param topEdgePixelPosition The number of pixels from the top edge of the full size image
     * where the crop begins.
     * @param width The width of the crop
     * @param height The height of the crop
     * 
    */
    void getCurrentDetectionCropSizeAndPosition(uint32_t* leftEdgePixelPosition, uint32_t *topEdgePixelPosition, uint32_t *width, uint32_t *height);
    /**
     * @brief When the IMX500 performs object detection on a frame, it does so by cropping a portion
     * of it's field of view and performing image detection on it. This method adjusts the position
     * and size of the crop. NOTE: Regardless of the size of the crop, the cropped image will be 
     * compressed to 300 x 300 pixels; cropping too large of a region will cause loss of image 
     * integrity and negatively impact detection.
     * 
     * @param leftEdgePixelPosition The number of pixels from the left edge of the full size image
     * where the crop should begin.
     * @param topEdgePixelPosition The number of pixels from the top edge of the full size image
     * where the crop should begin.
     * @param width The width of the crop
     * @param height The height of the crop
     * 
    */
    void adjustDetectionCropSizeAndPosition(uint32_t leftEdgePixelPosition, uint32_t topEdgePixelPosition, uint32_t width, uint32_t height);

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Properties
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    //See struct Config for definition of values returned from these properties
    bool getConfig_bSaveRawTensorOutputFiles();
    E_DISPLAY_IMAGE_EXTENSION getConfig_displayImageExtension();
};



#ifdef __cplusplus
}
#endif


#endif
