/*
* Copyright 2018-2019 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/

#ifndef DETECTION_HANDLER_H_
#define DETECTION_HANDLER_H_

#include <stdint.h>
#include <stdio.h>
#include <vector>

#include "../imx500_hooks.h"
namespace BXDExample {
    namespace DetectionHandler {

        /**
         * @brief Parses through the list of objects in the detections argument and 
         * stores those that have a high enough confidence score. After 30 frames (or 
         * when CropROI::isImageCropFinished returns true), the collected information will 
         * be output to the console.
         * 
         * @param detections List of objects detected in the current frame
        */
        void handleDetections(std::vector<IMX500Hooks::DetectedObject> detections);
    }
}

#endif
