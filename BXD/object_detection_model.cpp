/*
* Copyright 2018-2019 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/

#include <stdint.h>
#include <stdio.h>
#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <ctime>

#include "TextTable.h"
#include "object_detection_model.h"
#include "config.h"
#include "helpers.h"
#include "image_handler.h"

static bool IsFirstCycle = true;
namespace BXDExample {
    namespace DetectionHandler {
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        // Constructor
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        ObjectDetectionModel::ObjectDetectionModel(int id, std::string label, float confidence) {
            this->id = id;
            this->label = label;
            this->wasDetectedThisCycle = false;
            this->numberOfCyclesDetectedIn = 0;
            this->highestConfidenceThisCycle = 0;
            this->lowestConfidenceRecorded = 0;
            this->highestConfidenceRecorded = 0;
            this->numberOfTimesDetected = 0;
            this->handleDetection(confidence);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        // Public Methods
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        void ObjectDetectionModel::handleDetection(float confidence) {
            if (!wasDetectedThisCycle) {
                wasDetectedThisCycle = true;
                numberOfCyclesDetectedIn++;
            }
            if (confidence > highestConfidenceThisCycle) {
                highestConfidenceThisCycle = confidence;
            }
            mostRecentConfidenceRecorded = confidence;
            if (lowestConfidenceRecorded == 0 || confidence < lowestConfidenceRecorded) {
                lowestConfidenceRecorded = confidence;
            }
            if (confidence > highestConfidenceRecorded) {
                highestConfidenceRecorded = confidence;
            }
            numberOfTimesDetected++;
            if (last10Confidences.size() == 10) {
                last10Confidences.erase(last10Confidences.begin());
            }
            last10Confidences.push_back(confidence);
            
            addEntryToCSV();
        }

        float ObjectDetectionModel::getHighestConfidenceThisCycle() {
            return highestConfidenceThisCycle;
        }

        int ObjectDetectionModel::getID() {
            return id;
        }

        void ObjectDetectionModel::onCycleFinished() {
            wasDetectedThisCycle = false;
            highestConfidenceThisCycle = 0;
        }

        std::string ObjectDetectionModel::toJSON() {
            std::ostringstream ss;
            ss << "{\"id\":" << id << ",\"label\":\"" << label << "\",\"confidence\":" << highestConfidenceThisCycle << "}";
            return ss.str();
        }

        void ObjectDetectionModel::addToTableOutput(TextTable *t) {
            std::ostringstream ss;
            //id
            ss << id;
            t->add( ss.str() );
            ss.str(std::string());

            //label
            t->add(label);

            //times detected
            ss << numberOfCyclesDetectedIn;
            t->add( ss.str() );
            ss.str(std::string());

            //min confidence
            ss << lowestConfidenceRecorded;
            t->add( ss.str() );
            ss.str(std::string());

            //avg confidence
            float averageConfidence = 0;
            if (last10Confidences.size() > 0) {
                float totalConfidence = 0.0f;
                for (int i = 0; i < (int)last10Confidences.size(); i++) {
                    totalConfidence += last10Confidences[i];
                }
                averageConfidence = totalConfidence / last10Confidences.size();
            }
            ss << averageConfidence;
            t->add( ss.str() );
            ss.str(std::string());

            //max confidence
            ss << highestConfidenceRecorded;
            t->add( ss.str() );
            ss.str(std::string());

            //last confidence
            ss << mostRecentConfidenceRecorded;
            t->add( ss.str() );
            ss.str(std::string());

            t->endOfRow();
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        // Private Methods
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        void ObjectDetectionModel::addEntryToCSV() {
            if (Config::ObjectDetection::OD_LogToCSV) {
                char filename[ ] = "OBJECT_DETECTION_LOG.csv";
                std::fstream fs;
                fs.open(filename, std::fstream::in | std::fstream::out | std::fstream::app);
                
                if (IsFirstCycle) {
                    IsFirstCycle = false;
                    if (fs) {
                        // If file exists from our last session, nuke it. No sense in saving history
                        system("rm OBJECT_DETECTION_LOG.csv");
                    }
                }
                
                if (!fs) {
                    // Creat a fresh copy if we don't have one for this cycle
                    fs.open(filename, std::fstream::in | std::fstream::out | std::fstream::trunc);
                }

                std::string imagePath = "N/A";
                Helpers::E_IMAGE_TYPE imageType = ImageHandler::willSaveNonStitchedImageThisFrame();
                if (imageType != Helpers::E_IMAGE_TYPE_NONE) {
                    char pn[2000];
                    getImagePathAndName(imageType, pn);
                    imagePath = std::string(pn);
                }

                auto t = std::time(nullptr);
                auto tm = *std::localtime(&t);

                fs 
                    << std::put_time(&tm, "%m/%d/%Y %H:%M:%S") << ","
                    << std::to_string(id) << ","
                    << label << ","
                    << std::to_string(mostRecentConfidenceRecorded) << ","
                    << imagePath
                    << "\r\n";
                fs.close();
            }

        }
    }
}
