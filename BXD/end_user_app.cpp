/*
* Copyright 2018-2019 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/

#include <stdint.h>
#include <iostream>
#include <string.h>
#include <cstring>
#include <vector>
#include <algorithm>
#include <chrono>
#include <fstream>
#include <map>
#include "end_user_app.h"
#include "imx500_hooks.h"
#include "detection_handler.h"
#include "config.h"
#include "crop_roi.h"
#include "image_handler.h"
#include "helpers.h"

namespace BXDExample {

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Definitions For Exern Declarations
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    namespace Config {
        namespace ObjectDetection {
            enum object_detection_type OD_Type = E_OD_TYPE_PEOPLE;
            enum object_detection_output_type OD_OutputType = E_OD_OUTPUT_TYPE_NONE;
            float OD_ConfidenceThresholdDefault = 0.8f;
            std::vector<ids_to_detect> OD_IdsToDetect;
            bool OD_LogToCSV = false;
        }
        enum video_output_type VideoOutputType = E_VIDEO_OUTPUT_TYPE_NONE;
        enum image_save_format ImageSaveFormat = E_IMAGE_SAVE_FORMAT_JPG;
        bool SaveLowResFullImage = false;
        std::string SaveLowResFullImage_FolderPath = "";
        ushort SaveLowResFullImage_FrameRate = 10;
        bool SaveLowResFullImage_FirstOnly = false;
        namespace ROICrop {
            bool ROICropOn = false;
            bool SaveStitchedImage = false;
            std::string SaveStitchedImage_FolderPath = "";
            ushort SaveStitchedImage_CycleRate = 3;
            bool SaveStitchedImage_FirstOnly = false;
            bool SaveImageSet = false;
            std::string SaveImageSet_FolderPath = "";
            ushort SaveImageSet_CycleRate = 3;
            bool SaveImageSet_FirstOnly = false;
        }
    }

    namespace Helpers {
        uint64_t FRAME_COUNTER = 0;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Class Definition
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    class EndUserApp: public IMX500Hooks::Hooks {

        private:

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        // Members
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        bool didInitCrop = false;

        public:

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        // Constructor
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        EndUserApp() : Hooks() {
            this->setConfig();
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        // Base Class Callbacks
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        void onFrame(IMX500Hooks::ImageData imageData) override {
            Helpers::FRAME_COUNTER++;            
            if (BXDExample::Config::ROICrop::ROICropOn) {
                if (!didInitCrop) {
                    BXDExample::CropROI::updateCropValues();
                    didInitCrop = true;
                } else {
                    BXDExample::CropROI::crop();
                    BXDExample::ImageHandler::displayAndSaveImage(imageData);
                    BXDExample::DetectionHandler::handleDetections(imageData.detectedObjects);
                }
            } else {
                BXDExample::ImageHandler::displayAndSaveImage(imageData);
                BXDExample::DetectionHandler::handleDetections(imageData.detectedObjects);
            }
        }
        
        private:

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        // Private methods
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        void setConfig() {
            FILE * fp;
            int res;
            char getstr[128] = {0};
            char equal[2] = {0};
            char value[1023] = {0};        
            fp = fopen("./BXD/end_user_app_config.txt","rb");
            struct IMX500Hooks::Config config;
            std::string key, val;
            while((res = fscanf(fp,"%s %s %1022s",getstr,equal,value))!= EOF) {
                key = std::string(getstr);
                val = std::string(value);
                if (key == "SAVE_RAW_TENSOR_OUTPUT") {
                    config.bSaveRawTensorOutputFiles = val == "ON";
                }
                if (key == "SAVE_RAW_TENSOR_OUTPUT_FOLDER_PATH") {
                    unsigned int len = strlen(value);
                    memset(config.rawTensorSavePath, 0, sizeof(config.rawTensorSavePath));
                    if (len + 1 <= sizeof(config.rawTensorSavePath)) {
                        strncpy(config.rawTensorSavePath, value, sizeof(config.rawTensorSavePath) - 1);
                    }
                }
                else if (key == "COMPUTER_VISION_MODELS_FOLDER") {
                    unsigned int len = strlen(value);
                    memset(config.computerVisionModelsFolder, 0, sizeof(config.computerVisionModelsFolder));
                    if (len + 1 <= sizeof(config.computerVisionModelsFolder)) {
                        strncpy(config.computerVisionModelsFolder, value, sizeof(config.computerVisionModelsFolder) - 1);
                    }
                }
                else if (key == "LOAD_NEW_COMPUTER_VISION_MODELS") {
                    config.bDoFlashUpdate = val == "ON";
                }
                else if (key == "FRAMES_PER_SECOND") {
                    config.frameRate = std::stoi(val);
                }
                else if (key == "OBJECT_DETECTION_TYPE") {
                    if (val == "PEOPLE") {
                        Config::ObjectDetection::OD_Type = Config::ObjectDetection::E_OD_TYPE_PEOPLE;
                    } else {//PRODUCT
                        Config::ObjectDetection::OD_Type = Config::ObjectDetection::E_OD_TYPE_PRODUCT;
                    }
                }
                else if (key == "OBJECT_DETECTION_OUTPUT_TYPE") {
                    if (val == "JSON") {
                        Config::ObjectDetection::OD_OutputType = Config::ObjectDetection::E_OD_OUTPUT_TYPE_JSON;
                    }
                    else if (val == "TABLE") {
                        Config::ObjectDetection::OD_OutputType = Config::ObjectDetection::E_OD_OUTPUT_TYPE_TABLE;
                    } else {
                        Config::ObjectDetection::OD_OutputType = Config::ObjectDetection::E_OD_OUTPUT_TYPE_NONE;
                    }
                }
                else if (key == "OBJECT_DETECTION_CONFIDENCE_THRESHOLD_DEFAULT") {
                    Config::ObjectDetection::OD_ConfidenceThresholdDefault = std::stof(val) * 0.01f;
                }
                else if (key == "OBJECT_DETECTION_IDS_TO_DETECT") {
                    if (val != "ALL") {
                        std::vector<std::string> parts = Helpers::splitString(val, ',');
                        std::vector<std::string> partsInner;
                        for (int i = 0; i < (int)parts.size(); i++) {
                            partsInner = Helpers::splitString(parts[i], '|');
                            Config::ObjectDetection::OD_IdsToDetect.push_back({std::stoi(partsInner[0]), std::stof(partsInner[1])});
                        }
                    }
                }
                else if (key == "OBJECT_DETECTION_LOG_TO_CSV") {
                    Config::ObjectDetection::OD_LogToCSV = val == "ON";
                }
                else if (key == "VIDEO_OUTPUT") {
                    if (val == "DIRECT") {
                        Config::VideoOutputType = Config::E_VIDEO_OUTPUT_TYPE_DIRECT;
                    }
                    else if (val == "STITCHED") {
                        Config::VideoOutputType = Config::E_VIDEO_OUTPUT_TYPE_STITCHED;
                    }
                    else if (val == "BOTH") {
                        Config::VideoOutputType = Config::E_VIDEO_OUTPUT_TYPE_BOTH;
                    } else {
                        Config::VideoOutputType = Config::E_VIDEO_OUTPUT_TYPE_NONE;
                    }
                }
                else if (key == "IMAGE_SAVE_FORMAT") {
                    if (val == "BMP") {
                        config.displayImageExtension = IMX500Hooks::E_BMP;
                        Config::ImageSaveFormat = Config::E_IMAGE_SAVE_FORMAT_BMP;
                    }
                    else if (val == "PNG") {
                        config.displayImageExtension = IMX500Hooks::E_PNG;
                        Config::ImageSaveFormat = Config::E_IMAGE_SAVE_FORMAT_PNG;
                    } else {//JPG
                        config.displayImageExtension = IMX500Hooks::E_JPEG;
                        Config::ImageSaveFormat = Config::E_IMAGE_SAVE_FORMAT_JPG;
                    }
                }
                else if (key == "SAVE_LOW_RES_FULL_IMAGE") {
                    Config::SaveLowResFullImage = val == "ON";
                }
                else if (key == "SAVE_LOW_RES_FULL_IMAGE_FOLDER_PATH") {
                    Config::SaveLowResFullImage_FolderPath = val;
                }
                else if (key == "SAVE_LOW_RES_FULL_IMAGE_FRAME_RATE") {
                    Config::SaveLowResFullImage_FrameRate = std::stoi(val);
                }
                else if (key == "SAVE_LOW_RES_FULL_IMAGE_FIRST_ONLY") {
                    Config::SaveLowResFullImage_FirstOnly = val == "ON";
                }
                else if (key == "ROI_CROP") {
                    Config::ROICrop::ROICropOn = val == "ON";
                }
                else if (key == "ROI_CROP_SAVE_STITCHED_IMAGE") {
                    Config::ROICrop::SaveStitchedImage = val == "ON";
                }
                else if (key == "ROI_CROP_SAVE_STITCHED_IMAGE_FOLDER_PATH") {
                    Config::ROICrop::SaveStitchedImage_FolderPath = val;
                }
                else if (key == "ROI_CROP_SAVE_STITCHED_IMAGE_CYCLE_RATE") {
                    Config::ROICrop::SaveStitchedImage_CycleRate = std::stoi(val);
                }
                else if (key == "ROI_CROP_SAVE_STITCHED_IMAGE_FIRST_CYCLE_ONLY") {
                    Config::ROICrop::SaveStitchedImage_FirstOnly = val == "ON";
                }
                else if (key == "ROI_CROP_SAVE_IMAGE_SET") {
                    Config::ROICrop::SaveImageSet = val == "ON";
                }
                else if (key == "ROI_CROP_SAVE_IMAGE_SET_FOLDER_PATH") {
                    Config::ROICrop::SaveImageSet_FolderPath = val;
                }
                else if (key == "ROI_CROP_SAVE_IMAGE_SET_CYCLE_RATE") {
                    Config::ROICrop::SaveImageSet_CycleRate = std::stoi(val);
                }
                else if (key == "ROI_CROP_SAVE_IMAGE_SET_FIRST_CYCLE_ONLY") {
                    Config::ROICrop::SaveImageSet_FirstOnly = val == "ON";
                }
            }
            fclose(fp);
            IMX500Hooks::setConfig(config);

            if (Config::ObjectDetection::OD_Type == Config::ObjectDetection::E_OD_TYPE_PEOPLE) {
                fp = fopen("./BXD/trainedObjects_People.txt","rb");
            } else {
                fp = fopen("./BXD/trainedObjects_Product.txt","rb");
            }
            
            while((res = fscanf(fp,"%s",getstr))!= EOF) {
                std::vector<std::string> parts = Helpers::splitString(std::string(getstr), '|');
                IMX500Hooks::TrainedObjects.push_back({std::stoi(parts[0]),parts[1]});
            }
            fclose(fp);
        }
        
    };
}

static BXDExample::EndUserApp* instance = new BXDExample::EndUserApp;