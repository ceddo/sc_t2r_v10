#include <vector>
#include <string>
#include <iostream>
#include <sys/stat.h>
#include <errno.h>
#include <chrono>
#include <regex>

#include "helpers.h"
#include "config.h"
#include "image_handler.h"

namespace BXDExample {
    namespace Helpers {
        const std::string SESSION_TIME_STAMP = std::to_string(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count());

        std::vector<std::string> splitString(std::string str, char delimiter) {
            std::string s = std::string(str);
            std::string delim = std::string(1, delimiter);
            size_t pos = 0;
            std::string token;
            std::vector<std::string> parts;
            
            while ((pos = s.find(delim)) != std::string::npos) {
                parts.push_back(s.substr(0, pos));
                s.erase(0, pos + delim.length());
            }
            //Because the strings never end with the delimiter, always add the last string
            parts.push_back(s.substr(0, s.length()));
            
            return parts;
        }

        //S_IRWXU | S_IRGRP |  S_IXGRP | S_IROTH | S_IXOTH
        int createDirectoryIfNotExist(char *path) {
            int status = 1;
            if (std::string(path) != "." && std::string(path) != "./") {
                char* p = path;
                //S_IRWXU: Write permission bit for the owner of the file. Usually 0200. S_IWRITE is an obsolete synonym provided for BSD compatibility.
                //S_IRGRP: Read permission bit for the group owner of the file. Usually 040.
                //S_IXGRP: Execute or search permission bit for the group owner of the file. Usually 010.
                //S_IROTH: Read permission bit for other users. Usually 04.
                //S_IXOTH: Execute or search permission bit for other users. Usually 01.
                //mode_t mode = S_IRWXU | S_IRGRP |  S_IXGRP | S_IROTH | S_IXOTH;
                mode_t mode = S_IRWXU | S_IRGRP | S_IROTH;

                // Do mkdir for each slash until end of string or error
                while (*p != '\0') {
                    // Skip first character
                    p++;

                    // Find first slash or end
                    while(*p != '\0' && *p != '/') p++;

                    // Remember value from p
                    char v = *p;

                    // Write end of string at p
                    *p = '\0';

                    // Create folder from path to '\0' inserted at p
                    if(mkdir(path, mode) == -1 && errno != EEXIST) {
                        *p = v;
                        status = 0;
                        break;
                    }

                    // Restore path to it's former glory
                    *p = v;
                }
            }
            return status;
        }

        void getImagePathAndName(enum E_IMAGE_TYPE imageType, char *rett) {
            char path[2000];
            getImagePath(imageType, path);
            std::string ret = std::string(path);

            std::string ext = "";
            
            switch (Config::ImageSaveFormat) {
                case Config::E_IMAGE_SAVE_FORMAT_JPG: ext = ".jpg"; break;
                case Config::E_IMAGE_SAVE_FORMAT_PNG: ext = ".png"; break;
                case Config::E_IMAGE_SAVE_FORMAT_BMP:
                default:
                    ext = ".bmp";
                    break;
            }
            std::string imagePrefix;
            switch (imageType) {
                case E_IMAGE_TYPE_SAVE_LOW_RES_FULL_IMAGE:
                    imagePrefix = Config::SaveLowResFullImage_FirstOnly ? "LOW_RES_FULL_IMAGE_FIRST_" : "LOW_RES_FULL_IMAGE_";
                    ret += "/" + imagePrefix + SESSION_TIME_STAMP + "_" + std::to_string(FRAME_COUNTER) + ext;
                    break;
                case E_IMAGE_TYPE_ROI_CROP_SAVE_STITCHED_IMAGE:
                    ret += "/ROI_CROP_SAVE_STITCHED_IMAGE_" + SESSION_TIME_STAMP + "_" + std::to_string(FRAME_COUNTER) + ext;
                    break;
                case E_IMAGE_TYPE_ROI_CROP_SAVE_IMAGE_SET:
                    ret += "/ROI_CROP_IMAGE_" + SESSION_TIME_STAMP + "_" + std::to_string(FRAME_COUNTER) + ext;
                    break;
                default: break;
            }

            snprintf(rett, ret.length() + 1, ret.c_str());
        }

        void getImagePath(enum E_IMAGE_TYPE imageType, char *rett) {
            std::string ret = "";
            std::string imagePrefix;
            switch (imageType) {
                case E_IMAGE_TYPE_SAVE_LOW_RES_FULL_IMAGE:
                    ret = Config::SaveLowResFullImage_FolderPath;
                    break;
                case E_IMAGE_TYPE_ROI_CROP_SAVE_STITCHED_IMAGE:
                    ret = Config::ROICrop::SaveStitchedImage_FolderPath;
                    break;
                case E_IMAGE_TYPE_ROI_CROP_SAVE_IMAGE_SET:
                    imagePrefix = "roiCropImageSetForCycle_" + std::to_string(ImageHandler::getDetectionCycleCount()) + "_" + SESSION_TIME_STAMP;
                    ret = Config::ROICrop::SaveImageSet_FolderPath + "/" + imagePrefix;
                    break;
                default: break;
            }

            snprintf(rett, ret.length() + 1, ret.c_str());
        }

    }
}
