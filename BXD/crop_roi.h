/*
* Copyright 2018-2019 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/

#ifndef CROP_ROI_H_
#define CROP_ROI_H_

#include <stdint.h>
#include <stdio.h>
#include <string>
#include <vector>

namespace BXDExample {
    namespace CropROI {

        /**
         * @brief Updates all of the constants for crop roi with the values stored in crop_config.txt.
        */
        void updateCropValues();

        /**
         * @brief Method to generate all of the coordinates for the ROI and to tell the IMX500 to move its
         * ROI to one of the specified coordinates.
        */
        void crop();
        
        /**
         * @brief If we are in cropping mode, product/people_detect.cpp will not print their 
         * detection results unless this method returns true.
         * 
         * @return True if we have scanned the whole field of view of the IMX500
        */
        bool isImageCropFinished();

        /**
         * @brief Convenience method to know if the ROI moved since the last frame.
         * 
         * @return True if the ROI moved.
        */
        bool getDidROIMove();
    }
}

#endif
