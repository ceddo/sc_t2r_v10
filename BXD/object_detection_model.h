/*
* Copyright 2018-2019 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/

#ifndef OBJECT_DETECTION_MODEL_H_
#define OBJECT_DETECTION_MODEL_H_

#include <stdint.h>
#include <stdio.h>
#include <string>
#include <vector>

#include "TextTable.h"
namespace BXDExample {
    namespace DetectionHandler {
        class ObjectDetectionModel {
            public:
            ////////////////////////////////////////////////////////////////////////////////////////////////////
            //
            // Constructor
            //
            ////////////////////////////////////////////////////////////////////////////////////////////////////
            ObjectDetectionModel(int id, std::string label, float confidence);

            ////////////////////////////////////////////////////////////////////////////////////////////////////
            //
            // Public Properties
            //
            ////////////////////////////////////////////////////////////////////////////////////////////////////
            float getHighestConfidenceThisCycle();
            int getID();

            ////////////////////////////////////////////////////////////////////////////////////////////////////
            //
            // Public Methods
            //
            ////////////////////////////////////////////////////////////////////////////////////////////////////
            /**
             * @brief Method to update and store all of the members related to confidence. Confidence thresholds
             * must be checked before this method is called, as this class does not handle thresholds.
             * 
             * @param confidence A percentage value denoting the accuracy of this detection
            */
            void handleDetection(float confidence);

            /**
             * @brief Call once a detection cycle has been completed. Will reset the values that are detection
             * cycle dependant.
            */
            void onCycleFinished();

            /**
             * @brief Converts this object to a JSON string in the format expected by the electron app.
            */
            std::string toJSON();

            /**
             * @brief Converts this object to table format and adds those converted values to a TextTable
             * object.
             * 
             * @param t The TextTable object to add this object to
            */
            void addToTableOutput(TextTable *t);

            private:
            ////////////////////////////////////////////////////////////////////////////////////////////////////
            //
            // Private Methods
            //
            ////////////////////////////////////////////////////////////////////////////////////////////////////
            /**
             * @brief If CREATE_DETECTION_TO_IMAGE_CSV is set to on in config.txt, this will create the .csv
             * file if it doesn't exist already and add a row representing this object along with the location
             * of the image that it was saved in. This method does nothing if SAVE_DNN_IMAGE in config.txt is
             * not set to ROI or BOTH, or if no ROI image was saved this frame.
            */
            void addEntryToCSV();

            ////////////////////////////////////////////////////////////////////////////////////////////////////
            //
            // Private Members
            //
            ////////////////////////////////////////////////////////////////////////////////////////////////////
            int id;
            std::string label;
            bool wasDetectedThisCycle;
            float highestConfidenceThisCycle;
            int numberOfCyclesDetectedIn;
            float lowestConfidenceRecorded;
            float highestConfidenceRecorded;
            float mostRecentConfidenceRecorded;
            int numberOfTimesDetected;
            std::vector<float> last10Confidences;
        };
    }
}

#endif
