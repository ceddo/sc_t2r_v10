/*
* Copyright 2018-2019 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <map>
#include <time.h>

#include "detection_handler.h"
#include "crop_roi.h"
#include "TextTable.h"
#include "object_detection_model.h"
#include "config.h"
#include "helpers.h"
#include "../imx500_hooks.h"

namespace BXDExample {
    namespace DetectionHandler {
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        // Members
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        std::map<int, IMX500Hooks::trained_object> mapTrainedObjects;
        std::map<int, Config::ObjectDetection::ids_to_detect> mapObservableObject;
        std::map<int, DetectionHandler::ObjectDetectionModel*> detectedObjects;
        bool didMapObjects = false;
        bool bHaveObservableObjects = false;
        int numFramesThisCycle = 0;
        clock_t theTime = clock();
        int cyclesProcessed = 0;

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        // Private Method Prototypes
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        void onDetectionCycleFinished();
        void mapObjects();
        bool isConfidenceHighEnough(int id, float confidence);
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        // Public Methods
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        void handleDetections(std::vector<IMX500Hooks::DetectedObject> detections) {
            if (!didMapObjects) {
                mapObjects();
                didMapObjects = true;
            }

            numFramesThisCycle++;

            for (int j = 0; j < (int)detections.size(); j++) {
                int id = detections[j].id;
                if (isConfidenceHighEnough(id, detections[j].confidence)) {                
                    //The index for a people object is 0. We don't care about elephants or anything else...
                    if (Config::ObjectDetection::OD_Type == Config::ObjectDetection::E_OD_TYPE_PEOPLE && id != 0) { continue; }
                    if (detectedObjects.find(id) == detectedObjects.end()) {
                        detectedObjects[id] = new ObjectDetectionModel(id, mapTrainedObjects[id].label, detections[j].confidence);
                    } else {
                        detectedObjects[id]->handleDetection(detections[j].confidence);
                    }
                }
            }
            if (Config::ROICrop::ROICropOn) {
                if (CropROI::isImageCropFinished()) {
                    onDetectionCycleFinished();
                }
            }
            else if (Helpers::FRAME_COUNTER % OBJECT_DETECTION_NUMBER_OF_FRAMES_PER_CYCLE_NON_ROI_CROP == 0) {
                onDetectionCycleFinished();
            }   
        }
    
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        // Private Methods
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /**
         * @brief Called when a detection cycle has been completed. Outputs the results of the detection
         * cycle to the console in the format specified in config.txt by DETECTION_RESULTS_OUTPUT_TYPE.
         * Calls onCycleFinished() on each object in the detectedObjects map.
        */
        void onDetectionCycleFinished() {
            if (Config::ObjectDetection::OD_OutputType != Config::ObjectDetection::E_OD_OUTPUT_TYPE_NONE) {
                std::cout << std::flush;
                if (Config::ObjectDetection::OD_OutputType == Config::ObjectDetection::E_OD_OUTPUT_TYPE_JSON) {
                    std::ostringstream ss;
                    ss << "{\"detections\":[";
                    std::vector<std::string> detections;
                    for (auto const& x : detectedObjects) {
                        if (isConfidenceHighEnough(detectedObjects[x.first]->getID(), detectedObjects[x.first]->getHighestConfidenceThisCycle())) {
                            detections.push_back(detectedObjects[x.first]->toJSON());
                        }
                        detectedObjects[x.first]->onCycleFinished();
                    }
                    for (int i = 0; i < (int)detections.size(); i++) {
                        ss << detections[i];
                        if (i + 1 < (int)detections.size()) {
                            ss << ",";
                        }
                    }
                    ss << "]}\n";
                    printf(ss.str().c_str());
                } 
                else if (Config::ObjectDetection::OD_OutputType == Config::ObjectDetection::E_OD_OUTPUT_TYPE_TABLE) {
                    system("clear");
                    TextTable t( '-', '|', '+' );
                    t.add( "ID" );
                    t.add( "Label" );
                    t.add( "Times Detected");
                    t.add( "Lowest Confidence");
                    t.add( "Average Confidence");
                    t.add( "Highest Confidence");
                    t.add( "Last Confidence");
                    t.endOfRow();
                    for (auto const& x : detectedObjects) {
                        detectedObjects[x.first]->addToTableOutput(&t);
                        detectedObjects[x.first]->onCycleFinished();
                    }
                    t.setAlignment( 7, TextTable::Alignment::RIGHT );
                    std::cout << "Seconds To Finish This Cycle: " << (float(clock() - theTime)/ CLOCKS_PER_SEC) << std::endl;
                    std::cout << "Frames Processed This Cycle: " << numFramesThisCycle << std::endl;
                    std::cout << "Number of Cycles Processed: " << (++cyclesProcessed) << std::endl;        
                    std::cout << t;
                }
                std::cout << std::flush;
            } else {
                for (auto const& x : detectedObjects) {
                    detectedObjects[x.first]->onCycleFinished();
                }
            }
            numFramesThisCycle = 0;
            theTime = clock();
        }

        /**
        * @brief Packages all of the objects specified by config.txt's OBSERVABLE_OBJECT_IDS into a
        * map.
        */
        void mapObjects() {
            if ((int)Config::ObjectDetection::OD_IdsToDetect.size() > 0) {
                bHaveObservableObjects = true;
                for (int i = 0; i < (int)Config::ObjectDetection::OD_IdsToDetect.size(); i++) {
                    mapObservableObject[Config::ObjectDetection::OD_IdsToDetect[i].id] = Config::ObjectDetection::OD_IdsToDetect[i];
                }
            }

            for (int i = 0; i < (int)IMX500Hooks::TrainedObjects.size(); i++) {
                mapTrainedObjects[IMX500Hooks::TrainedObjects[i].id] = IMX500Hooks::TrainedObjects[i];
            }
        }

        /**
        * @brief Abstract method to perform the checks required to know if a detected object should 
        * have its handleDetection method triggered.
        */
        bool isConfidenceHighEnough(int id, float confidence) {
            bool observableObjectsCheck = false;
            bool standardConfidenceCheck = false;
            if (Config::ObjectDetection::OD_Type == Config::ObjectDetection::E_OD_TYPE_PEOPLE || !bHaveObservableObjects) {        
                standardConfidenceCheck = confidence > Config::ObjectDetection::OD_ConfidenceThresholdDefault;
            } else {
                observableObjectsCheck = 
                    mapObservableObject.find(id) != mapObservableObject.end() && 
                    confidence > mapObservableObject[id].confidenceThreshold;
            }

            return observableObjectsCheck || standardConfidenceCheck;
        }
    }
}


