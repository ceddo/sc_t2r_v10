/*
* Copyright 2018-2019 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/

#ifndef HELPERS_H_
#define HELPERS_H_

#include <vector>
#include <string>

#define OBJECT_DETECTION_NUMBER_OF_FRAMES_PER_CYCLE_NON_ROI_CROP 30

namespace BXDExample {
    namespace Helpers {

        extern uint64_t FRAME_COUNTER;

        enum E_IMAGE_TYPE {
            E_IMAGE_TYPE_SAVE_LOW_RES_FULL_IMAGE,
            E_IMAGE_TYPE_ROI_CROP_SAVE_STITCHED_IMAGE,
            E_IMAGE_TYPE_ROI_CROP_SAVE_IMAGE_SET,
            E_IMAGE_TYPE_NONE
        };

        /**
        * @brief Splits the given C string into a two dimentional char array, splitting by
        * the dilimiter character.
        * 
        * @param str The string to be split
        * @param delimiter The character to split the string on
        * 
        * @return Object containing an array of C strings produced by breaking up the original
        * by the delimiter and a count of the total number of substrings that were created.
        */
        std::vector<std::string> splitString(std::string str, char delimiter);

        /**
        * @brief Creates a directory or directory path (if needed) in the current working 
        * directory if that if it does not yet exist.
        * 
        * @param path Can be a folder name or a path to a folder
        * 
        * @return 1 if successful
        */
        int createDirectoryIfNotExist(char *path);

        /**
        * @brief Generates a string for the image name and path for the image type matching
        * the imageType argument.
        * 
        * @param imageType The type of image to get a path for
        * @param rett Point to the char[] to populate with the path
        */
        void getImagePathAndName(enum E_IMAGE_TYPE imageType, char *rett);

        /**
        * @brief Generates a string for the image path for the image type matching
        * the imageType argument.
        * 
        * @param imageType The type of image to get a path for
        * @param rett Point to the char[] to populate with the path
        */
        void getImagePath(enum E_IMAGE_TYPE imageType, char *rett);

    }
}

#endif
