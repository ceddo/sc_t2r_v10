/*
* Copyright 2018-2019 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/

#include <stdint.h>
#include <string>
#include <iostream>
#include <vector>
#include <fstream>

#include "crop_roi.h"
#include "../imx500_hooks.h"

namespace BXDExample {
    namespace CropROI {

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        // Constants
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        bool showVideo = false;               // A cmd-line arg that shows the frames real-time.
        uint32_t FULL_WIDTH = 4056;           // 4056x3040
        uint32_t FULL_HEIGHT = 3040;          // 4056x3040
        uint32_t HSTART = 1700;               // Cropping area start x-coordinate
        uint32_t VSTART = 1100;               // Cropping area start y-coordinate
        uint32_t HEND = FULL_WIDTH - HSTART;  // Cropping area end x-coordinate
        uint32_t VEND = FULL_HEIGHT - VSTART; // Cropping area end y-coordinate
        uint32_t H_INC = 100;                 // Horizontal increment
        uint32_t V_INC = 100;                 // Vertical increment
        uint32_t WIDTH = 400;                 // Crop area WIDTHxHEIGHT
        uint32_t HEIGHT = 400;                // See above comment
        uint32_t FRAMES_PER_CROP = 3;         // How many frames per each crop area are fed to DNN before moving on
        const std::string CROP_CONFIG = "./BXD/crop_config.txt";
        uint32_t HLEN = 1;
        uint32_t VLEN = 1;
        uint32_t NUM_CROP_POINTS = 1;

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        // Members
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        static uint32_t cropCounter = 0;
        static uint32_t frame_counter = 0;
        static std::vector<uint32_t> hhOffsets;
        static std::vector<uint32_t> vvOffsets;
        static bool didROIMove = false;

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        // Private Methods
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        std::vector<std::pair<std::string, std::string>> readCropConfig() {
            std::ifstream file(CROP_CONFIG);
            std::string line;
            std::vector<std::pair<std::string, std::string>> keyVals;
            if (file.is_open()) {
                while (getline(file, line)) {
                    auto index = line.find('=');
                    std::pair<std::string, std::string> keyVal;
                    if (index != std::string::npos) {
                        keyVal = std::make_pair(
                            line.substr(0, index),
                            line.substr(index + 1));

                        while (!keyVal.second.empty() && keyVal.second.front() == ' ') {
                            keyVal.second.erase(0, 1);
                        }
                        keyVals.push_back(keyVal);
                    }
                }
                file.close();
            }
            return keyVals;
        }
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        // Public Methods
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        void updateCropValues() {
            std::vector<std::pair<std::string, std::string>> cropValues = readCropConfig();
            std::cout << std::endl
                    << "Cropping Values:" << std::endl;
            for (auto keyVal : cropValues) {
                if (keyVal.first == "HSTART") {
                    HSTART = static_cast<uint32_t>(std::stoul(keyVal.second));
                    std::cout << "HSTART: " << HSTART << std::endl;
                }
                else if (keyVal.first == "VSTART") {
                    VSTART = static_cast<uint32_t>(std::stoul(keyVal.second));
                    std::cout << "VSTART: " << VSTART << std::endl;
                }
                else if (keyVal.first == "H_INC") {
                    H_INC = static_cast<uint32_t>(std::stoul(keyVal.second));
                    std::cout << "H_INC: " << H_INC << std::endl;
                }
                else if (keyVal.first == "V_INC") {
                    V_INC = static_cast<uint32_t>(std::stoul(keyVal.second));
                    std::cout << "V_INC: " << V_INC << std::endl;
                }
                else if (keyVal.first == "WIDTH") {
                    WIDTH = static_cast<uint32_t>(std::stoul(keyVal.second));
                    std::cout << "WIDTH: " << WIDTH << std::endl;
                }
                else if (keyVal.first == "HEIGHT") {
                    HEIGHT = static_cast<uint32_t>(std::stoul(keyVal.second));
                    std::cout << "HEIGHT: " << HEIGHT << std::endl;
                }
                else if (keyVal.first == "FRAMES_PER_CROP") {
                    FRAMES_PER_CROP = static_cast<uint32_t>(std::stoul(keyVal.second));
                    std::cout << "FRAMES_PER_CROP: " << FRAMES_PER_CROP << std::endl;
                }
            }
            std::cout << std::endl
                    << "End Cropping Values" << std::endl;
            //Recalculate these values, but offset them by the increment amount so the box doesn't go off screen
            HEND = (FULL_WIDTH - HSTART) - WIDTH;
            VEND = (FULL_HEIGHT - VSTART) - HEIGHT;
            HLEN = (HEND - HSTART) / H_INC + 1;
            VLEN = (VEND - VSTART) / V_INC + 1;
            NUM_CROP_POINTS = HLEN * VLEN;
            crop();
        }

        void crop() {
            if (frame_counter == 0) {
                std::vector<uint32_t> hOffsets(NUM_CROP_POINTS, 0);
                std::vector<uint32_t> vOffsets(NUM_CROP_POINTS, 0);
                hhOffsets = hOffsets;
                vvOffsets = vOffsets;

                for (uint32_t j = 0; j < HLEN; j++) {
                    for (uint32_t i = 0; i < VLEN; i++) {
                        hhOffsets[(VLEN * j) + i] = HSTART + H_INC * j;
                        vvOffsets[(VLEN * j) + i] = VSTART + V_INC * i;
                    }
                }
                IMX500Hooks::adjustDetectionCropSizeAndPosition(hhOffsets[0], vvOffsets[0], WIDTH, HEIGHT);
            }
            frame_counter++;

            if (frame_counter % FRAMES_PER_CROP == 0) {
                IMX500Hooks::adjustDetectionCropSizeAndPosition(hhOffsets[cropCounter], vvOffsets[cropCounter], WIDTH, HEIGHT);
                cropCounter++;
                cropCounter = cropCounter % NUM_CROP_POINTS;
                didROIMove = true;
            } else {
                didROIMove = false;
            }
        }

        bool isImageCropFinished() {
            return cropCounter == 0 && frame_counter % FRAMES_PER_CROP == 0;
        }

        bool getDidROIMove() {
            return didROIMove;
        }
    }
}
