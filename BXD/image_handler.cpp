/*
* Copyright 2018-2019 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <sys/time.h>
#include <ctime>
#include <vector>
#include <inttypes.h>
#include <sys/statvfs.h>

#include "image_handler.h"
#include "helpers.h"
#include "crop_roi.h"
#include "config.h"
#include "../imx500_hooks.h"

#define LABELS_FILE_PRODUCT     "BXD/class_definition_file/Product/labels.txt"
#define LABELS_FILE_PEOPLE      "BXD/class_definition_file/People/labels.txt"
#define LABEL_FILE_NAME         "class_definition_file/class80.txt"

namespace BXDExample {
    namespace ImageHandler {
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        // Members
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        static bool didFinishFirstCropSet = false;
        static bool didSaveFirstLowResFull = false;
        static int detectionCycleCount = 0;
        //FPS Members
        static struct timeval prev;
        static double sum = 0.0;
        static uint32_t count = 0;
        //SSDMOBILENET Only
        std::vector<std::string> label_name;

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        // Logic Wrappers
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /**
         * @brief Logic abstraction for direct video
         * 
         * @return True if video output setting is direct or both
        */
        bool bDisplayDirectVideo() {
            return Config::VideoOutputType == Config::E_VIDEO_OUTPUT_TYPE_DIRECT || Config::VideoOutputType == Config::E_VIDEO_OUTPUT_TYPE_BOTH;
        }

        /**
         * @brief Logic abstraction for stitched video. Always false if roi crop is off.
         * 
         * @return True if roi crop is on, and the crop region moved (since we don't want duplicate images),
         * and video output setting is stiched or both.
        */
        bool bDisplayStichedVideo() {
            return
            Config::ROICrop::ROICropOn && 
            (
                Config::VideoOutputType == Config::E_VIDEO_OUTPUT_TYPE_BOTH ||
                Config::VideoOutputType == Config::E_VIDEO_OUTPUT_TYPE_STITCHED
            ) &&
            CropROI::getDidROIMove();
        }

        /**
         * @brief Logic abstraction for low res full image. Always false if roi crop is on.
         * 
         * @return True if roi crop is off, and either: a) We're only saving the first image and we haven't saved
         * it yet or b) we're not only saving the first image, we're supposed to save the low res full image, and
         * the rate at which we should be saving them corresponds to the current frame number.
        */
        bool bSaveLowResFullImage() {
            bool status = false;
            if (!Config::ROICrop::ROICropOn) {
                if (Config::SaveLowResFullImage_FirstOnly) {
                    status = !didSaveFirstLowResFull;
                } else {
                    status = Config::SaveLowResFullImage &&
                    Helpers::FRAME_COUNTER % Config::SaveLowResFullImage_FrameRate == 0;
                }
            }
            return status;
        }

        /**
         * @brief Logic abstraction for saving first crop image set. Always false if roi crop is off.
         * 
         * @return True if roi crop is on, we're supposed save the first crop set, we haven't been through
         * an entire crop set yet, and the crop region moved (prevents duplicate images).
        */
        bool bSaveFirstCropImageSet() {
            return 
            Config::ROICrop::SaveImageSet_FirstOnly && 
            Config::ROICrop::ROICropOn && 
            !didFinishFirstCropSet && 
            CropROI::getDidROIMove();
        }

        /**
         * @brief Logic abstraction for saving crop image set. Always false if roi crop is off.
         * 
         * @return True if roi crop is on, we're supposed to be saving image crop sets, we're NOT
         * supposed to save ONLY the first crop set, the crop region moved (prevents duplicate images),
         * and this is the first detection cycle or the rate at which we should save image crop sets
         * divides evenly into the current detection cycle number.
        */
        bool bSaveCropImageSet() {
            return 
            Config::ROICrop::SaveImageSet &&
            !Config::ROICrop::SaveImageSet_FirstOnly &&
            Config::ROICrop::ROICropOn && 
            (
                detectionCycleCount == 0 ||
                detectionCycleCount % Config::ROICrop::SaveImageSet_CycleRate == 0
            ) && 
            CropROI::getDidROIMove();
        }

        /**
         * @brief Logic abstraction for saving the first stiched image. Always false if roi crop is off.
         * 
         * @return True if roi crop is on, we're supposed to save the first stitched image, we haven't
         * finished the first crop set yet, and the image crop region moved (prevents duplicate images).
        */
        bool bSaveFirstStitchedImage() {
            return 
            Config::ROICrop::SaveStitchedImage_FirstOnly && 
            Config::ROICrop::ROICropOn && 
            !didFinishFirstCropSet &&
            CropROI::getDidROIMove();
        }

        /**
         * @brief Logic abstraction for saving stiched image. Always false if roi crop is off.
         * 
         * @return True if roi crop is on, we're supposed to save stitched images, we're NOT
         * supposed to save ONLY the first stitched image, the image crop region moved (prevents
         * duplicate images), and this is the first detection cycle or the rate at which we should save
         * stitched images divides evenly into the current detection cycle number.
        */
        bool bSaveStichedImage() {
            return 
            Config::ROICrop::SaveStitchedImage &&
            !Config::ROICrop::SaveStitchedImage_FirstOnly &&
            Config::ROICrop::ROICropOn && 
            (
                detectionCycleCount == 0 ||
                detectionCycleCount % Config::ROICrop::SaveStitchedImage_CycleRate == 0
            ) && 
            CropROI::getDidROIMove();
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        // Private Methods
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /**
         * @brief Abstract method to update didFinishFirstCropSet and detectionCycleCount. Because this
         * needs to be done whether or not we will do anything with images this frame but at different
         * times, respectively, this logic was lifted out to avoid repeating code.
        */
        void updateGlobalVars() {
            if (Config::ROICrop::ROICropOn) {
                if (!didFinishFirstCropSet) {
                    didFinishFirstCropSet = CropROI::isImageCropFinished();
                }
                detectionCycleCount += CropROI::isImageCropFinished() ? 1 : 0;
            } else {
                detectionCycleCount += Helpers::FRAME_COUNTER % OBJECT_DETECTION_NUMBER_OF_FRAMES_PER_CYCLE_NON_ROI_CROP == 0 ? 1 : 0;
            }
        }

        /**
         * @brief Abstract method to remove the 50 oldest files from a directory of a specified extension
         * and file prefix.
         * 
         * @param ext The file extension of the files to be removed
         * @param dir The directory from which the files will be removed
         * @param filePrefix The prefix of the file that precedes the .ext
        */
        void delete50FilesOfExtFromDirectory(std::string ext, std::string dir, std::string filePrefix) {
            std::string cmd;
            int delCount = 0;
            cmd = "cd " + dir + "/;rm \"$(ls -t " + filePrefix + "." + ext + " | tail -1)\"";
            delCount = 0;
            while (delCount++ < 50) { if (system(cmd.c_str()) != 0) { break; } }
        }

        /**
         * @brief Checks the remaining hard drive free space percentage. If there is less than 10% free
         * space remaining, each active image output directory described by config.txt will have its
         * 50 oldest items deleted.
        */
        void checkAndMakeHDDSpace() {
            struct statvfs driveInfo;
            statvfs("/", &driveInfo);
            double percentageRemaining = 1.0f - (driveInfo.f_bavail / driveInfo.f_blocks);
            if (percentageRemaining < .1f) {
                //We've only got 10% of the hard drive left, so lets get rid of the top 50 oldest files from all of their save directories
                if (IMX500Hooks::getConfig_bSaveRawTensorOutputFiles()) {
                    delete50FilesOfExtFromDirectory("bin", ".", "output*");
                }
                
                std::string ext;
                switch (Config::ImageSaveFormat) {
                    case Config::E_IMAGE_SAVE_FORMAT_JPG: ext = "jpg"; break;
                    case Config::E_IMAGE_SAVE_FORMAT_PNG: ext = "png"; break;
                    case Config::E_IMAGE_SAVE_FORMAT_BMP:
                    default:
                        ext = "bmp";
                        break;
                }
                
                if (Config::SaveLowResFullImage || Config::SaveLowResFullImage_FirstOnly) {
                    delete50FilesOfExtFromDirectory(ext, Config::SaveLowResFullImage_FolderPath, "*");
                }
                if (Config::ROICrop::SaveStitchedImage || Config::ROICrop::SaveStitchedImage_FirstOnly) {
                    delete50FilesOfExtFromDirectory(ext, Config::ROICrop::SaveStitchedImage_FolderPath, "*");
                }
                if (Config::ROICrop::SaveImageSet || Config::ROICrop::SaveImageSet_FirstOnly) {
                    std::string cmd;
                    int delCount = 0;
                    cmd = "cd " + Config::ROICrop::SaveImageSet_FolderPath + "/;rm -rf \"$(ls -t | tail -1)\"";
                    delCount = 0;
                    while (delCount++ < 10) { if (system(cmd.c_str()) != 0) { break; } }
                }
            }
        }
        
        /**
         * @brief Saves a DNN image to a file.
         * 
         * @param resizeWin The image to be saved
         * @param roi The rectangle to resize the image into
         * @param folderPath The path to the folder in which to save the image
         * @param fileNamePrefix How the file will be named. The final file name will have the frame number
         * appended to it.
        */
        void saveDNNImage(IMX500Hooks::ImageData imageData, Helpers::E_IMAGE_TYPE imageType) {
            char path[2000];
            char pathAndName[2000];
            Helpers::getImagePath(imageType, path);
            Helpers::getImagePathAndName(imageType, pathAndName);
            if (Helpers::createDirectoryIfNotExist(path) == 1) {
                std::ofstream file;
                file.open(pathAndName, std::ios::out|std::ios::binary);
                file.write(imageData.imageBytes.size() ? (char*)&imageData.imageBytes[0] : 0, std::streamsize(imageData.imageBytes.size()));
                file.close();
            }
        }

        /**
         * @brief Driven by the config.txt flags SAVE_DNN_IMAGE, SAVE_DNN_IMAGE_FORMAT, DO_CROP_ROI, and
         * SAVE_DNN_IMAGE_ROI_RATE. Performs all of the necessary logic before writing this frame's image
         * to a file. If the logic is satisfied, one or more files will be written by calling saveDNNImage.
         * If the right checks are satisfied, then the ROI image is stored in a matrix and, once the crop
         * session is finished, all of the ROI images in the matrix are stitched together and saved as a
         * full-size image.
         * 
         * @param imageData The image to be saved
        */
        void doSaveDNNImageLogic(IMX500Hooks::ImageData imageData) {
            //The only way to get full res images is if CROP ROI is happening, otherwise there'll be nothing to stitch together
            if (bSaveFirstStitchedImage() || bSaveStichedImage()) {
                //NOTE: Create the stitched image variable at the top level so that we only need to go through stitching once.
                //Store the image in the matrix
                if (CropROI::isImageCropFinished()) {
                    //Save the full image. You can worry about the names when you implement this.
                }
            }

            if (bSaveLowResFullImage()) {
                saveDNNImage(imageData, Helpers::E_IMAGE_TYPE_SAVE_LOW_RES_FULL_IMAGE);
                didSaveFirstLowResFull = true;
            }

            if (bSaveFirstCropImageSet() || bSaveCropImageSet()) {
                saveDNNImage(imageData, Helpers::E_IMAGE_TYPE_ROI_CROP_SAVE_IMAGE_SET);
            }
        }
        
        /**
         * @brief Handles displaying the image for the current frame (roi image) and the stiched image (if
         * we are configured to do so in config.txt).
         * 
         * @param dnnrgb The roi image for the current frame
        */
        void displayImage(IMX500Hooks::ImageData imageData) {
            cv::Mat tmp(imageData.imageBytes);
            cv::Mat dnnrgb(cv::imdecode(tmp, 1));
            struct timeval now;
            gettimeofday(&now, NULL);
            double fps = 0;
            fps = 1000000.0 / static_cast<double>(((now.tv_sec - prev.tv_sec) * 1000000) + (now.tv_usec - prev.tv_usec));
            prev = now;
            count++;
            sum += fps;
            double ave = static_cast<double>(sum / count);
            char charfps[256];
            char charave[256];
            snprintf(charfps, sizeof(charfps), "FPS: %5.2f", fps);
            snprintf(charave, sizeof(charave), "AVE FPS = %5.3f [FPS]", ave);
            // Put fps information on the image
            cv::putText(dnnrgb, charfps, cv::Point(10, 30),cv::FONT_HERSHEY_SIMPLEX, 1, cv::Scalar(225, 225, 0), 2);
            cv::namedWindow("DNN ROI input", cv::WINDOW_KEEPRATIO | cv::WINDOW_NORMAL);
            cv::imshow("DNN ROI input", dnnrgb);
            cv::waitKey(10);
        }

        void displayFullImage(IMX500Hooks::ImageData imageData) {
            //Store the image in the matrix
            if (CropROI::isImageCropFinished()) {
                //If we're done with the crop cycle, stitch the image together, than display it
            //TODO:
                //Check to see if we have a stiched image already. If not, make one
                //Add all the same fps stuff or whatever to the stiched image
                //Show the stiched image
                //NOTE: If we're using ssdmobilenet or mobilenetv1, we'll probably wanna add it to the matrix AFTER we draw all of those boxes and stuff   
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        // Public Methods
        //
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        void displayAndSaveImage(IMX500Hooks::ImageData imageData) {
            if (
                !bDisplayDirectVideo() &&
                !bDisplayStichedVideo() &&
                !bSaveLowResFullImage() &&
                !bSaveFirstCropImageSet() &&
                !bSaveCropImageSet() &&
                !bSaveFirstStitchedImage() &&
                !bSaveStichedImage()
            ) {
                updateGlobalVars();
                //No display and no save, so bail
                return;
            }

            checkAndMakeHDDSpace();
            
            doSaveDNNImageLogic(imageData);

            if (bDisplayDirectVideo() || bDisplayStichedVideo()) {                
                if (bDisplayDirectVideo()) {
                    displayImage(imageData);
                }
                if (bDisplayStichedVideo()) {
                    displayFullImage(imageData);
                }
            }

            updateGlobalVars();
        }

        Helpers::E_IMAGE_TYPE willSaveNonStitchedImageThisFrame() {
            Helpers::E_IMAGE_TYPE ret = Helpers::E_IMAGE_TYPE_NONE;
            if (bSaveLowResFullImage()) {
                ret = Helpers::E_IMAGE_TYPE_SAVE_LOW_RES_FULL_IMAGE;
            }
            else if (bSaveFirstCropImageSet() || bSaveCropImageSet()) {
                ret = Helpers::E_IMAGE_TYPE_ROI_CROP_SAVE_IMAGE_SET;
            }
            return ret;
        }

        int getDetectionCycleCount() {
            return detectionCycleCount;
        }
    }
}
