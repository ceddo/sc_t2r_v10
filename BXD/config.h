/*
* Copyright 2018-2019 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/

#ifndef CONFIG_H_
#define CONFIG_H_

#include <stdint.h>
#include <stdio.h>
#include <string>
#include <vector>

namespace BXDExample {
	namespace Config {
		namespace ObjectDetection {

			enum object_detection_type {
				E_OD_TYPE_PEOPLE,
				E_OD_TYPE_PRODUCT
			};
			extern enum object_detection_type OD_Type;

			enum object_detection_output_type {
				E_OD_OUTPUT_TYPE_NONE,
				E_OD_OUTPUT_TYPE_JSON,
				E_OD_OUTPUT_TYPE_TABLE
			};
			extern enum object_detection_output_type OD_OutputType;

			extern float OD_ConfidenceThresholdDefault;

			typedef struct {
				int id;
				float confidenceThreshold;
			}ids_to_detect;
			extern std::vector<ids_to_detect> OD_IdsToDetect;

			extern bool OD_LogToCSV;
			
		}

		enum video_output_type {
			E_VIDEO_OUTPUT_TYPE_NONE,
			E_VIDEO_OUTPUT_TYPE_DIRECT,
			E_VIDEO_OUTPUT_TYPE_STITCHED,
			E_VIDEO_OUTPUT_TYPE_BOTH
		};
		extern enum video_output_type VideoOutputType;

		enum image_save_format {
			E_IMAGE_SAVE_FORMAT_JPG,
			E_IMAGE_SAVE_FORMAT_PNG,
			E_IMAGE_SAVE_FORMAT_BMP
		};
		extern enum image_save_format ImageSaveFormat;
		
		extern bool SaveLowResFullImage;
		extern std::string SaveLowResFullImage_FolderPath;
		extern ushort SaveLowResFullImage_FrameRate;
		extern bool SaveLowResFullImage_FirstOnly;

		namespace ROICrop {

			extern bool ROICropOn;
			extern bool SaveStitchedImage;
			extern std::string SaveStitchedImage_FolderPath;
			extern ushort SaveStitchedImage_CycleRate;
			extern bool SaveStitchedImage_FirstOnly;
			extern bool SaveImageSet;
			extern std::string SaveImageSet_FolderPath;
			extern ushort SaveImageSet_CycleRate;
			extern bool SaveImageSet_FirstOnly;

		}
	}
}

#endif
