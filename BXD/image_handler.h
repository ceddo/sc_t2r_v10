/*
* Copyright 2018-2019 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/

#ifndef IMAGE_HANDLER_H_
#define IMAGE_HANDLER_H_

#include <stdint.h>
#include <stdio.h>

#include "helpers.h"
#include "../imx500_hooks.h"

namespace BXDExample {
    namespace ImageHandler {

        /**
         * @brief Method to handle the saving and displaying of the DNN image. All of the logic surrounding
         * when and if to display and save all frames is handled herein.
         * 
         * @param imageData The prepared image data from the DNN
        */
        void displayAndSaveImage(IMX500Hooks::ImageData imageData);

        /**
         * @brief Convenience method to determine if a non-stitched image will be saved this frame.
         * 
         * @return True if a non-stitched image will be saved this frame
        */
        Helpers::E_IMAGE_TYPE willSaveNonStitchedImageThisFrame();

        /**
         * @brief Property accessor for the current cycle number
         * 
         * @return The current cycle number
        */
        int getDetectionCycleCount();
    }
}

#endif
