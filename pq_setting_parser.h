/*
* Copyright 2020-2021 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/
#ifndef PQ_SETTING_PARSE_H_
#define PQ_SETTING_PARSE_H_

#include <stdint.h>

/*
* PQ Setting Namespace
*/
namespace imx500 {
    namespace pq_setting_parser {

        typedef struct tagPqSettingInfo {
            /* Block: Frame Count */
            uint8_t     frame_count;
            uint8_t     padding1;
            /* Block: SHT */
            uint16_t    coarse_integ_time;
            /* Block: GAIN */
            uint16_t    ana_gain_global;
            uint16_t    ana_gain_c0;
            uint16_t    ana_gain_c1;
            uint16_t    dig_gain_gr;
            uint8_t     dpga_use_global_gain;
            uint8_t     rg_pre_pga_gain;
            /* Block: WBG */
            uint16_t    rg_pre_wbg_gain_r;
            uint16_t    rg_pre_wbg_gain_gr;
            uint16_t    rg_pre_wbg_gain_gb;
            uint16_t    rg_pre_wbg_gain_b;
            /* Block: OPB */
            uint16_t    rg_pre_opb_val_r;
            uint16_t    rg_pre_opb_val_gr;
            uint16_t    rg_pre_opb_val_gb;
            uint16_t    rg_pre_opb_val_b;
            /* Block: LSC */
            uint8_t     rg_pdhlsc_crct_enable;
            uint8_t     rg_pdhlsc_en_each_color;
            uint8_t     rg_pdhlsc_n_knotsram_ramsel_toggle;
            /* Block: LMT */
            uint8_t     pdhcadj_lmtx_mode;
            uint16_t    pdhcadj_lmtx_r_grg;
            uint16_t    pdhcadj_lmtx_r_grb;
            uint16_t    pdhcadj_lmtx_g_ggr;
            uint16_t    pdhcadj_lmtx_g_ggb;
            uint16_t    pdhcadj_lmtx_b_gbr;
            uint16_t    pdhcadj_lmtx_b_gbg;
            /* Block: GAMMA */
            uint8_t     pdhcadj_gamma_mode;
            uint8_t     padding2;
            /* Block: Hue/Gain */
            uint8_t     pdhcadj_hg_coef0_crg1;
            uint8_t     pdhcadj_hg_coef0_crg2;
            uint8_t     pdhcadj_hg_coef0_crg3;
            uint8_t     pdhcadj_hg_coef0_crg4;
            uint16_t    pdhcadj_hg_coef1_crh1;
            uint16_t    pdhcadj_hg_coef1_crh2;
            uint16_t    pdhcadj_hg_coef2_crh3;
            uint16_t    pdhcadj_hg_coef2_crh4;
            uint8_t     pdhcadj_hg_coef3_cbg1;
            uint8_t     pdhcadj_hg_coef3_cbg2;
            uint8_t     pdhcadj_hg_coef3_cbg3;
            uint8_t     pdhcadj_hg_coef3_cbg4;
            uint16_t    pdhcadj_hg_coef4_cbh1;
            uint16_t    pdhcadj_hg_coef4_cbh2;
            uint16_t    pdhcadj_hg_coef5_cbh3;
            uint16_t    pdhcadj_hg_coef5_cbh4;
            /* Block: Ygamma */
            uint8_t     pdhcadj_ygamma_mode;
            uint8_t     pdhcadj_ygamma_knot_y0_knot000;
            uint8_t     pdhcadj_ygamma_knot_y0_knot010;
            uint8_t     pdhcadj_ygamma_knot_y0_knot020;
            uint8_t     pdhcadj_ygamma_knot_y0_knot030;
            uint8_t     pdhcadj_ygamma_knot_y1_knot040;
            uint8_t     pdhcadj_ygamma_knot_y1_knot050;
            uint8_t     pdhcadj_ygamma_knot_y1_knot060;
            uint8_t     pdhcadj_ygamma_knot_y1_knot070;
            uint8_t     pdhcadj_ygamma_knot_y2_knot080;
            uint8_t     pdhcadj_ygamma_knot_y2_knot090;
            uint8_t     pdhcadj_ygamma_knot_y2_knot0a0;
            uint8_t     pdhcadj_ygamma_knot_y2_knot0b0;
            uint8_t     pdhcadj_ygamma_knot_y3_knot0c0;
            uint8_t     pdhcadj_ygamma_knot_y3_knot0d0;
            uint8_t     pdhcadj_ygamma_knot_y3_knot0e0;
            uint8_t     pdhcadj_ygamma_knot_y3_knot0f0;
            uint8_t     pdhcadj_ygamma_knot_y4_knot100;
            /* Block: Auto */
            uint8_t     isp_manual_mode;
            uint8_t     awbmode_sn1;
            uint16_t    normr;
            uint16_t    normb;
            uint16_t    awbprer;
            uint16_t    awbpreb;
            /* Block: APOUT */
            uint16_t    dwp_apout_input_hsize;
            uint16_t    dwp_apout_input_vsize;
            uint16_t    dwp_apout_scl_hoffset;
            uint16_t    dwp_apout_scl_voffset;
            uint16_t    dwp_apout_scl_hsize;
            uint16_t    dwp_apout_scl_vsize;
            uint16_t    dwp_apout_map_hoffset;
            uint16_t    dwp_apout_map_voffset;
            uint16_t    dwp_apout_map_hsize;
            uint16_t    dwp_apout_map_vsize;
            uint16_t    dwp_apout_output_hsize;
            uint16_t    dwp_apout_output_vsize;
            uint8_t     dnn_apout_exec_current_network;
            uint8_t     padding3[3];
        } pqSettingInfo;
    }
}
#endif
