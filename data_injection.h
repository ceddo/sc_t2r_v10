/*
* Copyright 2017-2018 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/
#ifndef _DATA_INJECTION_H_
#define _DATA_INJECTION_H_

#include <return_code.h>
#include <stdio.h>
#include <stdint.h>
#include "data_injection.h"
#include "output_tensor_parser.h"

typedef void * DataInjection_ModuleHandle;

#define REG_ADDR_DD_DAT_INJECTION_HNDSK (0xD0DB)
#define REG_SIZE_DD_DAT_INJECTION_HNDSK (1)
#define REG_ADDR_LEV_IMAGE_DATA_NOT_OVERWRITE (0xD641)
#define REG_SIZE_LEV_IMAGE_DATA_NOT_OVERWRITE (1)




namespace imx500 {
    namespace control {
        namespace DataInjection {
            E_RESULT_CODE  InputTensorInput_Open(DataInjection_ModuleHandle *pHandle);
            E_RESULT_CODE  InputTensorInput_Close(DataInjection_ModuleHandle pHandle);
            E_RESULT_CODE  InputTensorInput_TransferInputTensor(DataInjection_ModuleHandle pHandle, uint8_t *pTransbuf,uint32_t size);
            E_RESULT_CODE  DataInjectionMain(void);
            void saveOutputTensorinL2MemFormat(const uint8_t* src, const uint16_t lineSize, imx500::output_tensor_parser::FrameOutputInfo* frameOutputTensorInfo);
            void cmpInputTensorinL2MemFormat(const uint8_t* src, uint32_t totalSize, uint16_t numOfLines, uint16_t lineSize, uint16_t channel, uint16_t maxLineLen);
        }
    }
}
#endif  // _DATA_INJECTION_H_