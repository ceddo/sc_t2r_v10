/*
* Copyright 2017-2018 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/

#include <stdint.h>
#include "return_code.h"
#include "i2c_access.h"
#include "common_define.h"
#include "sensor_control_private.h"
#include "sensor_property.h"
#include "sensor_param_manager.h"
#include "imx500lib_sample.h"
#include "dbg_log.h"

extern nw_info networkInfoArray[];


namespace imx500 {
    namespace property {

        /**************************************************************************
         * Set function
         **************************************************************************/
        /**
         * @brief set_framerate.
         */
        E_RESULT_CODE set_framerate(FrameRateParam *param) {
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] set_framerate\n");
            param = param;  /* Warning suppression */
            return E_OK;
        }

        /**
         * @brief set_raw_image_size.
         */
        E_RESULT_CODE set_raw_image_size(ImageParam *param) {
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] set_raw_image_size\n");

            E_RESULT_CODE return_code;

            /* status check */
            control::SensoStatus sts = control::get_status();
            if (sts != control::SENSOR_STNDBY) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] status error, sts=0x%08X\n", sts);
                return E_INVALID_STATE;
            }

            /* param check */
            if (param == NULL) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] param error, NULL pointer\n");
                return E_INVALID_PARAM;
            }
            if ((param->m_width < RAW_IMAGE_WIDTH_MIN)   ||
                (param->m_width > RAW_IMAGE_WIDTH_MAX)) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] param error, width=%d\n", param->m_width);
                return E_INVALID_PARAM;
            }
            if ((param->m_height < RAW_IMAGE_HEIGHT_MIN) ||
                (param->m_height > RAW_IMAGE_HEIGHT_MAX)) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] param error, height=%d\n", param->m_height);
                return E_INVALID_PARAM;
            }
            if (param->m_pixelFmt != E_PIXEL_FMT_SGBRG10) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] param error, pixFmt=%d\n", param->m_pixelFmt);
                return E_INVALID_PARAM;
            }

            /* set raw image scaling */
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] set width : %d\n", param->m_width);
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] set height: %d\n", param->m_height);
            return_code = param_manager::SetScalingRawImg(param->m_width, param->m_height);

            return return_code;
        }

        /**
         * @brief set_auto_exposure.
         */
        E_RESULT_CODE set_auto_exposure(AeParam *param) {
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] set_auto_exposure\n");
            param = param;  /* Warning suppression */
            return E_OK;
        }

        /**
         * @brief set_auto_white_balance.
         */
        E_RESULT_CODE set_auto_white_balance(AwbParam *param) {
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] set_auto_white_balance\n");
            param = param;  /* Warning suppression */
            return E_OK;
        }

        /**
         * @brief set_dnn_crop_size.
         */
        E_RESULT_CODE set_dnn_crop_size(CropParam *param) {
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] set_dnn_crop_size\n");

            E_RESULT_CODE return_code;

            /* param check */
            if (param == NULL) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] param error, NULL pointer\n");
                return E_INVALID_PARAM;
            }
            if (param->m_left > DNN_IMAGE_WIDTH_MAX) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] param error, left=%d\n", param->m_left);
                return E_INVALID_PARAM;
            }
            if (param->m_top > DNN_IMAGE_HEIGHT_MAX) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] param error, top=%d\n", param->m_top);
                return E_INVALID_PARAM;
            }
            if ((param->m_width < DNN_IMAGE_WIDTH_MIN) ||
                (param->m_width > DNN_IMAGE_WIDTH_MAX)) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] param error, width=%d\n", param->m_width);
                return E_INVALID_PARAM;
            }
            if ((param->m_height < DNN_IMAGE_HEIGHT_MIN) ||
                (param->m_height > DNN_IMAGE_HEIGHT_MAX)) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] param error, height=%d\n", param->m_height);
                return E_INVALID_PARAM;
            }

            /* set dnn image cropping */
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] set left  : %d\n", param->m_left);
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] set top   : %d\n", param->m_top);
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] set width : %d\n", param->m_width);
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] set height: %d\n", param->m_height);
            return_code = param_manager::SetCroppingDnnImg(param->m_left, param->m_top, param->m_width, param->m_height);

            return return_code;
        }

        /**
         * @brief set_raw_crop_size.
         */
        E_RESULT_CODE set_raw_crop_size(CropParam *param) {
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] set_raw_crop_size\n");

            E_RESULT_CODE return_code;

            /* status check */
            control::SensoStatus sts = control::get_status();
            if (sts != control::SENSOR_STNDBY) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] status error, sts=0x%08X\n", sts);
                return E_INVALID_STATE;
            }

            /* param check */
            if (param == NULL) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] param error, NULL pointer\n");
                return E_INVALID_PARAM;
            }
            if (param->m_left > RAW_IMAGE_WIDTH_MAX) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] param error, left=%d\n", param->m_left);
                return E_INVALID_PARAM;
            }
            if (param->m_top > RAW_IMAGE_HEIGHT_MAX) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] param error, top=%d\n", param->m_top);
                return E_INVALID_PARAM;
            }
            if ((param->m_width < RAW_IMAGE_WIDTH_MIN) ||
                (param->m_width > RAW_IMAGE_WIDTH_MAX)) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] param error, width=%d\n", param->m_width);
                return E_INVALID_PARAM;
            }
            if ((param->m_height < RAW_IMAGE_HEIGHT_MIN) ||
                (param->m_height > RAW_IMAGE_HEIGHT_MAX)) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] param error, height=%d\n", param->m_height);
                return E_INVALID_PARAM;
            }

            /* set raw image cropping */
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] set left  : %d\n", param->m_left);
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] set top   : %d\n", param->m_top);
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] set width : %d\n", param->m_width);
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] set height: %d\n", param->m_height);
            return_code = param_manager::SetCroppingRawImg(param->m_left, param->m_top, param->m_width, param->m_height);

            return return_code;
        }

        /**
         * @brief set_power_save_mode.
         */
        E_RESULT_CODE set_power_save_mode(PsvParam *param) {
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] set_power_save_mode\n");
            param = param;  /* Warning suppression */
            return E_OK;
        }

        /**
         * @brief set_reg_value.
         */
        E_RESULT_CODE set_reg_value(RegSetting *param) {
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] set_reg_value\n");

            /* status check */
            control::SensoStatus sts = control::get_status();
            if (sts != control::SENSOR_STNDBY) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] status error, sts=0x%08X\n", sts);
                return E_INVALID_STATE;
            }

            /* param check */
            if (param == NULL) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] param error, NULL pointer\n");
                return E_INVALID_PARAM;
            }

            while (param->m_size != 0) {

                I2C_ACCESS_WRITE(param->m_addr, param->m_value, param->m_size);
#if 0
                if (param->m_size == 1) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] addr=0x%04X, value=0x%02X\n", param->m_addr, param->m_value);
                }
                else if (param->m_size == 2) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] addr=0x%04X, value=0x%04X\n", param->m_addr, param->m_value);
                }
                else {
                    DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] addr=0x%04X, value=0x%08X\n", param->m_addr, param->m_value);
                }
#endif
                param++;
            }

            return E_OK;
        }

        /**
         * @brief set_init_param.
         */
        E_RESULT_CODE set_init_param(uint8_t raw_full_size_ena, uint8_t frame_rate, uint8_t binning_opt) {
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] set_init_param\n");

            /* status check */
            control::SensoStatus sts = control::get_status();
            if (sts != control::SENSOR_STNDBY) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] status error, sts=0x%08X\n", sts);
                return E_INVALID_STATE;
            }

            /* param check */
            if (raw_full_size_ena > 1) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] param error, raw_size=%d\n", raw_full_size_ena);
                return E_INVALID_PARAM;
            }
            if (frame_rate > 30) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] param error, frame_rate=%d\n", frame_rate);
                return E_INVALID_PARAM;
            }

            param_manager::SetDefaultParam();
            param_manager::SetValiableParam(raw_full_size_ena, frame_rate, binning_opt);

            return E_OK;
        }

        /**************************************************************************
         * Get function
         **************************************************************************/
        /**
         * @brief get_framerate.
         */
        E_RESULT_CODE get_framerate(FrameRateParam *param) {
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] get_framerate\n");
            param = param;  /* Warning suppression */
            return E_OK;
        }

        /**
         * @brief get_dnn_image_size.
         */
        E_RESULT_CODE get_dnn_image_size(ImageParam *param) {
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] get_dnn_image_size\n");

            /* status check */
            control::SensoStatus sts = control::get_status();
            if (sts == control::SENSOR_INIT) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] status error, sts=0x%08X\n", sts);
                return E_INVALID_STATE;
            }

            /* param check */
            if (param == NULL) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] param error, NULL pointer\n");
                return E_INVALID_PARAM;
            }

            uint32_t dnnIndex;
            I2C_ACCESS_READ(REG_ADDR_DNN_EXEC_NETWORK, &dnnIndex, REG_SIZE_DNN_EXEC_NETWORK);
             
            if (dnnIndex >= MAX_NUM_OF_NETWORKS) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][SetCrpDnnImg] Invalid dnnIndex %d\n", dnnIndex);
                return E_OTHER;
            }

            /* get raw image size */
            param->m_width = networkInfoArray[dnnIndex].inputTensorWidth;
            param->m_height = networkInfoArray[dnnIndex].inputTensorHeight;
            param->m_stride = networkInfoArray[dnnIndex].inputTensorWidthStride;
            param->m_pixelFmt = E_PIXEL_FMT_RGB888PLANAR;
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] get width : %d\n", param->m_width);
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] get height: %d\n", param->m_height);
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] get stride: %d\n", param->m_stride);

            return E_OK;
        }

        /**
         * @brief get_raw_image_size.
         */
        E_RESULT_CODE get_raw_image_size(ImageParam *param) {
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] get_raw_image_size\n");

            /* status check */
            control::SensoStatus sts = control::get_status();
            if (sts == control::SENSOR_INIT) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] status error, sts=0x%08X\n", sts);
                return E_INVALID_STATE;
            }

            /* param check */
            if (param == NULL) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] param error, NULL pointer\n");
                return E_INVALID_PARAM;
            }

            /* get raw image size */
            I2C_ACCESS_READ(REG_ADDR_X_OUT_SIZE, &param->m_width,  REG_SIZE_X_OUT_SIZE);
            I2C_ACCESS_READ(REG_ADDR_Y_OUT_SIZE, &param->m_height, REG_SIZE_Y_OUT_SIZE);
            param->m_stride = param->m_width;
            param->m_pixelFmt = E_PIXEL_FMT_SGBRG10;
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] get width : %d\n", param->m_width);
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] get height: %d\n", param->m_height);
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] get stride: %d\n", param->m_stride);

            return E_OK;
        }

        /**
         * @brief get_auto_exposure.
         */
        E_RESULT_CODE get_auto_exposure(AeParam *param) {
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] get_auto_exposure\n");
            param = param;  /* Warning suppression */
            return E_OK;
        }

        /**
         * @brief get_auto_white_balance.
         */
        E_RESULT_CODE get_auto_white_balance(AwbParam *param) {
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] get_auto_white_balance\n");
            param = param;  /* Warning suppression */
            return E_OK;
        }

        /**
         * @brief get_dnn_crop_size.
         */
        E_RESULT_CODE get_dnn_crop_size(CropParam *param) {
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] get_dnn_crop_size\n");

            /* status check */
            control::SensoStatus sts = control::get_status();
            if (sts == control::SENSOR_INIT) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] status error, sts=0x%08X\n", sts);
                return E_INVALID_STATE;
            }

            /* param check */
            if (param == NULL) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] param error, NULL pointer\n");
                return E_INVALID_PARAM;
            }

            /* get raw image size */
            I2C_ACCESS_READ(REG_ADDR_DWP_AP_VC_HOFFSET, &param->m_left,   REG_SIZE_DWP_AP_VC_HOFFSET);
            I2C_ACCESS_READ(REG_ADDR_DWP_AP_VC_VOFFSET, &param->m_top,    REG_SIZE_DWP_AP_VC_VOFFSET);
            I2C_ACCESS_READ(REG_ADDR_DWP_AP_VC_HSIZE,   &param->m_width,  REG_SIZE_DWP_AP_VC_HSIZE);
            I2C_ACCESS_READ(REG_ADDR_DWP_AP_VC_VSIZE,   &param->m_height, REG_SIZE_DWP_AP_VC_VSIZE);
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] get left  : %d\n", param->m_left);
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] get top   : %d\n", param->m_top);
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] get width : %d\n", param->m_width);
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] get height: %d\n", param->m_height);

            return E_OK;
        }

        /**
         * @brief get_raw_crop_size.
         */
        E_RESULT_CODE get_raw_crop_size(CropParam *param) {
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] get_raw_crop_size\n");

            /* status check */
            control::SensoStatus sts = control::get_status();
            if (sts == control::SENSOR_INIT) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] status error, sts=0x%08X\n", sts);
                return E_INVALID_STATE;
            }

            /* param check */
            if (param == NULL) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][PROPTY] param error, NULL pointer\n");
                return E_INVALID_PARAM;
            }

            /* get raw image size */
            I2C_ACCESS_READ(REG_ADDR_DIG_CROP_X_OFFSET,     &param->m_left,   REG_SIZE_DIG_CROP_X_OFFSET);
            I2C_ACCESS_READ(REG_ADDR_DIG_CROP_Y_OFFSET,     &param->m_top,    REG_SIZE_DIG_CROP_Y_OFFSET);
            I2C_ACCESS_READ(REG_ADDR_DIG_CROP_IMAGE_WIDTH,  &param->m_width,  REG_SIZE_DIG_CROP_IMAGE_WIDTH);
            I2C_ACCESS_READ(REG_ADDR_DIG_CROP_IMAGE_HEIGHT, &param->m_height, REG_SIZE_DIG_CROP_IMAGE_HEIGHT);
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] get left  : %d\n", param->m_left);
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] get top   : %d\n", param->m_top);
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] get width : %d\n", param->m_width);
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] get height: %d\n", param->m_height);

            return E_OK;
        }

        /**
         * @brief get_power_save_mode.
         */
        E_RESULT_CODE get_power_save_mode(PsvParam *param) {
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] get_power_save_mode\n");
            param = param;  /* Warning suppression */
            return E_OK;
        }

        /**
         * @brief get_isp_support.
         */
        E_RESULT_CODE get_isp_support(IspSptParam *param) {
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IMX500LIB][PROPTY] get_isp_support\n");
            param = param;  /* Warning suppression */
            return E_OK;
        }

    } /* namespace property */
} /* namespace imx500 */
