/*
* Copyright 2017-2018 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/

#include <iostream>
#include <algorithm>
#include <cstdint>
#include <cstring>
#include <cmath>
#include "input_tensor_parser.h"
#include "apParams.flatbuffers_generated.h"
#include "utils.h"
#include "common_define.h"
#include "i2c_access.h"
#include "data_injection.h"
#include "dbg_log.h"


#define MAX_AP_PARAM_SIZE 4084 // 4KB - 12B(Header)
#define INPUT_TENSOR_MAX_H_SIZE 1280
#define INPUT_TENSOR_MAX_V_SIZE 960

using namespace imx500::utils;

namespace imx500 {
	namespace input_tensor_parser {

		int32_t parseApParams(const char *bodyApParams, const struct DnnHeader* dnnHeader, struct FrameInputTensorInfo* frameInputTensorInfo) {
			const apParams::fb::FBApParams* fbApParams;
			//const apParams::fb::FBNetwork fbNetworks[];
			const apParams::fb::FBNetwork* fbNetwork;
			const apParams::fb::FBInputTensor* fbInputTensor;

			fbApParams = apParams::fb::GetFBApParams(bodyApParams);
			//fbNetworks = fbApParams->networks();
			DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[IN_TENSOR] Networks size:%d\n", fbApParams->networks()->size());
			for (uint32_t i = 0; i < fbApParams->networks()->size(); i++) {
				fbNetwork = (apParams::fb::FBNetwork*)(fbApParams->networks()->Get(i));
				if (fbNetwork->id() == dnnHeader->networkId) {
					//Network = fbApParams->networks[i];
                    frameInputTensorInfo->networkId = fbNetwork->id();
					DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[IN_TENSOR] name:%s\n", fbNetwork->name()->c_str());
					DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[IN_TENSOR] Input Tensors size:%d\n", fbNetwork->inputTensors()->size());
					if (fbNetwork->inputTensors()->size() != 1) {
						DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IN_TENSOR] Invalid number of InputTensors");
						return -1;
					}
					fbInputTensor = (apParams::fb::FBInputTensor*)fbNetwork->inputTensors()->Get(0);

					if (fbInputTensor->dimensions()->size() != 3) {
						DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IN_TENSOR] Invalid number of dimensions in InputTensor");
						return -1;
					}

					for (uint32_t j = 0; j < fbInputTensor->dimensions()->size(); j++) {
						switch (fbInputTensor->dimensions()->Get(j)->serializationIndex()) {
						case 0:
							frameInputTensorInfo->width = fbInputTensor->dimensions()->Get(j)->size();
							frameInputTensorInfo->widthStride = frameInputTensorInfo->width + fbInputTensor->dimensions()->Get(j)->padding();
							break;
						case 1:
							frameInputTensorInfo->height = fbInputTensor->dimensions()->Get(j)->size();
							frameInputTensorInfo->heightStride = frameInputTensorInfo->height + fbInputTensor->dimensions()->Get(j)->padding();
							break;
						case 2:
							frameInputTensorInfo->channel = fbInputTensor->dimensions()->Get(j)->size();
							break;
						default:
							DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IN_TENSOR] Invalid dimension in InputTensor %d\n", j);
							break;
						}
					}

					break;
				}
			}
			return 0;
		}

		int32_t parseInputTensor(const struct InputDataInfo* in,
			struct OutputDataInfo* out, const struct InputNormInfo* inputNorm,struct FrameInputTensorInfo* frameInputTensorInfo) {
			uint16_t width, height, channel;
			struct DnnHeader* dnnHeader = new DnnHeader();
			uint8_t * src;
			uint8_t * readPonter;
			uint8_t * dst;


			/*Extract All Header Info */
			src = in->addr;
			readPonter = src;
			readPonter += MIPI_PH_SIZE;
			int32_t ret = utils::extract_dnn_header(readPonter, dnnHeader);
			if (ret != 0) {
					return -1;
			}

			DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IN_TENSOR] after extarct dnn header\n");
			readPonter += DNN_HDR_SIZE;

			if (dnnHeader->frameValid != 1) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IN_TENSOR] Invalid Frame, valid: %d\n", dnnHeader->frameValid);
				delete dnnHeader;
				return -1;
			}
			if (dnnHeader->tensorType != TYPE_INPUT_TENSOR) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IN_TENSOR] Wrong tensorType: %d\n", dnnHeader->tensorType);
				delete dnnHeader;
				return -1;
			}
			if ((dnnHeader->apParamSize == 0) ||(dnnHeader->apParamSize > MAX_AP_PARAM_SIZE)) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IN_TENSOR] Invalid AP Param size: %d\n", dnnHeader->apParamSize);
				delete dnnHeader;
				return -1;
			}
			if (dnnHeader->maxLineLen == 0) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IN_TENSOR] Invalid max line length: 0\n");
				delete dnnHeader;
				return -1;
			}
			char* ap_params = new char[dnnHeader->apParamSize];
			if (ap_params == NULL) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IN_TENSOR] Error allocating memory\n");
				delete dnnHeader;
				return -1;
			}

			//64 byte Align 
			uint16_t lineSize = in->pitch;
			uint32_t lineLen = in->pitch;
			uint32_t lineIndex = DNN_HDR_SIZE;

			for (int j = 0; j < dnnHeader->apParamSize; j++) {
				if (lineIndex >= lineLen) {
					lineIndex = 0;
					src += lineSize;
					readPonter = src + MIPI_PH_SIZE;
				}
				ap_params[j] = *(readPonter++);
				lineIndex++;
			}
			DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IN_TENSOR] before parse ap_params\n");
			ret = parseApParams((const char*) ap_params, (const DnnHeader*) dnnHeader, frameInputTensorInfo);
			if (ret != 0) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IN_TENSOR] Error parsing AP Params\n");
				delete[] ap_params;
				delete dnnHeader;
				return -1;
			}

			frameInputTensorInfo->format = inputNorm->input_format[frameInputTensorInfo->networkId];
			DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IN_TENSOR] after parse ap_params whc: width=%d, height=%d,channel=%d\n", frameInputTensorInfo->width, frameInputTensorInfo->height, frameInputTensorInfo->channel);
			delete[] ap_params;

			src += lineSize;
			width = frameInputTensorInfo->width;
			height = frameInputTensorInfo->height;
			channel = frameInputTensorInfo->channel;
			if ((width > INPUT_TENSOR_MAX_H_SIZE) || (height > INPUT_TENSOR_MAX_V_SIZE) || ((channel != 1) && (channel != 3))) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IN_TENSOR] Invalid input tensor size w:%d h:%d c%d\n", width, height, channel);
				delete dnnHeader;
				return -1;
			}

			/* correct channel */
			if ((in->ych_en == 1) && (channel != 1)) {
				static bool warn_out = false;
				if (warn_out == 0) {
					/* log output only once */
					DBG_LOG_PRINT(DBG_LOG_LVL_WARNING, "[IN_TENSOR] Monochrome mode, but there are %dch in AP params\n", channel);
					warn_out = true;
				}
				channel = 1;
			}

			/* Set Output Information */
			dst = out->addr;

			DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[IN_TENSOR] dst addr :%p\n ",dst); 
			uint16_t width_stride = frameInputTensorInfo->widthStride;
			uint16_t height_stride = frameInputTensorInfo->heightStride;
			if ((width_stride > INPUT_TENSOR_MAX_H_SIZE) || (height_stride > INPUT_TENSOR_MAX_V_SIZE)) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IN_TENSOR] Invalid input tensor size w_stride:%d h_stride:%d\n", width_stride, height_stride);
				delete dnnHeader;
				return -1;
			}

			uint16_t numOfLines = (uint16_t)ceil((width_stride * height_stride * channel) / (float)dnnHeader->maxLineLen);
			out->size = width * height * channel;
			uint32_t pixelIndex = 0;
			uint32_t outlineIndex = 0;
			uint16_t linenum = 0;
			uint16_t heightIndex = 0;
			lineIndex = 0;
			uint32_t normVal[3]={0};
			uint32_t normShift[3]={0};
			uint32_t dbgHeightCount=0;
			uint32_t dnnIndex = frameInputTensorInfo->networkId;

			if(dnnIndex >= MAX_NUM_OF_NETWORKS) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR,"[IN_TENSOR] Invalid Network ID %d\n",dnnIndex);
				delete dnnHeader;
				return -1;
			}
    
			uint16_t numOfLinesPerChannel = (uint16_t)ceil((width_stride * height_stride) / (float)dnnHeader->maxLineLen);
			uint32_t totalSize = width_stride * height_stride * channel;
			imx500::control::DataInjection::cmpInputTensorinL2MemFormat(src, totalSize, numOfLinesPerChannel, lineSize, channel, dnnHeader->maxLineLen);

			DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[IN_TENSOR] Width_Stride= %d,Height_Stride= %d, Num of Lines=%d outSize=%d\n",width_stride,height_stride,numOfLines,out->size);
			for (int i = 0; i < channel; i++) {
				if(inputNorm->norm_val[dnnIndex][i] >> 8 == 0) {
					normVal[i] = inputNorm->norm_val[dnnIndex][i] & 0xff;
				}
				else {
					normVal[i] = - (inputNorm->norm_val[dnnIndex][i] & 0xff);
				}
				normShift[i] = std::pow(2, inputNorm->norm_shift[dnnIndex][i]);
			}

			uint8_t channelIndex = 0;
			for (linenum = 0; linenum < numOfLines; linenum++) {
				lineIndex = 0;
			   	while (lineIndex < dnnHeader->maxLineLen) {
					if(outlineIndex == width_stride) {
						outlineIndex = 0;
						heightIndex++;
						dbgHeightCount++;
					}
					else if(outlineIndex >= width) { /* Skip width padding pixels */
						outlineIndex++;
						lineIndex++;
						continue;
					}

					if (heightIndex == height_stride) {
						heightIndex = 0;
					}
				        else if(heightIndex >= height){ /* Skip height padding pixels */
						outlineIndex++;
						lineIndex++;
						continue;
					}	

					if ((pixelIndex != 0) && (pixelIndex % (width*height) == 0)) {
						if (pixelIndex == out->size) {
			                    DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[IN_TENSOR]Out size reached linenum :%d dbgHeightCount=%d, pixelCount=%d\n ",linenum,dbgHeightCount,pixelIndex); 
					            break;
					        }
					    /* Increment channel index */
					    channelIndex++;
					}

					dst[pixelIndex] = (((*(src + lineIndex) * normShift[channelIndex] - normVal[channelIndex]) << 5) / in->gain) & 0xFF;
																											/* ~~~~~~~~~~~~~~~~ */
																											/* Since the upper 3 bits are integers and the lower 5 bits are decimals,   */
																											/* the gain accuracy is shifted by 5 bits before division.                  */
					pixelIndex++;
					outlineIndex++;
					lineIndex++;
				}
			
				if (pixelIndex == out->size) {
			               DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[IN_TENSOR]Outer loop: out size reached linenum :%d dbgHeightCount=%d, pixelCount=%d\n ",linenum,dbgHeightCount,pixelIndex); 
				       break;
				}
				src += lineLen; // (refpacketHeader->wordCount + 0x1F + MIPI_PH_SIZE + MIPI_PF_SIZE) / 0x20 * 0x20;
		        // DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[IN_TENSOR] linenum :%d dbgHeightCount=%d, pixelCount=%d\n ",linenum,dbgHeightCount,pixelIndex); 
			}

			delete dnnHeader;
			//DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[IN_TENSOR] dbgHeightCount=%d pixelCnt=%d\n",dbgHeightCount,pixelIndex);
			DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[IN_TENSOR] Parsing InputTensor Complete!!!\n");
			return 0;
		}
	}
}   // namespace rgb_extractor
