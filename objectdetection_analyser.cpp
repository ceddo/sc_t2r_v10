#include <iostream>
#include <algorithm>
#include <cmath>
#include "objectdetection_analyser.h"
#include "objectdetection_interface.h"
#include "dbg_log.h"


namespace imx500 {
	namespace objectdetection_analyser {

		SSDMobilenetV1Data getActualDetections(SSDMobilenetV1Data ssdData) {
			DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[PPL][OD] getActualDetections\n");
			//SSDMobilenetV1Data sorted_ssdData;
			struct Compare : std::binary_function<size_t, size_t, bool> {
				Compare(const std::vector<float>& scores)
					: v_scores(scores)
				{}

				bool operator()(size_t Lhs, size_t Rhs)const
				{
					return v_scores[Lhs] > v_scores[Rhs];
				}

				const std::vector<float>& v_scores;
			};

			std::vector<size_t>pos(ssdData.v_scores.size());
			for (size_t i = 0; i < pos.size(); i++) {
				pos[i] = i;
			}
			std::stable_sort(pos.begin(), pos.end(), Compare(ssdData.v_scores));

			return ssdData;
		}

		bool IsNonZero(float i) { return (i != 0); }

		int32_t analyseObjectDetectionOutput(const struct imx500::objectdetection_interface::ObjectDetectionOutputTensor outTensor, \
			                                 struct SSDMobilenetV1Data* outputSSDMobilenetV1Data, \
			                                 uint16_t maxDetections) {
			uint8_t numOfDetections;
			std::vector<Bbox> v_bbox;
			std::vector<float> v_scores;
			std::vector<uint8_t> v_classes;
			SSDMobilenetV1Data ssdData;

			//Extract number of detections
			numOfDetections = (uint8_t)outTensor.numOfDetections;
			DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[PPL][OD] number of Detections = %d\n", numOfDetections);

			//Extract scores
			for (uint8_t i = 0; i < numOfDetections; i++) {
				float score;
				score = outTensor.scores[i];
				v_scores.push_back(score);
			}

			//Extract bounding box co-ordinates
			for (uint8_t i = 0; i < numOfDetections; i++) {
				struct Bbox bbox;
				bbox.x_min = (uint16_t)(round((outTensor.bboxes[i].x_min) * (INPUT_TENSOR_WIDTH-1)));
				bbox.y_min = (uint16_t)(round((outTensor.bboxes[i].y_min) * (INPUT_TENSOR_HEIGHT-1)));
				bbox.x_max = (uint16_t)(round((outTensor.bboxes[i].x_max) * (INPUT_TENSOR_WIDTH-1)));
				bbox.y_max = (uint16_t)(round((outTensor.bboxes[i].y_max) * (INPUT_TENSOR_HEIGHT-1)));
				v_bbox.push_back(bbox);
			}

			for (uint8_t i = 0; i < numOfDetections; i++) {
				uint8_t class_index;
				class_index = (uint8_t)outTensor.classes[i];
				v_classes.push_back(class_index);
			}

			ssdData.numOfDetections = numOfDetections;
			ssdData.v_bbox = v_bbox;
			ssdData.v_scores = v_scores;
			ssdData.v_classes = v_classes;
			ssdData = getActualDetections(ssdData);

			//Debug
			DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[PPL][OD] Output Data\n");
			DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[PPL][OD] number of Detections = %d\n", ssdData.numOfDetections);
			for (int i = 0; i < numOfDetections; i++) {
				DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[PPL][OD] v_bbox[%d] :[x_min,y_min,x_max,y_max] = [%d,%d,%d,%d]\n", i, ssdData.v_bbox[i].x_min, ssdData.v_bbox[i].y_min, ssdData.v_bbox[i].x_max, ssdData.v_bbox[i].y_max);
			}
			for (int i = 0; i < numOfDetections; i++) {
				DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[PPL][OD] scores[%d] = %f\n", i, ssdData.v_scores[i]);
			}
			for (int i = 0; i < numOfDetections; i++) {
				DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[PPL][OD] class_indices[%d] = %d\n", i, ssdData.v_classes[i]);
			}

			if (ssdData.numOfDetections > maxDetections) {
				ssdData.numOfDetections = maxDetections;
				ssdData.v_bbox.resize(maxDetections);
				ssdData.v_classes.resize(maxDetections);
				ssdData.v_scores.resize(maxDetections);
			}

			outputSSDMobilenetV1Data->numOfDetections = ssdData.numOfDetections;
			outputSSDMobilenetV1Data->v_bbox = ssdData.v_bbox;
			outputSSDMobilenetV1Data->v_scores = ssdData.v_scores;
			outputSSDMobilenetV1Data->v_classes = ssdData.v_classes;

			//Debug
			DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[PPL][OD] Output Data after Sorting\n");
			DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[PPL][OD] number of maxDetections = %d\n", ssdData.numOfDetections);
			numOfDetections = ssdData.numOfDetections;
			for (int i = 0; i < numOfDetections; i++) {
				DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[PPL][OD] v_bbox[%d] :[x_min,y_min,x_max,y_max] = [%d,%d,%d,%d]\n", i, ssdData.v_bbox[i].x_min, ssdData.v_bbox[i].y_min, ssdData.v_bbox[i].x_max, ssdData.v_bbox[i].y_max);
			}
			for (int i = 0; i < numOfDetections; i++) {
				DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[PPL][OD] scores[%d] = %f\n", i, ssdData.v_scores[i]);
			}
			for (int i = 0; i < numOfDetections; i++) {
				DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[PPL][OD] class_indices[%d] = %d\n", i, ssdData.v_classes[i]);
			}

			DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[PPL][OD] Analyser Complete\n");
			return 0;
		}
	}
}
