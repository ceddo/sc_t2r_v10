#include <iostream>
#include <algorithm>
#include "output_tensor_parser.h"
#include "classification_analyser.h"
#include "dbg_log.h"


namespace imx500 {
	namespace classification_analyser {

		int32_t analyseClassificationOutput(classification_interface::ClassificationOutputTensor outTensor,
			struct ClassificationData* outputClassificationData, uint16_t numOfScores)
		{

			uint16_t numOfDetections;
			std::vector<ClassificationItem> classData;

			//Extract number of detections
			numOfDetections = outTensor.scores.size();
			DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[PPL][CS] number of Detections = %d\n", numOfDetections);

			//Extract scores
			for (uint16_t i = 0; i < numOfDetections; i++) {
				ClassificationItem item;
				item.index = i;
				item.score = outTensor.scores[i];
		        classData.push_back(item);
			}

		    std::stable_sort(classData.begin(), classData.end(), [](const ClassificationItem &left, const ClassificationItem &right) {
		       return left.score > right.score;
		    });
		    DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[PPL][CS] after sort\n");
			if (numOfDetections > NUM_OF_CLASSES) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[PPL][CS] Error: NumOfScores > Max number of classes, Set to Max number of claases");
				numOfDetections = NUM_OF_CLASSES;
			}

			for (uint16_t i = 0; i < numOfScores; i++) {
				outputClassificationData->v_classItem.push_back(classData[i]);
				DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[PPL][CS] Top[%d] = id: %d  score: %f\n", i, classData[i].index, classData[i].score);
			}

			DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[PPL][CS] Analyser Complete\n");
			return 0;
		}
	}
}
