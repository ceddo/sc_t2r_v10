/*
* Copyright 2020-2021 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/
#ifndef DBG_LOG_H_
#define DBG_LOG_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdint.h>


/* enums */
typedef enum tagDBG_LOG_Level {
    DBG_LOG_LVL_DETAIL,         /* Detail output */
    DBG_LOG_LVL_TRACE,          /* Trace output */
    DBG_LOG_LVL_INFO,           /* Information output */
    DBG_LOG_LVL_WARNING,        /* Warning output */
    DBG_LOG_LVL_ERROR,          /* Error output */
    DBG_LOG_LVL_MAX
} DBG_LOG_Level;


/* extern */
extern DBG_LOG_Level g_dbgLogLevel;
extern void DBG_LOG_Init(uint32_t level);

/* macro define */
#define DBG_LOG_PRINT(lv, fmt, ...)    (((lv) >= g_dbgLogLevel) ? printf((fmt), ##__VA_ARGS__) : (lv))


#ifdef __cplusplus
}
#endif

#endif  /* DBG_LOG_H_ */
