/*
* Copyright 2017-2018 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/
#ifndef RETURN_CODE_H_
#define RETURN_CODE_H_

namespace imx500 {

/* enums */
typedef enum tagE_RESULT_CODE {
    E_OK            = 0,
    E_INVALID_STATE = 0x00000021,       /* State Error */
    E_INVALID_ID    = 0x00000022,       /* ID Error  */
    E_INVALID_PARAM = 0x00000023,       /* Parameter Error */
    E_DRIVER_ERROR  = 0x00000024,       /* Driver Error */
    E_DEVICE_ERROR  = 0x00000024,       /* Device Error */
    E_TIMEOUT       = 0x00000031,       /* TimeOut Error */
    E_MEM           = 0x00000032,       /* Memory association Error */
    E_OS_ERROR      = 0x00000033,       /* OS ERROR */ 
    E_OTHER         = 0x00000034,       /* Others Error */
    E_NOT_SUPPORT   = 0x00000035,       /* Not supported */
} E_RESULT_CODE;

} /* namespace imx500 */

#endif /* RETURN_CODE_H_ */
