/*
* Copyright 2017-2018 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/

#include <stdint.h>
#include "return_code.h"
#include "common_define.h"
#include "dbg_log.h"
#ifdef EXEC_ON_SSP300_ENV
#include "../../ssp300_image/src/dll_call.h"
#endif /*  EXEC_ON_SSP300_ENV */

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <semaphore.h>


/* IMX500 Register memory area */
#ifdef EXEC_ON_SSP300_ENV
#define PC_TO_SSP300_SLAVE_ADDR     (0x10)
#define SENSOR_REG_MEM_AREA         (0x22000000)
#else
#define PC_TO_SSP300_SLAVE_ADDR     (0x00)
#define SENSOR_REG_MEM_AREA         (0x00000000)
#endif /*  EXEC_ON_SSP300_ENV */


#define I2C_SLAVE_FORCE 0x0706
#define I2C_ADDR 0x1A


/* static variables */
#ifdef EXEC_ON_SSP300_ENV
static CDLLCALL* s_pI2cDrvObj;
#endif /*  EXEC_ON_SSP300_ENV */


#define DEFAULT_I2C_DEVICE 0
#define I2C_DEVICE_NAME_LEN 13	// "/dev/i2c-XXX"+NULL

static sem_t i2cSync;

namespace imx500 {
    namespace i2c_access {

#ifdef EXEC_ON_SSP300_ENV
        /**
         * @brief I2C open.
         */
        void I2cOpen(void* pI2cDrvObj) {

            s_pI2cDrvObj = (CDLLCALL*)pI2cDrvObj;
        }
#else /*  EXEC_ON_SSP300_ENV */
        void I2cOpen(void* pI2cDrvObj) {
            sem_init(&i2cSync, 0, 1);
        }
#endif
        /**
         * @brief I2C write.
         */
        E_RESULT_CODE I2cWrite(uint32_t addr, uint32_t val, uint32_t size) {

            uint32_t i2c_addr = addr | SENSOR_REG_MEM_AREA;
            uint8_t p_buf[4];
            E_RESULT_CODE ret_code = E_OK;

            /* set buffer */
            for (uint32_t i = 0; i < size; i++) {
                p_buf[i] = static_cast<uint8_t>((val >> ((size - 1 - i) * 8)) & 0x000000FF);
            }

#ifdef EXEC_ON_SSP300_ENV
            uint32_t ret = s_pI2cDrvObj->UsbI2CWrite(PC_TO_SSP300_SLAVE_ADDR,
                                            i2c_addr,
                                            p_buf,
                                            size);
            if (ret != ERROR_SUCCESS) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][I2C] Failed write : ret=%d, addr=0x%08X, size=%d\n", ret, i2c_addr, size);
                ret_code = E_DRIVER_ERROR;
            }
            return ret_code;
        }
#else
//start Raspberry Pi
            unsigned char msg[6] = {i2c_addr>>8, i2c_addr};
            int length = size +2;

            for (uint32_t i = 0; i < size; i++) {
                msg[i+2] = p_buf[i] ;
            }
            if (sem_wait(&i2cSync) != 0) {
                exit(1);
            }

            int fd;
            char i2c_device_name[I2C_DEVICE_NAME_LEN];
            snprintf(i2c_device_name, sizeof(i2c_device_name), "/dev/i2c-%d", DEFAULT_I2C_DEVICE);
			fd = open(i2c_device_name, O_RDWR);
			if (fd < 0)
			{
			    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][I2C] Failed write : fd:%d\n", fd);
                if (sem_post(&i2cSync) != 0) {
                    exit(1);
                }
			    return E_DRIVER_ERROR;
			}
			if (ioctl(fd, I2C_SLAVE_FORCE, I2C_ADDR) < 0)
			{
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][I2C] i2cwrite slave err ! :\n");
                if (sem_post(&i2cSync) != 0) {
                    exit(1);
                }
			    close(fd);
				return E_DRIVER_ERROR;
			}
			//write address
            int len = write(fd,msg,length);
            close(fd);
           	//end Raspberry Pi
            if (sem_post(&i2cSync) != 0) {
                exit(1);
            }
            if(len != length){	
                ret_code = E_DRIVER_ERROR;
            }
            
            return ret_code;
        }
#endif /*  EXEC_ON_SSP300_ENV */

        /**
         * @brief I2C read.
         */
        E_RESULT_CODE I2cRead(uint32_t addr, uint32_t* val, uint32_t size) {

            uint32_t i2c_addr = addr | SENSOR_REG_MEM_AREA;
            uint8_t p_buf[4] = {0};
            uint32_t timeout = 1000;
            uint32_t ret;

#ifdef EXEC_ON_SSP300_ENV
            /* Note: Cannot access more than 2 bytes */
            for (uint32_t i = 0; i < size; i++) {
                ret = s_pI2cDrvObj->UsbI2CRead(PC_TO_SSP300_SLAVE_ADDR,
                                                i2c_addr + i,
                                                &p_buf[i],
                                                1,
                                                timeout);
                if (ret != ERROR_SUCCESS) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][I2C] Failed read : ret=%d, addr=0x%08X, size=%d\n", ret, i2c_addr, size);
                    return E_DRIVER_ERROR;
                }
            }
#else
//start Raspberry Pi
            if (sem_wait(&i2cSync) != 0) {
                exit(1);
            }

            int fd;
            char i2c_device_name[I2C_DEVICE_NAME_LEN];
            snprintf(i2c_device_name, sizeof(i2c_device_name), "/dev/i2c-%d", DEFAULT_I2C_DEVICE);
			fd = open(i2c_device_name, O_RDWR);
			if (fd < 0)
			{
                if (sem_post(&i2cSync) != 0) {
                    exit(1);
                }
			    return E_DRIVER_ERROR;
			}
			if (ioctl(fd, I2C_SLAVE_FORCE, I2C_ADDR) < 0)
			{
                if (sem_post(&i2cSync) != 0) {
                    exit(1);
                }
			    close(fd);
				return E_DRIVER_ERROR;
			}
			//read address

            unsigned char msg[4];
            //for (uint32_t i = 0; i < size; i++) {
                msg[0] = ((i2c_addr) & 0xFF00) >> 8;
                msg[1] = (i2c_addr) & 0xFF ;
                write(fd, &msg, 2);
                read(fd, &p_buf, size);
//                PRINTF("[IMX500LIB][I2C] read : addr=0x%08X, value0=%d\n", i2c_addr, p_buf[0]);
//                PRINTF("[IMX500LIB][I2C] read : addr=0x%08X, value1=%d\n", i2c_addr, p_buf[1]);
//                PRINTF("[IMX500LIB][I2C] read : addr=0x%08X, value2=%d\n", i2c_addr, p_buf[2]);
//                PRINTF("[IMX500LIB][I2C] read : addr=0x%08X, value3=%d\n", i2c_addr, p_buf[3]);
            //}
            close(fd);
            if (sem_post(&i2cSync) != 0) {
                exit(1);
            }
//end Raspberry Pi

#endif /*  EXEC_ON_SSP300_ENV */

            /* set buffer */
            *val = 0;
            for (uint32_t i = 0; i < size; i++) {
                *val += static_cast<uint32_t>(p_buf[i] << ((size - 1 - i) * 8));
            }
            return E_OK;
        }

    } /* namespace i2c_access */
} /* namespace imx500 */
