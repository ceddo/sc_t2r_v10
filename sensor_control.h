/*
* Copyright 2017-2018 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/
#ifndef SENSOR_CONTROL_H_
#define SENSOR_CONTROL_H_

#include <stdint.h>
#include "return_code.h"

/*
* Input Tensor Namespace
*/
namespace imx500 {
    namespace control {

        typedef struct tagBootParam {
            uint32_t    m_inckFreq;
            uint32_t    m_flashAddrLoader;
            uint32_t    m_flashAddrMainFw;
            uint32_t    m_flashAddrNetWk;
            uint32_t    m_imageSizeLoader;
            uint32_t    m_imageSizeMainFw;
            uint32_t    m_imageSizeNetWk;
            uint32_t    m_imageSizeDwpCp;
            uint8_t*    m_pImageDataLoader;
            uint8_t*    m_pImageDataMainFw;
            uint8_t*    m_pImageDataNetWk;
            uint8_t*    m_pImageDataDwpCp;
            uint8_t     m_imgOnlyEna;
            uint8_t     m_bootMode;
            uint8_t     m_flashUpdateEna;
            uint8_t     m_flashType;
            uint32_t    m_mipiDataRate;
        } BootParam;

        void init(void* pDrvObj);       /* for SSP300 */
        E_RESULT_CODE open(BootParam* param);
        E_RESULT_CODE start();
        E_RESULT_CODE stop();
        E_RESULT_CODE close();

    } /* namespace control */
} /* namespace imx500 */

#endif /* SENSOR_CONTROL_H_ */
