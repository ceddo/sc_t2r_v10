/*
* Copyright 2017-2018 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/
#ifndef _IMAGE_LOADER_H_
#define _IMAGE_LOADER_H_

#include <return_code.h>
#include <stdio.h>
#include <stdint.h>

typedef void * IMX500SF_ModuleHandle;

namespace imx500 {
    namespace control {
        namespace imageloader {
            E_RESULT_CODE  Imx500SF_Open(IMX500SF_ModuleHandle *pHandle);
            E_RESULT_CODE  Imx500SF_Close(IMX500SF_ModuleHandle pHandle);
            E_RESULT_CODE  Imx500SF_SpiBoot_LoaderFW(IMX500SF_ModuleHandle pHandle, uint8_t *pLoaderFW,uint32_t size);
            E_RESULT_CODE  Imx500SF_SpiBoot_MainFW(IMX500SF_ModuleHandle pHandle, uint8_t *pMainFw,uint32_t size);
            E_RESULT_CODE  Imx500SF_SpiBoot_NetworkWeights(IMX500SF_ModuleHandle pHandle, uint8_t *pNetworkWeights,uint32_t size);
            E_RESULT_CODE  Imx500SF_FlashBoot_LoaderFW(IMX500SF_ModuleHandle pHandle, uint32_t flashAddr);
            E_RESULT_CODE  Imx500SF_FlashBoot_MainFW(IMX500SF_ModuleHandle pHandle,	uint32_t flashAddr);
            E_RESULT_CODE  Imx500SF_FlashBoot_NetworkWeights(IMX500SF_ModuleHandle pHandle,uint32_t flashAddr);
            E_RESULT_CODE  Imx500SF_FlashUpdate_LoaderFW(IMX500SF_ModuleHandle pHandle, uint32_t flashAddr, uint8_t * pLoaderFW,uint32_t size);
            E_RESULT_CODE  Imx500SF_FlashUpdate_MainFW(IMX500SF_ModuleHandle pHandle, uint32_t flashAddr, uint8_t *pMainFW, uint32_t size);
            E_RESULT_CODE  Imx500SF_FlashUpdate_NetworkWeights(IMX500SF_ModuleHandle pHandle, uint32_t flashAddr, uint8_t *pNetworkWeights, uint32_t size);
            E_RESULT_CODE  Imx500SF_NetworkWeightUpdate_Spi(IMX500SF_ModuleHandle pHandle, uint8_t *pNetworkWeights, uint32_t size);
            E_RESULT_CODE  Imx500SF_NetworkWeightUpdate_Flash(IMX500SF_ModuleHandle pHandle, uint32_t flashAddr);
            E_RESULT_CODE  Imx500SF_MainFwUpdate_Spi(IMX500SF_ModuleHandle pHandle, uint8_t *pMainFw, uint32_t size);
        }
    }
}
#endif  // _IMAGE_LOADER_H_