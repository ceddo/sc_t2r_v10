/*
Copyright (c) 2017, Raspberry Pi Foundation
Copyright (c) 2017, Dave Stevenson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the copyright holder nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// These values are copied from https://www.raspberrypi.org/forums/viewtopic.php?f=43&t=162722&p=1052339
// Raspberry Pi can not discuss these settings as we have information from
// Sony under NDA.

// REQUESTS FOR SUPPORT ABOUT THESE REGISTER VALUES WILL
// BE IGNORED BY PI TOWERS.

#ifndef IMX500MODES_H_
#define IMX500MODES_H_
struct sensor_regs imx500_8MPix[] =
{
};


struct mode_def imx500_modes[] = {
   {
      .regs          = imx500_8MPix,
      .num_regs      = NUM_ELEMENTS(imx500_8MPix),
      .width         = 2028,
      .height        = 1520,
      .encoding      = 0,
      .order         = BAYER_ORDER_BGGR,
      .native_bit_depth = 10,
      .image_id      = 0x2B,
      .data_lanes    = 2,
      .min_vts       = 2504,
      .line_time_ns  = 18904,
      .timing        = {0, 0, 0, 0, 0},
      .term          = {0, 0},
      .black_level   = 66,
   },
};

//From https://android.googlesource.com/kernel/bcm/+/android-bcm-tetra-3.10-lollipop-wear-release/drivers/media/video/imx219.c
struct sensor_regs imx500_stop[] = {
      /* to power down */
      {0x0100, 0x00},          /* disable streaming  */
};

// ID, exposure, and gain register settings taken from
// https://android.googlesource.com/kernel/bcm/+/android-bcm-tetra-3.10-lollipop-wear-release/drivers/media/video/imx219.c
// Flip settings taken from https://github.com/rellimmot/Sony-IMX219-Raspberry-Pi-V2-CMOS/blob/master/imx219mipiraw_Sensor.c#L585
struct sensor_def imx500 = {
      .name =                 "imx500",
      .modes =                imx500_modes,
      .num_modes =            NUM_ELEMENTS(imx500_modes),
      .stop =                 imx500_stop,
      .num_stop_regs =        NUM_ELEMENTS(imx500_stop),

      .i2c_addr =             0x1A,
      .i2c_addressing =       2,
      .i2c_ident_length =     2,
      .i2c_ident_reg =        0x0000,
      .i2c_ident_value =      0,     // 0x0500 bytes reversed

      //.vflip_reg =            0x172,
      //.vflip_reg_bit =        1,
      //.hflip_reg =            0x172,
      //.hflip_reg_bit =        0,

      .exposure_reg =         0x015A,
      .exposure_reg_num_bits = 16,

      .vts_reg =              0x0160,
      .vts_reg_num_bits =     16,

      .gain_reg =             0x0157,
      .gain_reg_num_bits =    8,    //Only valid up to 230.
};

#endif
