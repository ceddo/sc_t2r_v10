/*
* Copyright 2017-2018 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/

#include <iostream>
#include <algorithm>
#include <cstdint>
#include <cstring>
#include "data_splitter.h"
#include "common_define.h"
#include "dbg_log.h"


static 	uint16_t get_dataType(uint16_t* src, uint16_t line_size);
static void extract_4byte_from_5byte(uint32_t lineSize, uint8_t* extract_data, uint8_t * extracted_data);
static void extract_1byte_from_2byte(uint32_t lineSize, uint16_t* extract_data, uint8_t * extracted_data);

namespace data_splitter {

	
    uint16_t data_split_10bit_packed(const struct InputDataInfo* in, struct OutputDataInfo* out[], struct whcInfo* whc) {

		uint16_t lineSize = whc->pitch;

        uint32_t kpi_linenum = 0;
        uint32_t input_tensor_linenum = in->input_tensor_line;
        uint32_t output_tensor_linenum = in->output_tensor_line;
        uint32_t raw_linenum = in->raw_data_line;
        
        /* RAW */
        out[3]->addr = in->addr;
        out[3]->line = raw_linenum;
        out[3]->size = raw_linenum * (size_t)lineSize;

        out[0]->addr = out[3]->addr + raw_linenum * (size_t)lineSize;
        out[0]->size = kpi_linenum * (size_t)lineSize;
        /* Input Tensor */
        uint8_t* inputTensorAddr = out[0]->addr + out[0]->size;
        uint8_t* inputTensorOutAddr = (uint8_t*)inputTensorAddr;
        for (uint32_t i = 0; i < input_tensor_linenum; i++) {
			extract_4byte_from_5byte(lineSize, inputTensorAddr + lineSize * i, inputTensorOutAddr + lineSize * i);
        }
    	out[1]->addr = inputTensorOutAddr;
        out[1]->line = lineSize * 4/5;
        out[1]->size = input_tensor_linenum * (size_t)out[1]->line;
        /* Output Tensor */
        uint8_t* outputTensorAddr = out[1]->addr + input_tensor_linenum * (size_t)lineSize;
        uint8_t* outputTensorOutAddr = (uint8_t*)outputTensorAddr;
        for (uint32_t i = 0; i < output_tensor_linenum; i++) {
			extract_4byte_from_5byte(lineSize, outputTensorAddr + lineSize * i, outputTensorOutAddr + lineSize * i);
        }
    	out[2]->addr = outputTensorOutAddr;
        out[2]->line = lineSize * 4/5;
        out[2]->size = output_tensor_linenum * (size_t)out[2]->line;
        return 0;
    }

	
	    uint16_t data_split_10bit_unpacked(const struct InputDataInfo* in, struct OutputDataInfo* out[], struct whcInfo* whc) {

        uint16_t* input_mem = (uint16_t*)in->addr;
        uint16_t lineSize = whc->pitch;
        uint32_t numOfline = whc->height;

        uint32_t kpi_linenum = 0;
        uint32_t input_tensor_linenum = 0;
        uint32_t output_tensor_linenum = 0;
		uint32_t raw_linenum = 0;


        
		for (size_t i = 0; i < numOfline; i++) {
			uint16_t dataType = get_dataType(input_mem, lineSize * sizeof(uint16_t));

			switch (dataType) {
			case 0x33: //parameter���\��
				kpi_linenum++;
				break;
			case 0x34://parameter���\��
				input_tensor_linenum++;
				break;
			case 0x35://parameter���\��
				output_tensor_linenum++;
				break;
			default:
                //raw_linenum++;
				break;
			}

			input_mem += lineSize;
        }

        /* RAW */
        out[3]->addr = in->addr;
        out[3]->line = raw_linenum;
        out[3]->size = raw_linenum * (size_t)lineSize * 2;

        out[0]->addr = out[3]->addr + raw_linenum * (size_t)lineSize;
        out[0]->size = kpi_linenum * (size_t)lineSize * 2;
        /* Input Tensor */
        uint16_t* inputTensorAddr = (uint16_t*)(out[0]->addr + (kpi_linenum * (size_t)lineSize * 2));
        uint8_t* inputTensorOutAddr = (uint8_t*)inputTensorAddr;
        for (uint32_t i = 0; i < input_tensor_linenum; i++) {
            extract_1byte_from_2byte(lineSize, inputTensorAddr + lineSize * i, inputTensorOutAddr + lineSize * i);
        }
        out[1]->addr = inputTensorOutAddr;
        out[1]->line = lineSize;
        out[1]->size = input_tensor_linenum * (size_t)out[1]->line;
        /* Output Tensor */
        uint16_t* outputTensorAddr = (uint16_t*)(out[1]->addr + (input_tensor_linenum * (size_t)lineSize * 2));
        uint8_t* outputTensorOutAddr = (uint8_t*)outputTensorAddr;
        for (uint32_t i = 0; i < output_tensor_linenum; i++) {
            extract_1byte_from_2byte(lineSize, outputTensorAddr + lineSize * i, outputTensorOutAddr + lineSize * i);
        }
        out[2]->addr = outputTensorOutAddr;
        out[2]->line = lineSize;
        out[2]->size = output_tensor_linenum * (size_t)out[2]->line;
        return 0;
    }

	
	
	uint16_t data_split_8bit_packed(const struct InputDataInfo* in, struct OutputDataInfo* out[], struct whcInfo* whc) {

        uint16_t lineSize = whc->pitch;

        uint32_t kpi_linenum = in->kpi_line;
        uint32_t input_tensor_linenum = in->input_tensor_line;
        uint32_t output_tensor_linenum = in->output_tensor_line;
        uint32_t raw_linenum = in->raw_data_line;
        uint32_t pq_setting_linenum = in->pq_setting_line;


        /* RAW */
        out[SPLITTER_ARRAY_RAW]->addr = in->addr;
        out[SPLITTER_ARRAY_RAW]->line = raw_linenum;
        out[SPLITTER_ARRAY_RAW]->size = raw_linenum * (size_t)lineSize;

        /* KPI */
        out[SPLITTER_ARRAY_KPI]->addr = out[SPLITTER_ARRAY_RAW]->addr + raw_linenum * (size_t)lineSize;
        out[SPLITTER_ARRAY_KPI]->size = kpi_linenum * (size_t)lineSize;

        /* Input Tensor */
        out[SPLITTER_ARRAY_INPUTTENSOR]->addr = out[SPLITTER_ARRAY_KPI]->addr + out[SPLITTER_ARRAY_KPI]->size;
        out[SPLITTER_ARRAY_INPUTTENSOR]->line = lineSize;
        out[SPLITTER_ARRAY_INPUTTENSOR]->size = input_tensor_linenum * (size_t)out[SPLITTER_ARRAY_INPUTTENSOR]->line;
        /* CodeSonar Check */
        if (input_tensor_linenum != (out[SPLITTER_ARRAY_INPUTTENSOR]->size / (size_t)out[SPLITTER_ARRAY_INPUTTENSOR]->line)) {
            return 1;
        }

        /* Output Tensor */
        out[SPLITTER_ARRAY_OUTPUTTENSOR]->addr = out[SPLITTER_ARRAY_INPUTTENSOR]->addr + out[SPLITTER_ARRAY_INPUTTENSOR]->size;
        out[SPLITTER_ARRAY_OUTPUTTENSOR]->line = lineSize;
        out[SPLITTER_ARRAY_OUTPUTTENSOR]->size = output_tensor_linenum * (size_t)out[SPLITTER_ARRAY_OUTPUTTENSOR]->line;

        /* PQ Setting */
        out[SPLITTER_ARRAY_PQ_SETTING]->addr = out[SPLITTER_ARRAY_OUTPUTTENSOR]->addr + out[SPLITTER_ARRAY_OUTPUTTENSOR]->size;
        out[SPLITTER_ARRAY_PQ_SETTING]->line = lineSize;
        out[SPLITTER_ARRAY_PQ_SETTING]->size = pq_setting_linenum * (size_t)out[SPLITTER_ARRAY_PQ_SETTING]->line;

        return 0;
    }
}

static 	uint16_t get_dataType(uint16_t* src, uint16_t line_size)
{
	uint32_t size = (((*(src + 2) << 8 | *(src + 1)) * 16 / 10 + 0x3F + ((MIPI_PH_SIZE + MIPI_PF_SIZE) * 2)) / 0x40) * 0x40;

	if (size == line_size) {
		return ((*(uint8_t*)src) & 0x3f);
	}
	else {
		return 0;
	}
}

static void extract_4byte_from_5byte(uint32_t lineSize, uint8_t* extract_data, uint8_t * extracted_data) {
    for (uint32_t i = 0; i < lineSize; i += 5) {
        *extracted_data++ = (uint8_t)*(extract_data + i) & 0xFF;
        *extracted_data++ = (uint8_t)*(extract_data + i + 1) & 0xFF;
        *extracted_data++ = (uint8_t)*(extract_data + i + 2) & 0xFF;
        *extracted_data++ = (uint8_t)*(extract_data + i + 3) & 0xFF;
    }
}

static void extract_1byte_from_2byte(uint32_t lineSize, uint16_t* extract_data, uint8_t * extracted_data) {
	for (uint32_t i = MIPI_PH_SIZE; i < lineSize; i++) {
        *extracted_data++ = (uint8_t)((*(extract_data + i) >> 2) & 0xFF);
    }
}

