/*
* Copyright 2017-2018 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/
#ifndef I2C_ACCESS_H_
#define I2C_ACCESS_H_

#include <stdint.h>
#include "return_code.h"

namespace imx500 {

    #define I2C_ACCESS_WRITE(addr, val, size) {                                             \
                E_RESULT_CODE ret = imx500::i2c_access::I2cWrite((addr), (val), (size));    \
                if (ret != E_OK) {                                                          \
                    return ret;                                                             \
                }                                                                           \
            }

    #define I2C_ACCESS_READ(addr, val, size) {                                              \
                E_RESULT_CODE ret = imx500::i2c_access::I2cRead((addr), (val), (size));     \
                if (ret != E_OK) {                                                          \
                    return ret;                                                             \
                }                                                                           \
            }

    namespace i2c_access {

#ifdef EXEC_ON_SSP300_ENV
        void I2cOpen(void* pI2cDrvObj);
#else  
        void I2cOpen(void* pI2cDrvObj);
#endif /*  EXEC_ON_SSP300_ENV */
        E_RESULT_CODE I2cWrite(uint32_t addr, uint32_t val, uint32_t size);
        E_RESULT_CODE I2cRead(uint32_t addr, uint32_t* val, uint32_t size);

    } /* namespace i2c_access */
} /* namespace imx500 */

#endif  /* I2C_ACCESS_H_ */
