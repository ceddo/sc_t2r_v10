/*
 * Copyright 2017-2018 Sony Semiconductor Solutions Corporation.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
 * Solutions Corporation.
 * No part of this file may be copied, modified, sold, and distributed in any
 * form or by any means without prior explicit permission in writing from
 * Sony Semiconductor Solutions Corporation.
 *
 */
#ifndef OBJECTDETECTION_INTERFACE_H_
#define OBJECTDETECTION_INTERFACE_H_

//#include <senscord/serialize.h>

namespace imx500 {
	namespace objectdetection_interface {
		struct Bbox {
			float x_min;
			float y_min;
			float x_max;
			float y_max;

	//		SENSCORD_SERIALIZE_DEFINE(x_min, y_min, x_max, y_max);
		};

		struct ObjectDetectionOutputTensor {
			float numOfDetections;
			std::vector<Bbox> bboxes;
			std::vector<float> scores;
			std::vector<float> classes;

	//		SENSCORD_SERIALIZE_DEFINE(numOfDetections, bboxes, scores, classes);
		};

	}
}
#endif  // OBJECTDETECTION_INTERFACE_H_
