#! /bin/sh
echo "OTP Write start!"
./i2cwrite 1a 0A02 1D # page29
./i2cwrite 1a 0A00 03 # data_transfer_if1_0_ctrl_wen, data_transfer_if1_0_ctrl_en
./i2cwrite 1a 0A04 01 # data_transfer_if1_1_data0: icpu_reserved=1
./i2cwrite 1a 0A05 02 # data_transfer_if1_1_data1: flash_type=2(x8)
./i2cwrite 1a 0A43 00 # OTPIF0_data63
echo "OTP Write done!"
