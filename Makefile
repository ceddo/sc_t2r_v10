ifeq ($(OS),Windows_NT)
    CROSS_COMPILE = C:/SysGCC/raspberry/bin/arm-linux-gnueabihf-
    SYSROOT_PATH = C:/SysGCC/raspberry/arm-linux-gnueabihf/sysroot
    COMPILE_EXTENSION =.exe
    ARCH = arm
    CLEAN_CMD = del
else
    CROSS_COMPILE =
    SYSROOT_PATH =
    COMPILE_EXTENSION =
    CLEAN_CMD = -rm -f
endif


CC	:= $(CROSS_COMPILE)gcc$(COMPILE_EXTENSION)
LD	:= $(CROSS_COMPILE)g++$(COMPILE_EXTENSION)
CFLAGS ?= -I$(SYSROOT_PATH)/opt/vc/include -I./libs/opencv-4.1.0/include/opencv4 -pipe -W -Wall -Wextra -g -O0 -I. -I./flatbuffers
CXXFLAGS ?= $(CFLAGS)

ifeq ($(OS),Windows_NT)
CFLAGS += -mcpu=cortex-a72
CXXFLAGS += -mcpu=cortex-a72
endif

LDFLAGS	?=
LIBS	:= -L$(SYSROOT_PATH)/opt/vc/lib -L./libs/opencv-4.1.0/lib -lrt -lbcm_host -lvcos -lmmal_core -lmmal_util -lmmal_vc_client -lvcsm -lopencv_core -lopencv_highgui -lopencv_imgproc -lopencv_imgcodecs  -lpthread

%.o : %.c
	$(CC) $(CFLAGS) -c -o $@ $<
VPATH = BXD
%.o : %.cpp
	$(LD) $(CXXFLAGS) -c -o $@ $<

all: sc_t2r

sc_t2r: sc_t2r.o RaspiCLI.o data_splitter.o utils.o input_tensor_parser.o output_tensor_parser.o classification_analyser.o objectdetection_analyser.o i2c_access.o sensor_control.o sensor_param_manager.o sensor_property.o imx500lib_sample.o image_loader.o data_injection.o regsetting.o dbg_log.o imx500_hooks.o end_user_app.o crop_roi.o detection_handler.o helpers.o image_handler.o object_detection_model.o
	$(LD) $(LDFLAGS) -o $@ $^ $(LIBS)

clean:
	$(CLEAN_CMD) *.o
	$(CLEAN_CMD) sc_t2r

