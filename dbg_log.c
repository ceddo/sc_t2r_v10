/*
* Copyright 2020-2021 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/

#include <stdio.h>
#include <stdint.h>
#include "dbg_log.h"


DBG_LOG_Level g_dbgLogLevel = DBG_LOG_LVL_INFO;


/**
* @brief DBG_LOG_Init.
*/
void DBG_LOG_Init(uint32_t level) {

    g_dbgLogLevel = (DBG_LOG_Level)level;
}

