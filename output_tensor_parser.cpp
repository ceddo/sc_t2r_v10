/*
 * Copyright 2017-2018 Sony Semiconductor Solutions Corporation.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
 * Solutions Corporation.
 * No part of this file may be copied, modified, sold, and distributed in any
 * form or by any means without prior explicit permission in writing from
 * Sony Semiconductor Solutions Corporation.
 *
 */

#include <iostream>
#include <cstdint>
#include <cstring>
#include <cmath>
#include "output_tensor_parser.h"
#include "apParams.flatbuffers_generated.h"
#include "classification_interface.h"
#include "objectdetection_interface.h"
#include "utils.h"
#include "common_define.h"
#include "data_injection.h"
#include "dbg_log.h"


using namespace imx500::utils;

#define MAX_AP_PARAM_SIZE 4084 // 4KB - 12B(Header)

typedef enum tagTensorDatatype {
	TYPE_SIGNED = 0,
	TYPE_UNSIGNED
}TensorDataType;

#define bytes_to_uint16(MSB,LSB) (((uint16_t) ((unsigned char) MSB)) & 255)<<8 | (((unsigned char) LSB)&255) 
#define bytes_to_int16(MSB,LSB) (((int16_t) ((unsigned char) MSB)) & 255)<<8 | (((unsigned char) LSB)&255) 
#define extract_byte(MSB,LSB) (((((unsigned char) MSB)<<6) & 0xc0) | ((((unsigned char) LSB) & 0xfc) >> 2))
#define DIMENSION_MAX   3

float* outputTensorAddress = NULL;
namespace imx500{
	namespace output_tensor_parser {

		/* Parse AP Params */
		int32_t parseApParams(const char *bodyApParams, const DnnHeader* dnnHeader, FrameOutputInfo* frameOutputInfo) {
			const apParams::fb::FBApParams* fbApParams;
			//const apParams::fb::FBNetwork fbNetworks[];
			const apParams::fb::FBNetwork* fbNetwork;
			const apParams::fb::FBOutputTensor* fbOutputTensor;

			fbApParams = apParams::fb::GetFBApParams(bodyApParams);
			//fbNetworks = fbApParams->networks();
			DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[OUT_TENSOR] Networks size:%d\n", fbApParams->networks()->size());
			for (uint32_t i = 0; i < fbApParams->networks()->size(); i++) {
				fbNetwork = (apParams::fb::FBNetwork*)(fbApParams->networks()->Get(i));
				if (fbNetwork->id() == dnnHeader->networkId) {
					//Network = fbApParams->networks[i];
					frameOutputInfo->networkType = (char*)fbNetwork->type()->c_str();
					DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[OUT_TENSOR] Input Tensors size:%d\n", fbNetwork->inputTensors()->size());
					DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[OUT_TENSOR] Output Tensors size:%d\n", fbNetwork->outputTensors()->size());

					for (uint32_t j = 0; j < fbNetwork->outputTensors()->size(); j++) {
						OutputTensorApParams outApParam;

						fbOutputTensor = (apParams::fb::FBOutputTensor*)fbNetwork->outputTensors()->Get(j);

						outApParam.id = fbOutputTensor->id();
						outApParam.name = (char*)fbOutputTensor->name()->c_str();
						outApParam.numOfDimensions = fbOutputTensor->numOfDimensions();

						for (uint8_t k = 0; k < fbOutputTensor->numOfDimensions(); k++) {
							Dimensions dim;
							dim.size = fbOutputTensor->dimensions()->Get(k)->size();
							dim.serializationIndex = fbOutputTensor->dimensions()->Get(k)->serializationIndex();
							dim.padding = fbOutputTensor->dimensions()->Get(k)->padding();
							if (dim.serializationIndex != 0 && dim.padding != 0) {
								DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] Error in AP Params, Non-Zero padding for serialized Dimension %d", k);
								return -1;
							}
							outApParam.vecDim.push_back(dim);
						}
						outApParam.bitsPerElement = fbOutputTensor->bitsPerElement();
						outApParam.shift = fbOutputTensor->shift();
						outApParam.scale = fbOutputTensor->scale();
						outApParam.format = fbOutputTensor->format();

						/* Add the element to vector */
						frameOutputInfo->outputApParams->push_back(outApParam);
					}
					break;
				}
			}

			return 0;
		}

		/* Parse Output Tensor Header data consisting of DNN Haeder and AP Params */
		int32_t parseOutputTensorHeader(const uint8_t* src,
			uint16_t lineSize,
			FrameOutputInfo* frameOutputTensorInfo) {

			const uint8_t * readPonter = src;

			/* Extract Dnn Header */
			utils::extract_dnn_header(readPonter, frameOutputTensorInfo->dnnHeaderInfo);
			DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[OUT_TENSOR] after extarct dnn header\n");
			readPonter += DNN_HDR_SIZE;

			if (frameOutputTensorInfo->dnnHeaderInfo->frameValid != 1) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] Invalid Frame, valid: %d\n", frameOutputTensorInfo->dnnHeaderInfo->frameValid);
				return -1;
			}
			
			if (frameOutputTensorInfo->dnnHeaderInfo->tensorType != TYPE_OUTPUT_TENSOR) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] Wrong tensorType: %d\n", frameOutputTensorInfo->dnnHeaderInfo->tensorType);
				return -1;
			}
			if (frameOutputTensorInfo->dnnHeaderInfo->apParamSize > MAX_AP_PARAM_SIZE) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] Invalid AP Param size: %d\n", frameOutputTensorInfo->dnnHeaderInfo->apParamSize);
				return -1;
			}
			if (frameOutputTensorInfo->dnnHeaderInfo->maxLineLen == 0) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] Invalid max line length: 0\n");
				return -1;
			}
			char* ap_params = new char[frameOutputTensorInfo->dnnHeaderInfo->apParamSize];
			if (ap_params == NULL) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] Error allocating memory\n");
				return -1;
			}
			memset(ap_params,0,frameOutputTensorInfo->dnnHeaderInfo->apParamSize);

			uint32_t lineLen = lineSize;
			uint32_t lineIndex = DNN_HDR_SIZE;

			for (int j = 0; j < frameOutputTensorInfo->dnnHeaderInfo->apParamSize; j++) {
				if (lineIndex >= lineLen) {
					lineIndex = 0;
					src += lineSize;
					readPonter = src + MIPI_PH_SIZE;
				}
				ap_params[j] = (uint8_t)(*readPonter++);
				lineIndex++;
			}
			DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[OUT_TENSOR] before parse ap_params\n");
			int32_t ret = parseApParams((const char*)ap_params, (const DnnHeader*)frameOutputTensorInfo->dnnHeaderInfo, frameOutputTensorInfo);
			if (ret != 0) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] Error parsing AP Params ret =%d\n", ret);
				delete[] ap_params;
				return ret;
			}
			DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[OUT_TENSOR] after parse ap_params\n");
			delete[] ap_params;
			return 0;
		}


		/* Parse Output Tensor Body data */
		int32_t parseOutputTensorBody(const uint8_t* src, const uint16_t lineSize, FrameOutputInfo* frameOutputTensorInfo) {
			/* Input Parameter Validation*/
			if (src == NULL) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] Error src address is NULL\n");
				return -1;
			}
			if (frameOutputTensorInfo == NULL || frameOutputTensorInfo->outputBodyInfo->address == NULL) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] Error Header info  is NULL\n");
				return -1;
			}
			
			if (frameOutputTensorInfo->outputApParams == NULL) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] Error outApParams is NULL\n");
				return -1;
			}
			uint32_t offset = 0;
			int32_t retStatus = 0;

			float* dst = frameOutputTensorInfo->outputBodyInfo->address;
			/* CodeSonar Check */
			if (frameOutputTensorInfo->outputBodyInfo->totalSize > (UINT32_MAX / sizeof(float))) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] Error totalSize is greater than maximum size\n");
				return -1;
			}
			float tmp_dst[frameOutputTensorInfo->outputBodyInfo->totalSize];
			memset(tmp_dst, 0, frameOutputTensorInfo->outputBodyInfo->totalSize);

			for (unsigned int tensorIdx = 0; tensorIdx < frameOutputTensorInfo->outputApParams->size(); tensorIdx++) {
				DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[OUT_TENSOR] ====Parsing output Tensor = %d =========\n", tensorIdx);
				uint32_t outputTensorSize = 0;
				uint32_t outSize = 1;
				uint32_t inSize = 1;
				uint8_t padding = 0;
				uint32_t TensorDataNum = 0;

				OutputTensorApParams param = frameOutputTensorInfo->outputApParams->at(tensorIdx);
				std::vector<Dimensions> serialized_dim(param.numOfDimensions);
				std::vector<Dimensions> actual_dim(param.numOfDimensions);

				for (int idx = 0; idx < param.numOfDimensions; idx++) {
					actual_dim[idx].size = param.vecDim.at(idx).size;
					serialized_dim[param.vecDim.at(idx).serializationIndex].size = param.vecDim.at(idx).size;
					if (param.vecDim.at(idx).serializationIndex == 0) {
						padding = param.vecDim.at(idx).padding;
						inSize *= (param.vecDim.at(idx).size + param.vecDim.at(idx).padding)* (param.bitsPerElement / 8);
						if(outSize >= UINT32_MAX / param.vecDim.at(idx).size) {
							DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] Invalid output tensor info");
							return -1;
						}
						outSize *= param.vecDim.at(idx).size;
						if(outSize >= UINT32_MAX / param.bitsPerElement / 8) {
							DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] Invalid output tensor info");
							return -1;
						}
						outSize *= param.bitsPerElement / 8;
					}
					else {
						inSize *= (param.vecDim.at(idx).size);
						if(outSize >= UINT32_MAX / param.vecDim.at(idx).size) {
							DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] Invalid output tensor info");
							return -1;
						}
						outSize *= param.vecDim.at(idx).size;
					}
					actual_dim[idx].serializationIndex = param.vecDim.at(idx).serializationIndex;
					serialized_dim[param.vecDim.at(idx).serializationIndex].serializationIndex = (uint8_t)idx;

					DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[OUT_TENSOR] param.vecDim[%d].size = %d, param.vecDim[%d].padding=%d inSize=%d\n", idx, param.vecDim.at(idx).size , idx,param.vecDim.at(idx).padding,inSize);
				}
				outputTensorSize = outSize;
				if (outputTensorSize == 0) {
					DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] Invalid output tensorsize = %d\n", outputTensorSize);
					retStatus = -1;
					break;
				}

				/* For Debug */
				DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[OUT_TENSOR] ========Dimensions=========\n");
				for (int idx = 0; idx < param.numOfDimensions; idx++) {
					DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[OUT_TENSOR] actual_dim[%d] = %d, serialized_dim[%d]=%d\n",
								  idx,
								  actual_dim[idx].serializationIndex,
								  idx,
								  serialized_dim[idx].serializationIndex);
				}
				DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[OUT_TENSOR] ===========================\n");

				uint16_t numOfLines = (uint16_t)ceil(inSize / (float)frameOutputTensorInfo->dnnHeaderInfo->maxLineLen);
				DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[OUT_TENSOR] inSize=%d ,outSize=%d, numOfLines=%d\n", inSize, outSize, numOfLines);

				uint8_t* out_tensor = (uint8_t*) new char[outputTensorSize];
				if (out_tensor == NULL) {
					DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] Error allocating memory");
					retStatus = -1;
					break;
				}
				memset(out_tensor,0,outputTensorSize);

				/*Extract output tensor data*/
				uint32_t elementIndex = 0;
				uint32_t paddedDimIndex = 0;
				uint32_t dbg_inputCount = 0;
				for (int i = 0; i < numOfLines; i++) {
					int lineIndex = 0;
					while (lineIndex < frameOutputTensorInfo->dnnHeaderInfo->maxLineLen) {
						out_tensor[elementIndex] = (uint8_t)*(src + lineIndex);
						elementIndex++;
						lineIndex++;
						paddedDimIndex++;
						dbg_inputCount++;
						if (paddedDimIndex == (serialized_dim[0].size * (param.bitsPerElement / 8))) {

							//PRINTF("lineIndex==size of dim1: elementIndex=%d lineIndex=%d\n", elementIndex, lineIndex);
							lineIndex += (padding * (param.bitsPerElement / 8));
							dbg_inputCount += (padding * (param.bitsPerElement / 8));
							paddedDimIndex = 0;
							//PRINTF("lineIndex_afterpadding: elementIndex=%d lineIndex=%d\n", elementIndex, lineIndex);
						}
						if (elementIndex == outputTensorSize) {
							//PRINTF("innerloop: elementIndex=%d lineIndex=%d\n", elementIndex, lineIndex);
							break;
						}

					}

					src += lineSize;
					if (elementIndex == outputTensorSize) {
						//PRINTF("outerloop: i=%d image_size=%d\n", i, image_size);
						break;
					}

				}
				DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[OUT_TENSOR] out_ptr=%p ,outSize=%d, inputCount =%d\n",out_tensor,outSize,dbg_inputCount);
				DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[OUT_TENSOR] ===========================\n");   

				/* Calculate Output Tensor values using Ap Param Info*/
				if (param.bitsPerElement == 8) {
					for (uint32_t index = 0; index < outputTensorSize; index++) {
						float value = 0;
						if (param.format == TYPE_SIGNED) {
							int8_t temp = out_tensor[index];
							value = (temp - param.shift) * param.scale;
							//PRINTF("inside 8bit TYPE_SIGNED: scale = %f value=%f\n", param.scale, value);
						}
						else if (param.format == TYPE_UNSIGNED) {
							uint8_t temp = out_tensor[index];
							value = (temp - param.shift) * param.scale;
							//PRINTF("inside 8bit TYPE_UNSIGNED: scale = %f value=%f\n", param.scale, value);
						}
						tmp_dst[offset + index] = value;
						//PRINTF("output_tensor[%d] = %f\n", offset + index, dst[offset + index]);
					}
				}
				else if (param.bitsPerElement == 16) {
					for (uint32_t index = 0; index < (outputTensorSize / 2); index++) {
						float value = 0;
						if (param.format == TYPE_SIGNED) {
							int16_t temp = bytes_to_int16(out_tensor[2 * index + 1], out_tensor[2 * index]);
							value = (temp - param.shift) * param.scale;
							//PRINTF("inside 16 bit TYPE_SIGNED: scale = %f value=%f\n", param.scale, value);
						}
						else if (param.format == TYPE_UNSIGNED) {
							uint16_t temp = bytes_to_uint16(out_tensor[2 * index + 1], out_tensor[2 * index]);
							value = (temp - param.shift) * param.scale;
							//PRINTF("inside 16 bit TYPE_UNSIGNED: scale = %f value=%f\n", param.scale, value);
						}

						//if (value > 1 || value < 0.000001) value = 0;
						tmp_dst[offset + index] = value;
						//PRINTF("output_tensor[%d] = %f\n", offset + index, dst[offset + index]);
					}
				}
				else {
					DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] Invalid bitsPerElement value =%d\n", param.bitsPerElement);
					delete[] out_tensor;
					retStatus = -1;
					break;
				}

				/* Sorting in order according to AP Params. *//* Not supported if larger than 3D */
				/* Preparation */
				uint32_t loop_cnt[DIMENSION_MAX] = {1, 1, 1};
				uint32_t coef[DIMENSION_MAX] = {1, 1, 1};
				for (uint32_t i = 0; i < param.numOfDimensions; i++) {
					if (i >= DIMENSION_MAX) {
						DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] numOfDimensions value is 3 or higher\n");
						break;
					}

					loop_cnt[i] = serialized_dim.at(i).size;

					for (uint32_t j = serialized_dim.at(i).serializationIndex; j > 0; j--) {
						coef[i] *= actual_dim.at(j-1).size;
					}
				}
				/* Sort execution */
				uint32_t src_index = 0;
				uint32_t dst_index;
				for (uint32_t i = 0; i < loop_cnt[DIMENSION_MAX - 1]; i++) {
					for (uint32_t j = 0; j < loop_cnt[DIMENSION_MAX - 2]; j++) {
						for (uint32_t k = 0; k < loop_cnt[DIMENSION_MAX - 3]; k++) {
							dst_index = (coef[DIMENSION_MAX - 1] * i) + (coef[DIMENSION_MAX - 2] * j) + (coef[DIMENSION_MAX - 3] * k);
							dst[offset + dst_index] = tmp_dst[offset + src_index++];
						}
					}
				}

				delete[] out_tensor;
				TensorDataNum = (outputTensorSize / (param.bitsPerElement / 8));
				offset += TensorDataNum;
				if (offset > frameOutputTensorInfo->outputBodyInfo->totalSize) {
					DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] Error in Prasing output tensor offset(%d)>output_size\n", offset);
					retStatus = -1;
					break;
				}
				frameOutputTensorInfo->outputBodyInfo->tensorDataNum[tensorIdx] = TensorDataNum;

			}

			return retStatus;
		}


		int32_t parseOutputTensor(const struct InputDataInfo* in, struct FrameOutputInfo* frameOutTensorInfo) {
			uint32_t output_size = 0;
			struct PacketHeader* refpacketHeader = new PacketHeader();
			frameOutTensorInfo->dnnHeaderInfo = new DnnHeader();
			frameOutTensorInfo->outputApParams = new std::vector<OutputTensorApParams>();
			uint8_t * src;
			int32_t retStatus = 0;

			/* Get input Data pointer*/
			src = in->addr;

			/* Parse output tensor Header Data */
			retStatus = parseOutputTensorHeader(src, in->pitch, frameOutTensorInfo);
			if (retStatus != 0) {
				delete refpacketHeader;
				return retStatus;
			}

			/*Calculate total output size*/
			int numOutputTensors = frameOutTensorInfo->outputApParams->size();
			uint32_t totalOutSize = 0;
			for (int k = 0; k < numOutputTensors; k++) {
				uint32_t totalDimensionSize = 1;
				for (int idx = 0; idx < frameOutTensorInfo->outputApParams->at(k).numOfDimensions; idx++) {
					if(totalDimensionSize >= UINT32_MAX / frameOutTensorInfo->outputApParams->at(k).vecDim.at(idx).size) {
						DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] Invalid output tensor info");
						delete refpacketHeader;
						return -1;
					}
					totalDimensionSize *= frameOutTensorInfo->outputApParams->at(k).vecDim.at(idx).size;
				}
				if(totalOutSize >= UINT32_MAX - totalDimensionSize){
					DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] Invalid output tensor info");
					delete refpacketHeader;
					return -1;
				}
				totalOutSize += totalDimensionSize;
			}

			/* CodeSonar Check */
			if (totalOutSize == 0) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] Invalid output tensor info(totalOutSize is 0)");
				delete refpacketHeader;
				return -1;
			}
			output_size = totalOutSize;
			DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[OUT_TENSOR] final output size = %d\n", output_size);


			/* Set FrameOutputTensor Info */
			frameOutTensorInfo->outputBodyInfo = new OutputTensorInfo();
			memset(frameOutTensorInfo->outputBodyInfo, 0, sizeof(struct OutputTensorInfo));

			if(output_size >= UINT32_MAX / sizeof(float)) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] Invalid output tensor info");
				delete refpacketHeader;
				return -1;
			}

			frameOutTensorInfo->outputBodyInfo->address = (float*) new float[output_size];
			if (frameOutTensorInfo->outputBodyInfo->address == NULL) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] Error allocating memory for output tensor\n");
				delete refpacketHeader;
				return -1;
			}
			memset(frameOutTensorInfo->outputBodyInfo->address, 0, (sizeof(float) * output_size));
			
			frameOutTensorInfo->outputBodyInfo->totalSize = output_size;
			frameOutTensorInfo->outputBodyInfo->tensorNum = numOutputTensors;
			frameOutTensorInfo->outputBodyInfo->tensorDataNum = (uint32_t*) new uint32_t[numOutputTensors];
			if (frameOutTensorInfo->outputBodyInfo->tensorDataNum == NULL) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] Error allocating memory for output tensor size\n");
				delete refpacketHeader;
				return -1;
			}
             memset(frameOutTensorInfo->outputBodyInfo->tensorDataNum, 0, (sizeof(uint32_t) * numOutputTensors));

			/* Get line size (64 byte Align) */
			uint16_t lineSize = in->pitch; //(refpacketHeader->wordCount + 0x1F + MIPI_PH_SIZE + MIPI_PF_SIZE) / 0x20 * 0x20;
			src += lineSize;

			DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[OUT_TENSOR] Calling DataInj saveOutputTensor!!!\n");
            imx500::control::DataInjection::saveOutputTensorinL2MemFormat(src, lineSize, frameOutTensorInfo);

			/*Parse Output Tensor Body*/
			retStatus = parseOutputTensorBody(src, lineSize, frameOutTensorInfo);
			if (retStatus != 0) {
				DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[OUT_TENSOR] Error parsing output tensor body ret=%d\n", retStatus);
			}
			delete refpacketHeader;

			DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[OUT_TENSOR] Parsing OutputTensor Complete!!!\n");
			return retStatus;
		}

		int32_t createClassificationData(FrameOutputInfo* frameOutputInfo, classification_interface::ClassificationOutputTensor* classificationOutput) {
			uint16_t size = frameOutputInfo->outputBodyInfo->totalSize;
			for (uint16_t i = 0; i < size; i++) {
				float score = frameOutputInfo->outputBodyInfo->address[i];
				classificationOutput->scores.push_back(score);
			}
			return 0;
		}

		int32_t createObjectDetectionData(FrameOutputInfo* frameOutputInfo, objectdetection_interface::ObjectDetectionOutputTensor* objectDetectionOutput) {
			float* out_data = frameOutputInfo->outputBodyInfo->address;
			uint32_t count = 0;
			std::vector<objectdetection_interface::Bbox> v_bbox;
			std::vector<float> v_scores;
			std::vector<float> v_classes;

			//Extract number of Detections
			uint8_t numOfDetections = (uint8_t)frameOutputInfo->outputApParams->at(0).vecDim.at(0).size;

			//Extract scores
			for (uint8_t i = 0; i < numOfDetections; i++) {
				float score;
				score = out_data[count];
				v_scores.push_back(score);
				count++;
			}

			//Extract bounding box co-ordinates
			for (uint8_t i = 0; i < numOfDetections; i++) {
				struct objectdetection_interface::Bbox bbox;
				bbox.y_min = out_data[count + i];
				bbox.x_min = out_data[count + i + (1 * numOfDetections)];
				bbox.y_max = out_data[count + i + (2 * numOfDetections)];
				bbox.x_max = out_data[count + i + (3 * numOfDetections)];
				v_bbox.push_back(bbox);
			}
			count += (numOfDetections * 4);

			//Extract class indices
			for (uint8_t i = 0; i < numOfDetections; i++) {
				float class_index;
				if(count > frameOutputInfo->outputBodyInfo->totalSize) {
					return -1;
				}
				class_index = out_data[count];
				v_classes.push_back(class_index);
				count++;
			}

			objectDetectionOutput->numOfDetections = numOfDetections;
			objectDetectionOutput->bboxes = v_bbox;
			objectDetectionOutput->scores = v_scores;
			objectDetectionOutput->classes = v_classes;

			return 0;
		}

		int32_t cleanupFrameData(FrameOutputInfo* frameOutputTensorInfo) {
			if (frameOutputTensorInfo->outputBodyInfo != NULL) {
				delete[] frameOutputTensorInfo->outputBodyInfo->tensorDataNum;
				delete[] frameOutputTensorInfo->outputBodyInfo->address;
				delete frameOutputTensorInfo->outputBodyInfo;
			}
			if (frameOutputTensorInfo->dnnHeaderInfo != NULL) {
				delete frameOutputTensorInfo->dnnHeaderInfo;
			}
			if (frameOutputTensorInfo->outputApParams != NULL) {
				delete frameOutputTensorInfo->outputApParams;
			}
			return 0;
		}
	}
}   // namespace imx500::output_tensor_parser
