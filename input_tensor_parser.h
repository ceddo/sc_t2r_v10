/*
* Copyright 2017-2018 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/
#ifndef INPUT_TENSOR_PARSE_H_
#define INPUT_TENSOR_PARSE_H_

#include <stdint.h>
#include "data_splitter.h"

/*
* Input Tensor Namespace
*/
namespace imx500 {
	namespace input_tensor_parser {

		struct InputDataInfo {
			uint8_t* addr;
			size_t size;
			size_t pitch;
			uint8_t ych_en;
			uint8_t gain;
		};

		struct OutputDataInfo {
			uint8_t* addr;
			uint32_t size;
		};

		struct FrameInputTensorInfo {
			uint8_t networkId;
			uint16_t width;
			uint16_t height;
			uint16_t channel;
			uint16_t widthStride;
			uint16_t heightStride;
			uint8_t format;
		};

		struct InputNormInfo{
			uint32_t norm_val[3][3];
            uint32_t norm_shift[3][3];
            uint32_t input_format[3];
		};

		int32_t parseInputTensor(const struct InputDataInfo* in, struct OutputDataInfo* out,const struct InputNormInfo* norm, struct FrameInputTensorInfo* frameInputTensorInfo);
	}
}
#endif
