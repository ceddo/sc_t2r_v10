/*
* Copyright 2017-2018 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/

#include <stdio.h>
#include <stdint.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include "common_define.h"
#include "i2c_access.h"

#include <sys/ioctl.h>
#include <linux/types.h>
#include <cmath>

#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <time.h>

#include "imx500lib_sample.h"
#include "output_tensor_parser.h"
#include "data_injection.h"
#include "dbg_log.h"


/* SPI Transfer Measurement Enable / Disable */
#define DATAINJECTION_MEASUREMENT_ENABLE                          (0)


/* SPI Transfer Debug */
#define DATAINJECTION_SPI_DEBUG

//#defineDATAINJECTION_SPI_ONE_TRANS_SIZE 32768
#define DATAINJECTION_SPI_ONE_TRANS_SIZE 4096

/* SPI Master Driver Configuration Parameter */
#define DATAINJECTION_SPI_DRV_MAX_SPEED  50000000
//#define DATAINJECTION_SPI_DRV_MAX_SPEED  5000000
#define DATAINJECTION_SPI_DRV_MODE       SPI_MODE_3
#define DATAINJECTION_SPI_DRV_TRANS_BIT_WIDTH    8
#define DATAINJECTION_SPI_DRV_DELAY              0
#define DATAINJECTION_SPI_CS_CHANGE              0

extern char outputTensorFilePath[1023];
extern char inTensorListFile[1023];
char outputTensorfilename[1023];
char outputTensorfullfilename[2048];

extern nw_info networkInfoArray[];


#define REG_VAL_DD_DAT_INJECTION_HNDSK_TRANS_INIT_WAIT 1
#define REG_VAL_DD_DAT_INJECTION_HNDSK_TRANS_INIT_COMP 2
#define REG_VAL_DD_DAT_INJECTION_HNDSK_TRANS_READY_WAIT 3
#define REG_VAL_DD_DAT_INJECTION_HNDSK_TRANS_READY_COMP 4
#define REG_VAL_DD_DAT_INJECTION_HNDSK_TRANSFERRING 5
#define REG_VAL_DD_DAT_INJECTION_HNDSK_TRANS_COMP 6

#define DATAINJECTION_INPUT_TENSOR_MAX_DATA_SIZE 1228800 /* MAX_SIZE: 640x640x3 */


namespace imx500 {
    namespace control {
        namespace DataInjection {
            typedef struct tagINPUTTENSORINPUT {
                int fd;
                int debug;
            } DATAINJECTION_MODULE_INFO;
            //static const char *device = "/dev/spidev0.0";
            static DATAINJECTION_MODULE_INFO  moduleInfo;
            static E_RESULT_CODE InputTensorInput_TransferSpi(int fd, uint8_t *pTransbuf,uint32_t transSize);
            static void GetDirNameAndFileName(char *path,char *dir,char *file, uint32_t dirBufSize, uint32_t fileBufSize);
            static E_RESULT_CODE MakeOutPutFileName(char *sourcefileName, 
                                                    char *dstfileName, 
                                                    uint32_t dstBufSize);
            static E_RESULT_CODE TransferInputTensor(DataInjection_ModuleHandle pHandle, void *inputTensorMem, uint32_t inputTensrSize);
            static uint32_t reciveCnt = 0;
            static volatile uint32_t savedOutputTesnor = 0;
            static volatile uint32_t inputTensordiffEnd = 0;
            static volatile uint32_t inputTensorReSend = 0;
            static void* wk_mem = NULL;

            /**
             * @brief open.
             */
            
            E_RESULT_CODE InputTensorInput_Open(DataInjection_ModuleHandle *pHandle)
            {
                uint8_t mode = DATAINJECTION_SPI_DRV_MODE;
                uint8_t bits = DATAINJECTION_SPI_DRV_TRANS_BIT_WIDTH;
                int32_t ioctrl_ret = 0;
                uint32_t speed =  DATAINJECTION_SPI_DRV_MAX_SPEED;
                int fd;

                *pHandle = (void *)&moduleInfo;
                moduleInfo.debug = 1;
                fd = open("/dev/spidev0.0", O_RDWR);
                if (fd < 0) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] SPIDEV OPEN ERROR !!! \n");
                    return E_OTHER;
                }
                moduleInfo.fd = fd;
                DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[DATINJ] SPI Device Open %x\n", moduleInfo.fd);

                /* spi mode */
                ioctrl_ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
                if (ioctrl_ret == -1) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] SPI_IOC_WR_MODE ERROR !!! \n");
                    close(fd);
                    return E_OTHER;
                }
                #ifdef DATAINJECTION_SPI_DEBUG
                ioctrl_ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
                if (ioctrl_ret == -1){
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "InputTensorInput] SPI_IOC_RD_MODE ERROR !!! \n");
                    close(fd);
                    return E_OTHER;
                }
                else {
                    DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[DATINJ] SPI_IOC_RD_MODE::mode ::%x\n", mode);
                }
                #endif
                /* bits per word */
                ioctrl_ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
                if (ioctrl_ret == -1){
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] SPI_IOC_WR_BITS_PER_WORD ERROR !!!");
                    close(fd);
                    return E_OTHER;
                }
                #ifdef DATAINJECTION_SPI_DEBUG
                ioctrl_ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
                if (ioctrl_ret == -1) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] SPI_IOC_RD_BITS_PER_WORD ERROR !!!");
                    close(fd);
                    return E_OTHER;
                }
                else {
                    DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[DATINJ] SPI_IOC_RD_BITS_PER_WORD::bits ::%x\n", bits);
                }
                #endif

                /* max speed hz */
                ioctrl_ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
                if (ioctrl_ret == -1) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] SPI_IOC_WR_MAX_SPEED_HZ ERROR !!! \n");
                    close(fd);
                    return E_OTHER;
                }

                #ifdef DATAINJECTION_SPI_DEBUG
                ioctrl_ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
                if (ioctrl_ret == -1) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] SPI_IOC_RD_MAX_SPEED_HZ ERROR !!! \n");
                    close(fd);
                    return E_OTHER;
                }
                else {
                    DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[DATINJ] MAX_SPEED_HZ: %d Hz (%d KHz)\n", speed, speed/1000);
                }
                #endif
                return E_OK;
            }
        
            E_RESULT_CODE InputTensorInput_Close(DataInjection_ModuleHandle pHandle)
            {
                DATAINJECTION_MODULE_INFO *pModuleInfo;
                pModuleInfo = (DATAINJECTION_MODULE_INFO *)pHandle;
                DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[DATINJ] SPI Device Close %x\n", pModuleInfo->fd);
                close(pModuleInfo->fd);
                return E_OK;
            }

            E_RESULT_CODE InputTensorInput_TransferInputTensor(DataInjection_ModuleHandle pHandle, uint8_t *pTransbuf,uint32_t size) 
            {
                E_RESULT_CODE ret = E_OK;
                DATAINJECTION_MODULE_INFO *pModuleInfo;
                pModuleInfo = (DATAINJECTION_MODULE_INFO *)pHandle;
                uint8_t *pCurBuf = NULL;
                uint32_t residualSize = 0;
                uint32_t transSize = 0;
    #if  DATAINJECTION_MEASUREMENT_ENABLE
                struct timespec startTime, endTime;
    #endif
    #if  DATAINJECTION_MEASUREMENT_ENABLE
                clock_gettime(CLOCK_REALTIME, &startTime);
    #endif
                DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[DATINJ] InputTensorInpu_TransferInputTensor size %d\n",size);
                DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[DATINJ] SPI Device  %d\n",pModuleInfo->fd);
                /* SPI Transfer */
                residualSize = size;
                pCurBuf = pTransbuf;
                while (1) {
                    if (residualSize == 0) {
                        break;
                    }
                    if (residualSize >= DATAINJECTION_SPI_ONE_TRANS_SIZE) {
                        transSize = DATAINJECTION_SPI_ONE_TRANS_SIZE;
                    } else {
                        transSize = residualSize;
                    }               
                    ret = InputTensorInput_TransferSpi(pModuleInfo->fd,pCurBuf,transSize);
                    if (ret != E_OK) {
                        return ret;
                    }
                    if (transSize >= DATAINJECTION_SPI_ONE_TRANS_SIZE) {
                        pCurBuf = pCurBuf + DATAINJECTION_SPI_ONE_TRANS_SIZE;
                        residualSize = residualSize - DATAINJECTION_SPI_ONE_TRANS_SIZE;
                    } else {
                        residualSize = 0;
                    }               
                }
                #if DATAINJECTION_MEASUREMENT_ENABLE
                    clock_gettime(CLOCK_REALTIME, &endTime);
                    DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "Proc Time = ");
                    if (endTime.tv_nsec < startTime.tv_nsec) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "%10ld.%09ld", endTime.tv_sec - startTime.tv_sec - 1
                                ,endTime.tv_nsec + 1000000000 - startTime.tv_nsec);
                    } else {
                        DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "%10ld.%09ld", endTime.tv_sec - startTime.tv_sec
                                ,endTime.tv_nsec - startTime.tv_nsec);
                    }
                    DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "(second)\n");
                #endif
                return E_OK;
            }


            E_RESULT_CODE DataInjectionMain(void)
            {
                imx500::E_RESULT_CODE ret_code;
                FILE * finlist;
                void* byte_swap_wk_mem;
                uint32_t pos;
                int retval = 0;
                uint32_t regval;
                uint32_t inputFileSize;
                char inputTensorFileName[1023];
                char dirName[1023];
                char fileName[1023];
                uint32_t filneNameLen;
                uint32_t filePathLen;
                FILE *fp = NULL;
                DataInjection_ModuleHandle handle;

                DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[DATINJ] dataInjection start \n");
                
                ret_code = imx500::control::DataInjection::InputTensorInput_Open(&handle);
                if (ret_code != imx500::E_OK) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] ERROR Driver Open. ret=0x%08x", ret_code);
                    return E_OTHER;
                }

                
                while(1) {
                    regval = 0;
                    imx500::i2c_access::I2cRead(REG_ADDR_DD_DAT_INJECTION_HNDSK,&regval,REG_SIZE_DD_DAT_INJECTION_HNDSK);
                    usleep(1000);
                    if (regval == REG_VAL_DD_DAT_INJECTION_HNDSK_TRANS_INIT_COMP) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[DATINJ] Input Tensor Transfer Initlize Done \n");
                        break;
                    }
                }

                // inputfile list 
                if (( finlist = fopen(inTensorListFile, "r")) == NULL) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] !!! Open Error InputTensor File List !!!  %s\n",inTensorListFile);
                    imx500::control::DataInjection::InputTensorInput_Close(handle);
                    return E_OTHER;
                }
                memset(inputTensorFileName,0,sizeof(inputTensorFileName));
                
                while (fscanf(finlist, "%1022s%*[^\n]", inputTensorFileName) != EOF){

                    wk_mem  = malloc(DATAINJECTION_INPUT_TENSOR_MAX_DATA_SIZE);
                    
                    if (wk_mem == NULL) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] Input Swap Memory Malloc error .\n");
                        continue;
                    }
                    
                    fp = fopen(inputTensorFileName,"rb");
                    if (fp != NULL) {
                        retval = image_file_read(fp,(uint8_t *)wk_mem,&inputFileSize);
                        fclose (fp);
                    } else {
                        retval = -1;
                    }

                    if (retval == -1) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] Input Tensor Open Error .\n");
                        free(wk_mem);
                        continue;
                    }

                    byte_swap_wk_mem  = malloc(DATAINJECTION_INPUT_TENSOR_MAX_DATA_SIZE);
                    if (byte_swap_wk_mem == NULL) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] Input Swap Memory Malloc error .\n");
                        free(wk_mem);
                        continue;
                    }
                    
                    memset(dirName,0,sizeof(dirName));
                    memset(fileName,0,sizeof(fileName));
                    
                    GetDirNameAndFileName(inputTensorFileName,dirName,fileName,sizeof(dirName)-1,sizeof(fileName)-1);  
                    DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[DATINJ] dirName :%s\n",dirName);
                    DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[DATINJ] fileName :%s\n",fileName);
                    
                    MakeOutPutFileName(fileName, outputTensorfilename, 1023);
                    DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[DATINJ] outputTensorfilename :%s\n",outputTensorfilename);
                    
                    filneNameLen = strlen(outputTensorfilename);
                    filePathLen =  strlen(outputTensorFilePath);

                    DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[DATINJ] outputTensorfilename Len :%d\n",filneNameLen);
                    DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[DATINJ] outputTensorFilePath Len :%d\n",filePathLen);
                    
                    memset(outputTensorfullfilename,0,sizeof(outputTensorfullfilename));
                    if (filneNameLen + filePathLen < 1023 -1) {
                        snprintf(outputTensorfullfilename, 2048, "%s/%s",outputTensorFilePath,outputTensorfilename);
                    }
                    DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[DATINJ] outputTensorfullfilename  :%s\n",outputTensorfullfilename);
                    DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[DATINJ] outputTensorfullfilename Length :%d\n",strlen(outputTensorfullfilename) + 1);

                    if ((inputFileSize % 4 != 0) || (inputFileSize > DATAINJECTION_INPUT_TENSOR_MAX_DATA_SIZE)) {
                        free(wk_mem);
                        free(byte_swap_wk_mem);
                        continue;
                    }

                    pos = 0;
                    while ( pos  < inputFileSize  ) {
                        ((char *)byte_swap_wk_mem)[ 3 + pos] = ((char *)wk_mem)[ 0 + pos];
                        ((char *)byte_swap_wk_mem)[ 2 + pos] = ((char *)wk_mem)[ 1 + pos];
                        ((char *)byte_swap_wk_mem)[ 1 + pos] = ((char *)wk_mem)[ 2 + pos];
                        ((char *)byte_swap_wk_mem)[ 0 + pos] = ((char *)wk_mem)[ 3 + pos];
                        pos = pos + 4;
                    }
                    //PRINTF("[DATINJ] dataInjectionMain 2 \n");
                
                    ret_code = TransferInputTensor(handle,byte_swap_wk_mem,inputFileSize);
                    if (ret_code != E_OK) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] ERROR Transfer Input Tensor \n");
                        fclose(finlist);
                        imx500::control::DataInjection::InputTensorInput_Close(handle);
                        return ret_code;
                    }
#if 0
                    while(1) {
                        regval = 0;
                        imx500::i2c_access::I2cRead(REG_ADDR_DD_DAT_INJECTION_HNDSK,&regval,REG_SIZE_DD_DAT_INJECTION_HNDSK);
                        usleep(1000);
                        if ((savedOutputTesnor == 1) || (regval == 2)) {
                            temp_ret_code = imx500::i2c_access::I2cWrite(REG_ADDR_DD_DAT_INJECTION_HNDSK,REG_VAL_DD_DAT_INJECTION_HNDSK_TRANS_READY_WAIT,REG_SIZE_DD_DAT_INJECTION_HNDSK);
                            
                            if (temp_ret_code != E_OK) {
                                while (temp_ret_code != E_OK) {
                                    temp_ret_code = imx500::i2c_access::I2cWrite(REG_ADDR_DD_DAT_INJECTION_HNDSK,REG_VAL_DD_DAT_INJECTION_HNDSK_TRANS_READY_WAIT,REG_SIZE_DD_DAT_INJECTION_HNDSK);
                                    usleep(1000);
                                }
                            }
                            savedOutputTesnor = 0;
                            break;
                        }
                    }
                    
                    DEBUG_PRINTF("[DATINJ] Input Tensor Transfer  Ready .... \n");

                    while(1) {
                        regval = 0;
                        imx500::i2c_access::I2cRead(REG_ADDR_DD_DAT_INJECTION_HNDSK,&regval,REG_SIZE_DD_DAT_INJECTION_HNDSK);
                        usleep(1000);
                            if (regval == REG_VAL_DD_DAT_INJECTION_HNDSK_TRANS_READY_COMP) {
                            break;
                        }
                    }

                    DEBUG_PRINTF("[DATINJ] Input Tensor Transfer ... \n");

                    ret_code = imx500::control::DataInjection::InputTensorInput_Open(&handle);
                    if (ret_code != imx500::E_OK) {
                        PRINTF("[DATINJ] ERROR Driver Open. ret=0x%08x", ret_code);
                        return E_OTHER;
                    }

                    ret_code = imx500::control::DataInjection::InputTensorInput_TransferInputTensor(handle,(uint8_t *)byte_swap_wk_mem,fileSize);
                    if (ret_code != imx500::E_OK) {
                        PRINTF("[DATINJ] ERROR InputTensor Transfer Error . ret=0x%08x", ret_code);
                        return E_OTHER;
                    }

                    ret_code = imx500::control::DataInjection::InputTensorInput_Close(handle);
                    if (ret_code != imx500::E_OK) {
                        PRINTF("[DATINJ] ERROR Driver Close. ret=0x%08x", ret_code);
                        return E_OTHER;
                    }
                    
                    PRINTF("[DATINJ] Input Tensor Transfer End \n");
#endif
                    
                    while(1) {
                        if (inputTensordiffEnd == 1) {
                            if (inputTensorReSend == 1) {
                                inputTensorReSend = 0;
                                inputTensordiffEnd = 0;
                                ret_code = TransferInputTensor(handle,byte_swap_wk_mem,inputFileSize);
                                if (ret_code != E_OK) {
                                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] ERROR Transfer Input Tensor \n");
                                    fclose(finlist);
                                    imx500::control::DataInjection::InputTensorInput_Close(handle);
                                    return ret_code;
                                }
                            } 
                            else {
                    			free(byte_swap_wk_mem);
                                free(wk_mem);
                                wk_mem = NULL;
                                inputTensordiffEnd = 0;
                                inputTensorReSend = 0;
                                break;
                            }
                        }
                        usleep(1000);
                    }             

                
                    while(1) {
                        if (savedOutputTesnor == 1) {
                            imx500::i2c_access::I2cWrite(REG_ADDR_DD_DAT_INJECTION_HNDSK,REG_VAL_DD_DAT_INJECTION_HNDSK_TRANS_INIT_COMP,REG_SIZE_DD_DAT_INJECTION_HNDSK);
                            break;
                        }
                        usleep(1000);
                    }
                    DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[DATINJ]  Data Injection 1 Frame END  \n");
                    memset(inputTensorFileName,0,sizeof(inputTensorFileName));
                }
                //@ zanatei
                DBG_LOG_PRINT(DBG_LOG_LVL_INFO, "[DATINJ] !!! Data Injection Complete !!!  \n");
                imx500::i2c_access::I2cWrite(0x0100,0x00,0x01);
                fclose(finlist);
                imx500::control::DataInjection::InputTensorInput_Close(handle);
                return E_OK;
            }

        
            static E_RESULT_CODE TransferInputTensor(DataInjection_ModuleHandle pHandle, void *inputTensorMem, uint32_t inputTensrSize)
            {
                imx500::E_RESULT_CODE ret_code;
                imx500::E_RESULT_CODE temp_ret_code;
                uint32_t regval;

                while(1) {
                    regval = 0;
                    imx500::i2c_access::I2cRead(REG_ADDR_DD_DAT_INJECTION_HNDSK,&regval,REG_SIZE_DD_DAT_INJECTION_HNDSK);
                    usleep(1000);
                    if ((savedOutputTesnor == 1) || (regval == 2)) {
                        temp_ret_code = imx500::i2c_access::I2cWrite(REG_ADDR_DD_DAT_INJECTION_HNDSK,REG_VAL_DD_DAT_INJECTION_HNDSK_TRANS_READY_WAIT,REG_SIZE_DD_DAT_INJECTION_HNDSK);
                        
                        if (temp_ret_code != E_OK) {
                            while (temp_ret_code != E_OK) {
                                temp_ret_code = imx500::i2c_access::I2cWrite(REG_ADDR_DD_DAT_INJECTION_HNDSK,REG_VAL_DD_DAT_INJECTION_HNDSK_TRANS_READY_WAIT,REG_SIZE_DD_DAT_INJECTION_HNDSK);
                                usleep(1000);
                            }
                        }
                        savedOutputTesnor = 0;
                        break;
                    }
                }
                    
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[DATINJ] Input Tensor Transfer  Ready .... \n");

                while(1) {
                    regval = 0;
                    imx500::i2c_access::I2cRead(REG_ADDR_DD_DAT_INJECTION_HNDSK,&regval,REG_SIZE_DD_DAT_INJECTION_HNDSK);
                    usleep(1000);
                        if (regval == REG_VAL_DD_DAT_INJECTION_HNDSK_TRANS_READY_COMP) {
                        break;
                    }
                }

                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[DATINJ] Input Tensor Transfer ... \n");

                ret_code = imx500::control::DataInjection::InputTensorInput_TransferInputTensor(pHandle,(uint8_t *)inputTensorMem,inputTensrSize);
                if (ret_code != imx500::E_OK) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] ERROR InputTensor Transfer Error . ret=0x%08x", ret_code);
                    return E_OTHER;
                }

                
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[DATINJ] Input Tensor Transfer End \n");
                return E_OK;
            }
        
            /* for Data Injection */
            void saveOutputTensorinL2MemFormat(const uint8_t* src, const uint16_t lineSize, imx500::output_tensor_parser::FrameOutputInfo* frameOutputTensorInfo) {
                uint32_t regval = 0;

                DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[DATINJ] saveOutputTensorInL2Format savedOutputTensor= %d\n", savedOutputTesnor);
                if (savedOutputTesnor == 1) {
                    return ;
                }

                imx500::i2c_access::I2cRead(REG_ADDR_DD_DAT_INJECTION_HNDSK,&regval,REG_SIZE_DD_DAT_INJECTION_HNDSK);
                DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[DATINJ] saveOutputTensorInL2Format regVal= %d\n", regval);
                if (regval != REG_VAL_DD_DAT_INJECTION_HNDSK_TRANS_COMP) {
                    reciveCnt = 0;
                    DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[DATINJ] saveOutputTensorInL2Format returning regVal= %d\n", regval);
                    return ;
                }
                
                reciveCnt++;
                DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[DATINJ] reciveCnt %d\n", reciveCnt);

                if (reciveCnt < 5) {
                    return ;
                } else {
                    reciveCnt = 0;
                    DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[DATINJ] Output Tensor Save Mode \n");
                }
                /* Input Parameter Validation*/
                if (src == NULL) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] Error src address is NULL\n");
                    return;
                }
                if (frameOutputTensorInfo == NULL) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] Error Header info  is NULL\n");
                    return;
                }
                if (frameOutputTensorInfo->outputApParams == NULL) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] Error outApParams is NULL\n");
                    return;
                }
                uint32_t offset = 0;
                uint32_t total_size = 0;
                uint32_t dnnIndex = frameOutputTensorInfo->dnnHeaderInfo->networkId;        

                if (dnnIndex >= MAX_NUM_OF_NETWORKS) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] Error: Invalid dnnIndex %d\n", dnnIndex);
                    return;
                }

                for (uint32_t i = 0; i < networkInfoArray[dnnIndex].outputTensorNum; i++) {
                    if (total_size > (UINT32_MAX - networkInfoArray[dnnIndex].p_outputTensorSize[i])) {    /* CodeSonar check */
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] Error outputTensorSize overflow\n");
                        return;
                    }
                    total_size += networkInfoArray[dnnIndex].p_outputTensorSize[i];
                }
                /* CodeSonar check */
                if (total_size == 0) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] Error outputTensorSize is 0\n");
                    return;
                }

                uint8_t dst[total_size];

                for (int tensorIdx = 0; tensorIdx < frameOutputTensorInfo->outputApParams->size(); tensorIdx++) {

                    uint32_t outputTensorSize = 0;
                    uint32_t inSize = 1;

                    imx500::output_tensor_parser::OutputTensorApParams param = frameOutputTensorInfo->outputApParams->at(tensorIdx);
                    std::vector<uint16_t> serialized_dim(param.numOfDimensions);
                    std::vector<uint16_t> actual_dim(param.numOfDimensions);

                    for (int idx = 0; idx < param.numOfDimensions; idx++) {
                        actual_dim[idx] = param.vecDim.at(idx).size;
                        serialized_dim[param.vecDim.at(idx).serializationIndex] = param.vecDim.at(idx).size;
                        if (param.vecDim.at(idx).serializationIndex == 0) {
                            /* CodeSonar Check */
                            if (inSize >= (UINT32_MAX / ((param.vecDim.at(idx).size + param.vecDim.at(idx).padding) * (param.bitsPerElement / 8)))) {
                                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] Error inSize is greater than maximum size\n");
                                return;
                            }
                            inSize *= (param.vecDim.at(idx).size + param.vecDim.at(idx).padding)* (param.bitsPerElement / 8);
                        }
                        else {
                            /* CodeSonar Check */
                            if (inSize >= (UINT32_MAX / param.vecDim.at(idx).size)) {
                                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] Error inSize is greater than maximum size\n");
                                return;
                            }
                            inSize *= (param.vecDim.at(idx).size);
                        }
                    }
                    outputTensorSize = inSize;
                    if (outputTensorSize == 0) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] Invalid output tensorsize = %d, tensorIdx = %d\n", outputTensorSize, tensorIdx);
                        return;
                    }

                    uint16_t numOfLines = (uint16_t)ceil(inSize / (float)frameOutputTensorInfo->dnnHeaderInfo->maxLineLen);
                    uint8_t* out_tensor = (uint8_t*) new char[outputTensorSize];
                    memset(out_tensor,0,outputTensorSize);

                    /*Extract output tensor data*/
                    uint32_t elementIndex = 0;
                    for (int i = 0; i < numOfLines; i++) {
                        int lineIndex = 0;
                        while (lineIndex < frameOutputTensorInfo->dnnHeaderInfo->maxLineLen) {
                            /* CodeSonar check */
                            if (elementIndex >= outputTensorSize) {
                                break;
                            }
                            out_tensor[elementIndex] = (uint8_t)*(src + lineIndex);
                            elementIndex++;
                            lineIndex++;
                        }
                        src += lineSize;
                        if (elementIndex >= outputTensorSize) {
                            break;
                        }
                    }

                    /* CodeSonar check */
                    if (offset >= total_size) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] Error in Prasing output tensor offset(%d) >= output_size\n", offset);
                        delete[] out_tensor;
                        return;
                    }
                    memcpy(dst + offset, out_tensor, outputTensorSize);
                    delete[] out_tensor;
                    offset += outputTensorSize;
                }

                /* output file */
#if 0
                static int count = 1;
                FILE *file;
                char filename[64];
                sprintf(filename,"outputTensor_%05d.bin", count++);

                file = fopen(filename, "wb");
                printf("[DATINJ] Output Tensor flieName %s\n",filename);
                if (file)
                {
                    fwrite(dst, sizeof(char), total_size, file);
                    fclose(file);
                    savedOutputTesnor = 1;
                }
#endif
                FILE *file;
                file = fopen(outputTensorfullfilename, "wb");
                if (file)
                {
                    DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[DATINJ] Output Tensor flieName %s\n",outputTensorfullfilename);
                    fwrite(dst, sizeof(char), offset, file);
                    fclose(file);                    
                } 
                else {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] Failed Save Output Tensor flieName %s\n",outputTensorfullfilename);
                } 
                savedOutputTesnor = 1;
                return;
            }

            void cmpInputTensorinL2MemFormat(const uint8_t* src, uint32_t totalSize, uint16_t numOfLines, uint16_t lineSize, uint16_t channel, uint16_t maxLineLen) {
                uint32_t regval = 0;
                uint16_t linenum;
                uint16_t lineIndex = 0;
                uint32_t pixelIndex = 0;
                uint32_t array_size;

                if (savedOutputTesnor == 1) {
                    return ;
                }
#if 1
                imx500::i2c_access::I2cRead(REG_ADDR_DD_DAT_INJECTION_HNDSK,&regval,REG_SIZE_DD_DAT_INJECTION_HNDSK);
                if (regval != REG_VAL_DD_DAT_INJECTION_HNDSK_TRANS_COMP) {
                    return;
                }

                if (reciveCnt < 4) {
                    return;
                }
#endif
                /* Input Parameter Validation*/
                if (src == NULL) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] Error src address is NULL\n");
                    return;
                }
                //printf("[DATINJ] cmpInputTensorinL2MemFormat \n");
                /* calc array num */
                array_size = ((((totalSize / channel) + (maxLineLen - 1)) / maxLineLen) * maxLineLen) * channel;
                /* CodeSonar Check */
                if ((array_size / channel) != ((((totalSize / channel) + (maxLineLen - 1)) / maxLineLen) * maxLineLen)) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ] Error array_size  is greater than maximum size\n");
                    return;
                }
                uint8_t dst[array_size];
                memset(dst, 0, array_size);

                for (int i = 0; i < channel; i++) {
                    for (linenum = 0; linenum < numOfLines; linenum++) {
                        lineIndex = 0;
                        while (lineIndex < maxLineLen) {
                            /* CodeSonar Check */
                            if (pixelIndex >= array_size) {
                                break;
                            }
                            dst[pixelIndex] = *(src + lineIndex);
                            pixelIndex++;
                            lineIndex++;
                        }
                        /* CodeSonar Check */
                        if (pixelIndex >= array_size) {
                            break;
                        }
                        src += lineSize;
                    }
                    /* CodeSonar Check */
                    if (pixelIndex >= array_size) {
                        break;
                    }
                }

                
                if (wk_mem != NULL) {
                    if ( memcmp(dst,wk_mem,totalSize) != 0) {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[DATINJ]  !!! InputTensor Comp Error !!! \n");
                        inputTensorReSend = 1;
                    }
                    else {
                        inputTensorReSend = 0;
                    }
                    inputTensordiffEnd = 1;
                }
                else {
                    inputTensordiffEnd = 0;
                }
#if 0
                if (inputTensorReSend == 0) {
	                /* output file for debug */
	                FILE *file;
	                static uint32_t save_cnt_input = 1;
	                char inputTensorfilename[64];
	                sprintf(inputTensorfilename, "Test_inputTensor_Data_%04d.bin", save_cnt_input++);
	                file = fopen(inputTensorfilename, "wb");
	                if (file)
	                {
	                    PRINTF("[DATINJ] Input Tensor flieName %s\n",inputTensorfilename);
	                    fwrite(dst, sizeof(char), totalSize, file);
	                    fclose(file);
	                } 
	                else {
	                    PRINTF("[DATINJ] Failed Save Input Tensor flieName %s\n",inputTensorfilename);
	                } 
                }
#endif
                return;
            }

            static E_RESULT_CODE InputTensorInput_TransferSpi(int fd, uint8_t *pTransbuf,uint32_t transSize)
            {
                E_RESULT_CODE ret = E_OK;
                int32_t ioctrl_ret = 0;
                struct spi_ioc_transfer tr;

                memset(&tr, 0, sizeof(struct spi_ioc_transfer));
                tr.tx_buf = (unsigned long)pTransbuf;
                tr.rx_buf = (unsigned long)NULL;
                tr.len = transSize;
                tr.delay_usecs = 0;
                tr.speed_hz = DATAINJECTION_SPI_DRV_MAX_SPEED;
                tr.bits_per_word = DATAINJECTION_SPI_DRV_TRANS_BIT_WIDTH;
                tr.cs_change = DATAINJECTION_SPI_CS_CHANGE;

                ioctrl_ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
                if (ioctrl_ret < 1) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "InputTensorInput IOCTRL ERROR !!! \n");
                    ret = E_OTHER;
                }
                return ret;
            }

            static void GetDirNameAndFileName(char *path,char *dir,char *file, uint32_t dirBufSize, uint32_t fileBufSize)
            {
                const char *ptr = strrchr(path, '/');
                if (ptr == NULL) {        /* no '/' */
                    *dir = '\0';
                    strncpy(file, path,fileBufSize);
                }
                else if (ptr == path) {   /* head '/'  */
                    strncpy(dir, "/",dirBufSize);
                    strncpy(file, ptr + 1,fileBufSize);
                }
                else {                  /* found '/' */
                    memcpy(dir, path, ptr - path); 
                    dir[ptr - path] = '\0';
                    strncpy(file, ptr + 1,fileBufSize);
                }
            }
            static E_RESULT_CODE MakeOutPutFileName(char *sourcefileName, 
                                                    char *dstfileName, 
                                                    uint32_t dstBufSize)
            {
                const char *suffix = "_out";
                const char *ptr = strrchr(sourcefileName, '.');
                uint32_t srcStringLen;

                srcStringLen = strlen (sourcefileName);
                DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, " Source File Name Length ; %d byte \n", srcStringLen);
                if (ptr == NULL) {        /* '\.'  No extention */
                    if (dstBufSize  > (srcStringLen + 4 + 1)) {
                        strncpy(dstfileName, sourcefileName,dstBufSize);
                        strncat(dstfileName, suffix,dstBufSize);
                        DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, " dstfileName ; %s\n", dstfileName);
                    }
                    else {
                        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, " OutFileName Length Over Error" );
                        return E_OTHER;
                    }
                }
                else {                  /* '\.' found */
                    DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, ". Srings is number %d\n",ptr - sourcefileName);
                    memcpy(dstfileName, sourcefileName, ptr - sourcefileName); 
                    dstfileName[ptr - sourcefileName] = '\0';
                    strncat(dstfileName, suffix,dstBufSize);
                    strncat(dstfileName, ptr,dstBufSize);
                }
                return E_OK;
            }
        }
    }
}
