/*
* Copyright 2019-2020 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

#include "imx500lib_sample.h"
#include "regsetting.h"
#include "dbg_log.h"

#define REG_SETTING_FILE_COMMON             "regsetting_common.txt"
#define REG_SETTING_FILE_IU500              "regsetting_iu500.txt"
#define REG_SETTING_FILE_TYPE2_WIDE_ES1     "regsetting_type2_ACHIR0418B5M_es1.txt"
#define REG_SETTING_FILE_TYPE2_WIDE_ES2     "regsetting_type2_ACHIR0418B5M_es2.txt"
#define REG_SETTING_FILE_TYPE2_FISH_ES1     "regsetting_type2_AC118B029520IRMM_es1.txt"
#define REG_SETTING_FILE_TYPE2_FISH_ES2     "regsetting_type2_AC118B029520IRMM_es2.txt"
#define REG_SETTING_FILE_TYPE2_NORM_ES1     "regsetting_type2_AC12435MM_es1.txt"
#define REG_SETTING_FILE_TYPE2_NORM_ES2     "regsetting_type2_AC12435MM_es2.txt"
#define REG_SETTING_FILE_ES1                "regsetting_es1.txt"
#define REG_SETTING_FILE_ES2                "regsetting_es2.txt"
#define REG_SETTING_FILE_RAW_V2H2_MIPI1000  "regsetting_raw_v2h2_mipi1000.txt"
#define REG_SETTING_FILE_RAW_V2H2_MIPI2100  "regsetting_raw_v2h2_mipi2100.txt"
#define REG_SETTING_FILE_RAW_FULL_MIPI1000  "regsetting_raw_full_mipi1000.txt"
#define REG_SETTING_FILE_RAW_FULL_MIPI2100  "regsetting_raw_full_mipi2100.txt"

static uint32_t regsetting_string2decimal(std::string str);
static int regsetting_read_file(const char* file_path, SettingInfo** pp_reg_setting);
static int regsetting_search_data(SettingInfo* p_search, uint16_t search_addr, uint32_t* p_get_data);

static ReadRegSetting s_reg_setting;


/**
 * @brief regsetting_read
 */
int regsetting_read(int module_type, int lens_type, int es_ver, int full_size, int mipi_rate)
{
    int ret;
    const char* read_file_path;

    /* initialize  */
    memset(&s_reg_setting, 0, sizeof(s_reg_setting));
    s_reg_setting.state = READ_REG_SETTING_ST_INIT;

    /* common */
    ret = regsetting_read_file(REG_SETTING_FILE_COMMON, &s_reg_setting.mp_common);
    if (ret != 0) {
        return ret;
    }

    /* for each module type */
    if (module_type == E_MODULE_TYPE_KIND_IU500) {
        read_file_path = REG_SETTING_FILE_IU500;
    }
    else { /* module_type == E_MODULE_TYPE_KIND_TYPE2 */

        if (lens_type == E_LENS_TYPE_WIDEANGLE_ACHIR0418B5M) {

            if (es_ver == E_ES_VER_ES1) {
                read_file_path = REG_SETTING_FILE_TYPE2_WIDE_ES1;
            }
            else { /* es_ver == E_ES_VER_ES2 */
                read_file_path = REG_SETTING_FILE_TYPE2_WIDE_ES2;
            }
        }
        else if (lens_type == E_LENS_TYPE_FISHEYE_AC118B029520IRMM) {

            if (es_ver == E_ES_VER_ES1) {
                read_file_path = REG_SETTING_FILE_TYPE2_FISH_ES1;
            }
            else { /* es_ver == E_ES_VER_ES2 */
                read_file_path = REG_SETTING_FILE_TYPE2_FISH_ES2;
            }
        }
        else { /* lens_type == E_LENS_TYPE_NORMAL_AC12435MM or E_LENS_TYPE_CUSTOM */

            if (es_ver == E_ES_VER_ES1) {
                read_file_path = REG_SETTING_FILE_TYPE2_NORM_ES1;
            }
            else { /* es_ver == E_ES_VER_ES2 */
                read_file_path = REG_SETTING_FILE_TYPE2_NORM_ES2;
            }
        }
    }
    ret = regsetting_read_file(read_file_path, &s_reg_setting.mp_moduleType);
    if (ret != 0) {
        return ret;
    }

    /* for each ES version */
    if (es_ver == E_ES_VER_ES1) {
        read_file_path = REG_SETTING_FILE_ES1;
    }
    else { /* es_ver == E_ES_VER_ES2 */
        read_file_path = REG_SETTING_FILE_ES2;
    }
    ret = regsetting_read_file(read_file_path, &s_reg_setting.mp_esVer);
    if (ret != 0) {
        return ret;
    }

    /* for raw size */
    if (full_size == 0) {
        if (mipi_rate == 1000) {
            read_file_path = REG_SETTING_FILE_RAW_V2H2_MIPI1000;
        }
        else {
            read_file_path = REG_SETTING_FILE_RAW_V2H2_MIPI2100;
        }
    }
    else { /* full_size == 1 */
        if (mipi_rate == 1000) {
            read_file_path = REG_SETTING_FILE_RAW_FULL_MIPI1000;
        }
        else {
            read_file_path = REG_SETTING_FILE_RAW_FULL_MIPI2100;
        }
    }
    ret = regsetting_read_file(read_file_path, &s_reg_setting.mp_rawSize);
    if (ret != 0) {
        return ret;
    }

    s_reg_setting.state = READ_REG_SETTING_ST_DONE;

    return 0;
}

/**
 * @brief regsetting_get_data
 */
int regsetting_get_data(uint16_t req_addr, uint32_t* p_get_data)
{
    int ret;

    /* check state */
    if (s_reg_setting.state != READ_REG_SETTING_ST_DONE) {
        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[regsetting] ERROR: register setting file has not been read\n");
        return -1;
    }
    /* check param */
    if (p_get_data == NULL) {
        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[regsetting] ERROR: argument is NULL\n");
        return -1;
    }

    /* search data in common file */
    ret = regsetting_search_data(s_reg_setting.mp_common, req_addr, p_get_data);
    if (ret == 0) {
        return 0;
    }

    /* search data in module type file */
    ret = regsetting_search_data(s_reg_setting.mp_moduleType, req_addr, p_get_data);
    if (ret == 0) {
        return 0;
    }

    /* search data in es version file */
    ret = regsetting_search_data(s_reg_setting.mp_esVer, req_addr, p_get_data);
    if (ret == 0) {
        return 0;
    }

    /* search data in es raw size file */
    ret = regsetting_search_data(s_reg_setting.mp_rawSize, req_addr, p_get_data);
    if (ret == 0) {
        return 0;
    }

    DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[regsetting] ERROR: not found reqest data, address=0x%04X\n", req_addr);
    return -1;
}

/**
 * @brief regsetting_get_all_data
 */
int regsetting_get_all_data(ReadRegSetting** pp_get_all_data)
{
    /* check state */
    if (s_reg_setting.state != READ_REG_SETTING_ST_DONE) {
        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[regsetting] ERROR: register setting file has not been read\n");
        return -1;
    }
    /* check param */
    if (pp_get_all_data == NULL) {
        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[regsetting] ERROR: argument is NULL\n");
        return -1;
    }

    *pp_get_all_data = &s_reg_setting;
    return 0;
}


/**
 * @brief regsetting_string2decimal
 */
static uint32_t regsetting_string2decimal(std::string str)
{
    uint32_t dec = 0;
    uint8_t state = 0;
    uint32_t i = 0;

    while (i < str.size()) {
        switch (state) {
        case 0:
            if (str[i] == '0') {
                state = 1;
            }
            break;

        case 1:
            if (str[i] == 'x') {
                state = 2;
            }
            break;

        case 2:
            if ((str[i] >= '0') && (str[i] <= '9')) {
                dec <<= 4;
                dec += str[i] - 0x30;
            }
            else if ((str[i] >= 'A') && (str[i] <= 'F')) {
                dec <<= 4;
                dec += str[i] - 0x37;
            }
            else if ((str[i] >= 'a') && (str[i] <= 'f')) {
                dec <<= 4;
                dec += str[i] - 0x57;
            }
            else if ((str[i] == '\r') || (str[i] <= '\n')) {
                /* nop */
            }
            else {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[regsetting] ERROR: value format, value=%s\n", str.c_str());
                return 0;
            }
            break;

        default:
            break;

        }
        i++;
    }
    return dec;
}

/**
 * @brief regsetting_read_file
 */
static int regsetting_read_file(const char* file_path, SettingInfo** pp_reg_setting)
{
    SettingInfo* p_reg;
    std::string line;
    uint32_t line_num = 0;
    uint32_t count = 0;

    /* get line number */
    {
        std::ifstream ifs(file_path);
        if (ifs) {
            while (1) {
                getline(ifs, line);
                line_num++;

                if (ifs.eof()) {
                    break;
                }
            }
        }
        else {
            DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[regsetting] ERROR: can't open %s file\n", file_path);
            return -1;
        }
    }

    /* [Note]
       The number of line_num includes EOF lines */
    if (sizeof(SettingInfo) >= (UINT32_MAX / line_num)) {
        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[regsetting] ERROR: over size %s file, line_num=%d\n", file_path, line_num);
        return -1;
    }
    p_reg = (SettingInfo*)malloc(sizeof(SettingInfo) * line_num);
    if (p_reg == NULL) {
        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[regsetting] ERROR: can't malloc %s file, line_num=%d\n", file_path, line_num);
        return -1;
    }

    /* get reg info */
    std::ifstream ifs(file_path);
    while (getline(ifs, line)) {

        std::istringstream stream(line);
        std::string field;
        std::vector<std::string> result;
        while (getline(stream, field, ',')) {
            result.push_back(field);
        }

        p_reg[count].m_addr = regsetting_string2decimal(result.at(1));
        p_reg[count].m_size = stoi(result.at(2));
        p_reg[count].m_value = regsetting_string2decimal(result.at(3));
        count++;
    }

    /* set anchor data */
    p_reg[count].m_size = 0;

    *pp_reg_setting = p_reg;

    return 0;
}

/**
 * @brief regsetting_search_data
 */
static int regsetting_search_data(SettingInfo* p_search, uint16_t search_addr, uint32_t* p_get_data)
{
    while (p_search->m_size != 0) {

        if (p_search->m_addr == search_addr) {
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[regsetting] found reqest data, address=0x%04X, value=0x%08X\n", search_addr, p_search->m_value);
            *p_get_data = p_search->m_value;
            return 0;
        }
        p_search++;
    }
    return -1;
}
