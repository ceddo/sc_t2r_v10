/*
* Copyright 2018-2019 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/

#ifndef IMX500LIB_SAMPLE_H_
#define IMX500LIB_SAMPLE_H_

#include <stdint.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

int test();
int streaming(uint32_t image_width, uint32_t raw_data_line, uint8_t* p_pixcel_data);
int32_t image_file_read(FILE* fp, uint8_t* p_dst, uint32_t* p_size);


typedef struct {
	char* name;
	int value;
}option_table;

typedef struct  {
	uint16_t dnnHeaderSize;
	uint16_t inputTensorWidth;
	uint16_t inputTensorHeight;
	uint32_t inputTensorWidthStride;
	uint32_t inputTensorHeightStride;
	uint32_t inputTensorSize;
	uint8_t  outputTensorNum;
	uint32_t* p_outputTensorSize;
}nw_info;

typedef struct {
	int outputTensorCustom;
}output_file_rate;



enum option_table_index {
	E_RAW_FULLSIZE =0,
	E_BINNING_OPTION,
	E_MIPI_DATA_RATE,
	E_SAVE_RAWFILE,
	E_SAVE_RAWFILE_RATE,
	E_SAVE_RAWFILE_PATH,
	E_SAVE_BMPFILE_RATE,
	E_SAVE_PQFILE,
	E_SAVE_PQFILE_RATE,
	E_SAVE_PQFILE_PATH,
	E_IMAGE_ONLY,
	E_MODULE_TYPE,//enum option_module_kind
	E_ES_VER,//enum option_es_version
	E_LENS_TYPE,//enum option_lens_type
	E_TYPE2_GPIO_ADAPTER,
	E_DATA_INJECTION,
	E_DATA_INJECTION_OUTPUT_TENSOR_PATH,
	E_DATA_INJECTION_INPUT_TENSOR_LIST_NAME,
	E_DBG_LOG_LEVEL,
	E_OPTION_INDEX_MAX
};

enum option_network_kind {
	E_NET_KIND_SSDMOBILENET =0,
	E_NET_KIND_MOBILENETV1,
	E_NET_KIND_INPUTTENSOR_ONLY,
	E_NET_KIND_CUSTOM,
	E_NET_KIND_MAX
};

enum option_module_kind {
	E_MODULE_TYPE_KIND_IU500 =0,
	E_MODULE_TYPE_KIND_TYPE2,
	E_MODULE_TYPE_KIND_MAX
};

enum option_es_version {
	E_ES_VER_ES1 =0,
	E_ES_VER_ES2,
	E_ES_VER_MAX
};

enum option_lens_type {
	E_LENS_TYPE_NORMAL_AC12435MM =0,
	E_LENS_TYPE_WIDEANGLE_ACHIR0418B5M,
	E_LENS_TYPE_FISHEYE_AC118B029520IRMM,
	E_LENS_TYPE_CUSTOM,
	E_LENS_TYPE_MAX
};

enum option_binning_option {
	E_BINNING_OPT_BAYER =0,
	E_BINNING_OPT_MONO,
	E_BINNING_OPT_MAX
};

#define REC_CTRL_SET_VALUE_BIT_SAVE     (0x1)
#define REC_CTRL_SET_VALUE_BIT_VIEW     (0x2)


#ifdef __cplusplus
}
#endif


#endif
