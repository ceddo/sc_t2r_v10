/*
* Copyright 2019-2020 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/

#ifndef REGSETTING_H_
#define REGSETTING_H_

#include <stdint.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum tagE_READ_REG_SETTING_ST {
    READ_REG_SETTING_ST_INIT,
    READ_REG_SETTING_ST_DONE
} E_READ_REG_SETTING_ST;

typedef struct tagSettingInfo {
    uint16_t    m_addr;
    uint8_t     m_size;
    uint8_t     m_rsvd;
    uint32_t    m_value;
} SettingInfo;

typedef struct tagReadRegSetting {
    E_READ_REG_SETTING_ST   state;
    SettingInfo*            mp_common;
    SettingInfo*            mp_moduleType;
    SettingInfo*            mp_esVer;
    SettingInfo*            mp_rawSize;
} ReadRegSetting;


int regsetting_read(int module_type, int lens_type, int es_ver, int full_size, int mipi_rate);
int regsetting_get_data(uint16_t req_addr, uint32_t* p_get_data);
int regsetting_get_all_data(ReadRegSetting** pp_get_all_data);


#ifdef __cplusplus
}
#endif


#endif /* REGSETTING_H_ */
