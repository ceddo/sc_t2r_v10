/*
 * Copyright 2017-2018 Sony Semiconductor Solutions Corporation.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
 * Solutions Corporation.
 * No part of this file may be copied, modified, sold, and distributed in any
 * form or by any means without prior explicit permission in writing from
 * Sony Semiconductor Solutions Corporation.
 *
 */
#ifndef CLASSIFICATION_INTERFACE_H_
#define CLASSIFICATION_INTERFACE_H_

//#include <senscord/serialize.h>

namespace imx500 {
	namespace classification_interface {

		struct ClassificationOutputTensor {
			std::vector<float> scores;

			//SENSCORD_SERIALIZE_DEFINE(scores);
		};

	}
}
#endif  // CLASSIFICATION_INTERFACE_H_
