/*
* Copyright 2017-2018 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/
#ifndef DATA_SPLITTER_H_
#define DATA_SPLITTER_H_

#include <stdint.h>

/*
* Input Tensor Namespace
*/
namespace data_splitter {

typedef enum {
    SPLITTER_ARRAY_KPI = 0,
    SPLITTER_ARRAY_INPUTTENSOR,
    SPLITTER_ARRAY_OUTPUTTENSOR,
    SPLITTER_ARRAY_PQ_SETTING,
    SPLITTER_ARRAY_RAW,
    SPLITTER_ARRAY_MAX
} E_SPLITTER_ARRAY;


struct InputDataInfo {
	uint8_t* addr;
	uint32_t kpi_line;
	uint32_t input_tensor_line;
	uint32_t output_tensor_line;
	uint32_t pq_setting_line;
	uint32_t raw_data_line;
	size_t size;
};

struct OutputDataInfo {
	uint8_t* addr;
	size_t size;
	uint32_t line;
};

struct whcInfo {
	uint16_t width;
	uint16_t height;
	uint16_t color;
	size_t pitch;
};

	uint16_t data_split_10bit_packed(const struct InputDataInfo* in, struct OutputDataInfo* out[SPLITTER_ARRAY_MAX], struct whcInfo* whc);

	uint16_t data_split_8bit_packed(const struct InputDataInfo* in, struct OutputDataInfo* out[SPLITTER_ARRAY_MAX], struct whcInfo* whc);

	uint16_t data_split_10bit_unpacked(const struct InputDataInfo* in, struct OutputDataInfo* out[SPLITTER_ARRAY_MAX], struct whcInfo* whc);
}

#endif
