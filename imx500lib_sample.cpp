/*
* Copyright 2018-2019 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <fstream>
#include <sys/time.h>
#include <ctime>
#include <future>
#include <sys/stat.h>
#include <errno.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>

#include "common_define.h"
#include "return_code.h"
#include "sensor_control.h"
#include "imx500lib_sample.h"
#include "data_splitter.h"
#include "input_tensor_parser.h"
#include "output_tensor_parser.h"
#include "classification_analyser.h"
#include "classification_interface.h"
#include "objectdetection_analyser.h"
#include "objectdetection_interface.h"
#include "color_convert.h"
#include "i2c_access.h"
#include "data_injection.h"
#include "regsetting.h"
#include "sensor_property.h"
#include "dbg_log.h"
#include "pq_setting_parser.h"
#include "imx500_hooks.h"
#include "imx500_hooks_config.h"

/////////////////////////////////////////////////////////////////////
// Test Configure
/////////////////////////////////////////////////////////////////////
#define IMAGE_BIT_DEPTH       (10)
#define REGISTER_ACCESS_TEST  (0)
#define DNN_RUNTIME_OFFSET    9
#define DSP_RUNTIME_OFFSET    10
#define MAX_DETECT             5

#define IMAGE_FILE_DWP_CP_ACHIR0418B5M      "image/DWP/ACHIR0418B5M/cpoint_param_wide_spec_adj_f3_5.fpk"
#define IMAGE_FILE_DWP_CP_AC118B029520IRMM  "image/DWP/AC118B029520IRMM/cpoint_param_fish_spec_f2_95.fpk"
#define IMAGE_FILE_DWP_CP_CUSTOM            "image/DWP/custom/dwp_ctrl_pt.fpk"


typedef enum tagE_IMX500_STATE {
    E_IMX500_STATE_INIT,
    E_IMX500_STATE_STANDBY,
    E_IMX500_STATE_STREAM
} E_IMX500_STATE;


#define IMAGE_FILE_SIZE_MAX_LOADER      (51200)     // 50*1024 = 50KB
#define IMAGE_FILE_SIZE_MAX_MAINFW      (1572864)   // 1.5*1024*1024 = 1.5MB
#define IMAGE_FILE_SIZE_MAX_NETWK       (7340032)   // 7*1024*1024= 7MB
#define IMAGE_FILE_SIZE_MAX_DWPCP       (68608)     // 67*1024 = 67KB
#define CHK_FILE_SIZE(size, max)        (((size) % 4) == 0 ? ((size) <= (max) ? true : false) : false)

/* config default values */
const uint32_t kDefaultId          = 0;
const uint32_t kDefaultBufferNum   = 3;
const uint32_t kDefaultErrorLog    = 0;
const uint32_t kDefaultInfoLog     = 0;
const uint32_t kDefaultDebugLog    = 0;
const uint32_t kDefaultFlashAddrLoader = 0x00000000;
const uint32_t kDefaultFlashAddrMainFw = 0x00020000;
const uint32_t kDefaultFlashAddrNetWk  = 0x00100000;

//const uint32_t kDefaultFlashAddrLoader = 0x00420000;
//const uint32_t kDefaultFlashAddrMainFw = 0x00440000;
//const uint32_t kDefaultFlashAddrNetWk  = 0x00000000;

static void cleanup_image_data(imx500::control::BootParam* p_boot_param);
static void copy_byteswap_buffer(uint8_t* data, uint32_t size);
int createDirIfNotExist(char *path);

static E_IMX500_STATE s_imx500_state = E_IMX500_STATE_INIT;
static uint32_t s_kpi_line;
static uint32_t s_input_tensor_line;
static uint32_t s_output_tensor_line;
static uint32_t s_pq_setting_line;
static uint32_t s_lev_gain;

static imx500::input_tensor_parser::InputNormInfo inputNormInfo ={};
static imx500::input_tensor_parser::FrameInputTensorInfo frameInputTensorInfo;
static uint8_t* input_tensor_memory = NULL;


static uint32_t fil_count = 0;      // save file count


extern option_table table[];
extern output_file_rate outputFileRate;
extern char inTensorSavePath[1023];
extern char pqSavePath[1023];

std::string getDisplayImageExtension() {
    std::string ret = "";
    switch (IMX500Hooks::getConfig_displayImageExtension()) {
        case IMX500Hooks::E_JPEG: ret = ".jpg"; break;
        case IMX500Hooks::E_PNG: ret = ".png"; break;
        case IMX500Hooks::E_BMP: ret = ".bmp"; break;
    }
    return ret;
}
void callback_onFrame(struct IMX500Hooks::ImageData euImageData) {
    if (IMX500Hooks::Hooks::getInstance() != nullptr) {
        IMX500Hooks::Hooks::getInstance()->onFrame(euImageData);
    }
}

/**
 * @brief main
 */
int test() {
    FILE* image_fp = NULL;
    DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "\n\n=== IMX500 Sample application Start ===\n");

    //===========================================
    // init
    int ret;
    imx500::E_RESULT_CODE ret_code;

    imx500::control::init(NULL);
    DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "[APP] IMX500Lib init done\n");

    char gfp[2048] = {0};
    std::ifstream file;

    //===========================================
    // open
    imx500::control::BootParam boot_param;
    memset(&boot_param, 0, sizeof(imx500::control::BootParam));
    boot_param.m_flashAddrLoader = kDefaultFlashAddrLoader;
    boot_param.m_flashAddrMainFw = kDefaultFlashAddrMainFw;
    boot_param.m_flashAddrNetWk = kDefaultFlashAddrNetWk;
    boot_param.m_imgOnlyEna = table[E_IMAGE_ONLY].value;
    boot_param.m_bootMode = HooksConfig.bDoFlashUpdate;
    boot_param.m_flashUpdateEna = HooksConfig.bDoFlashUpdate;
    boot_param.m_mipiDataRate = table[E_MIPI_DATA_RATE].value;
    uint32_t exe_freq;
    ret = regsetting_get_data(REG_ADDR_EXCK_FREQ_C01, &exe_freq);
    if (ret != 0) {
        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] Could not get the INCK frequency.\n");
        return -1;
    }
    boot_param.m_inckFreq = exe_freq;
    uint32_t flash_type;
    ret = regsetting_get_data(REG_ADDR_FLASH_TYPE, &flash_type);
    if (ret != 0) {
        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] Could not get the Flash Type.\n");
        return -1;
    }
    boot_param.m_flashType = (uint8_t)flash_type;

    if (HooksConfig.bDoFlashUpdate == 1) {
        int32_t img_read_ret;

        /* Loader */
        void* wk_mem = malloc(IMAGE_FILE_SIZE_MAX_LOADER);
        if (wk_mem == NULL) {
            DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] Malloc error for Loader image file memcopy.\n");
            return -1;
        }
        boot_param.m_pImageDataLoader = (uint8_t*)wk_mem;
        std::string fileLocation = "image/Firmware/loader.fpk";
        image_fp = fopen(fileLocation.c_str(),"rb");
        if (image_fp != NULL) {
            img_read_ret = image_file_read(image_fp, boot_param.m_pImageDataLoader, &boot_param.m_imageSizeLoader);
            fclose(image_fp);
        } else {
            DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] Cannot open loader.fpk\n");
            img_read_ret = -1; 
        }

        if (img_read_ret != 0) {
            DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] image_file_read returned error for loader.fpk\n");
            cleanup_image_data(&boot_param);
            return -1;
        }

        if (CHK_FILE_SIZE(boot_param.m_imageSizeLoader, IMAGE_FILE_SIZE_MAX_LOADER) != true) {
            DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] Loader image file size error. size = %d\n", boot_param.m_imageSizeLoader);
            cleanup_image_data(&boot_param);
            return -1;
        }

        copy_byteswap_buffer(boot_param.m_pImageDataLoader, boot_param.m_imageSizeLoader);

        /* Main FW */
        wk_mem = malloc(IMAGE_FILE_SIZE_MAX_MAINFW);
        if (wk_mem == NULL) {
            DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] Malloc error for Main FW image file memcopy.\n");
            cleanup_image_data(&boot_param);
            return -1;
        }
        boot_param.m_pImageDataMainFw = (uint8_t*)wk_mem;

        fileLocation = "image/Firmware/firmware.fpk";
        image_fp = fopen(fileLocation.c_str(),"rb");
        if (image_fp != NULL) {
            img_read_ret = image_file_read(image_fp, boot_param.m_pImageDataMainFw, &boot_param.m_imageSizeMainFw);
            fclose(image_fp);
        } else {
            DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] Cannot open firmware.fpk\n");
            img_read_ret = -1;
        }

        if (img_read_ret != 0) {
            DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] image_file_read returned error for firmware.fpk\n");
            cleanup_image_data(&boot_param);
            return -1;
        }

        if (CHK_FILE_SIZE(boot_param.m_imageSizeMainFw, IMAGE_FILE_SIZE_MAX_MAINFW) != true) {
            DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] MainFW image file size error. size = %d\n", boot_param.m_imageSizeMainFw);
            cleanup_image_data(&boot_param);
            return -1;
        }

        copy_byteswap_buffer(boot_param.m_pImageDataMainFw, boot_param.m_imageSizeMainFw);

        /* Network */
        wk_mem = malloc(IMAGE_FILE_SIZE_MAX_NETWK);
        if (wk_mem == NULL) {
            DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] Malloc error for Network image file memcopy.\n");
            cleanup_image_data(&boot_param);
            return -1;
        }
        boot_param.m_pImageDataNetWk = (uint8_t*)wk_mem;
        memset(gfp, 0, 2048);
        snprintf(gfp, sizeof(gfp), "image/%s/network.fpk", HooksConfig.computerVisionModelsFolder);
        image_fp = fopen(gfp, "rb");
        if (image_fp != NULL) {
            img_read_ret = image_file_read(image_fp, boot_param.m_pImageDataNetWk, &boot_param.m_imageSizeNetWk);
            fclose(image_fp);
        } else {
            cleanup_image_data(&boot_param);
            return -1;
        }

        if (CHK_FILE_SIZE(boot_param.m_imageSizeNetWk, IMAGE_FILE_SIZE_MAX_NETWK) != true) {
            DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] Network image file size error. size = %d\n", boot_param.m_imageSizeNetWk);
            cleanup_image_data(&boot_param);
            return -1;
        }

        copy_byteswap_buffer(boot_param.m_pImageDataNetWk, boot_param.m_imageSizeNetWk);
        
        DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[APP] boot_param.m_imageSizeLoader :%d\n", boot_param.m_imageSizeLoader);
        DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[APP] boot_param.m_imageSizeMainFw :%d\n", boot_param.m_imageSizeMainFw);
        DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[APP] boot_param.m_imageSizeNetWk :%d\n", boot_param.m_imageSizeNetWk);
    }

    /* Dewarp Contro Point */
    if ((table[E_MODULE_TYPE].value == E_MODULE_TYPE_KIND_TYPE2) &&
        ((table[E_LENS_TYPE].value == E_LENS_TYPE_WIDEANGLE_ACHIR0418B5M)   ||
         (table[E_LENS_TYPE].value == E_LENS_TYPE_FISHEYE_AC118B029520IRMM) ||
         (table[E_LENS_TYPE].value == E_LENS_TYPE_CUSTOM))) {

        int32_t img_read_ret_dwp;
        const char* file_name;
        void* wk_mem_dwp = malloc(IMAGE_FILE_SIZE_MAX_DWPCP);
        if (wk_mem_dwp == NULL) {
            DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] Malloc error for DWP CP image file memcopy.\n");
            cleanup_image_data(&boot_param);
            return -1;
        }
        boot_param.m_pImageDataDwpCp = (uint8_t*)wk_mem_dwp;

        if (table[E_LENS_TYPE].value == E_LENS_TYPE_WIDEANGLE_ACHIR0418B5M) {
            file_name = IMAGE_FILE_DWP_CP_ACHIR0418B5M;
        }
        else if (table[E_LENS_TYPE].value == E_LENS_TYPE_FISHEYE_AC118B029520IRMM) {
            file_name = IMAGE_FILE_DWP_CP_AC118B029520IRMM;
        }
        else { /* table[E_LENS_TYPE].value == E_LENS_TYPE_CUSTOM */
            file_name = IMAGE_FILE_DWP_CP_CUSTOM;
        }

        image_fp = fopen(file_name,"rb");
        if (image_fp != NULL) {
            img_read_ret_dwp = image_file_read(image_fp, boot_param.m_pImageDataDwpCp, &boot_param.m_imageSizeDwpCp);
            fclose(image_fp);
        } else {
            img_read_ret_dwp = -1;
        }

        if (img_read_ret_dwp != 0) {
            /* Continue even if file reading fails */
            free(boot_param.m_pImageDataDwpCp);
            boot_param.m_pImageDataDwpCp = NULL;
            boot_param.m_imageSizeDwpCp = 0;
        }
        else {

            if (CHK_FILE_SIZE(boot_param.m_imageSizeDwpCp, IMAGE_FILE_SIZE_MAX_DWPCP) != true) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] DWP CP file size error. size = %d\n", boot_param.m_imageSizeDwpCp);
                cleanup_image_data(&boot_param);
                return -1;
            }

            copy_byteswap_buffer(boot_param.m_pImageDataDwpCp, boot_param.m_imageSizeDwpCp);
        }
    }
    else { /* (table[E_MODULE_TYPE].value == E_MODULE_TYPE_KIND_IU500) || */
           /* ((table[E_MODULE_TYPE].value == E_MODULE_TYPE_KIND_TYPE2) && (table[E_LENS_TYPE].value == E_LENS_TYPE_NORMAL_AC12435MM)) */
        boot_param.m_pImageDataDwpCp = NULL;
        boot_param.m_imageSizeDwpCp = 0;
    }

    ret_code = imx500::control::open(&boot_param);
    cleanup_image_data(&boot_param);

    if (ret_code != imx500::E_OK) {
        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] Couldn't IMX500LIB open. ret=0x%08x\n", ret_code);
        return -1;
    }
    s_imx500_state = E_IMX500_STATE_STANDBY;
    DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "[APP] IMX500Lib open done\n");

    if(HooksConfig.bDoFlashUpdate == 1) {
        DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "[APP] Flash Update Complete\n");
        DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "================= End =================\n");
        return 0;
    }
    
    //===========================================
    // start

    /* register setting */
    ReadRegSetting* p_reg_setting;
    ret = regsetting_get_all_data(&p_reg_setting);
    if (ret != 0) {
        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] Could not get regsetting.\n");
        return -1;
    }
    imx500::property::set_reg_value((imx500::property::RegSetting*)p_reg_setting->mp_common);
    imx500::property::set_reg_value((imx500::property::RegSetting*)p_reg_setting->mp_moduleType);
    imx500::property::set_reg_value((imx500::property::RegSetting*)p_reg_setting->mp_esVer);
    imx500::property::set_reg_value((imx500::property::RegSetting*)p_reg_setting->mp_rawSize);
    imx500::property::set_init_param(table[E_RAW_FULLSIZE].value, HooksConfig.frameRate, table[E_BINNING_OPTION].value);

    if (table[E_DATA_INJECTION].value == 1) {
        if ((table[E_MODULE_TYPE].value == E_MODULE_TYPE_KIND_IU500) || 
            ((table[E_MODULE_TYPE].value == E_MODULE_TYPE_KIND_TYPE2) &&
            (table[E_TYPE2_GPIO_ADAPTER].value == 1))) {
            imx500::i2c_access::I2cWrite(REG_ADDR_DD_DAT_INJECTION_HNDSK,0x01,REG_SIZE_DD_DAT_INJECTION_HNDSK);
            imx500::i2c_access::I2cWrite(REG_ADDR_LEV_IMAGE_DATA_NOT_OVERWRITE,0x01,REG_SIZE_LEV_IMAGE_DATA_NOT_OVERWRITE);
        }
    }
    ret_code = imx500::control::start();
    if (ret_code != imx500::E_OK) {
        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] Couldn't IMX500LIB start. ret=0x%08x", ret_code);
        return -1;
    }

    /* get register value */
    DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[APP] get register value\n");
    imx500::i2c_access::I2cRead(REG_ADDR_DD_CH06_Y_OUT_SIZE, &s_kpi_line, REG_SIZE_DD_CH06_Y_OUT_SIZE);
    DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[APP] $ DD_CH06_Y_OUT_SIZE         = 0x%04X\n", s_kpi_line);
    imx500::i2c_access::I2cRead(REG_ADDR_DD_CH07_Y_OUT_SIZE, &s_input_tensor_line, REG_SIZE_DD_CH07_Y_OUT_SIZE);
    DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[APP] $ DD_CH07_Y_OUT_SIZE         = 0x%04X\n", s_input_tensor_line);
    imx500::i2c_access::I2cRead(REG_ADDR_DD_CH08_Y_OUT_SIZE, &s_output_tensor_line, REG_SIZE_DD_CH08_Y_OUT_SIZE);
    DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[APP] $ DD_CH08_Y_OUT_SIZE         = 0x%04X\n", s_output_tensor_line);
    imx500::i2c_access::I2cRead(REG_ADDR_DD_CH09_Y_OUT_SIZE, &s_pq_setting_line, REG_SIZE_DD_CH09_Y_OUT_SIZE);
    DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[APP] $ DD_CH09_Y_OUT_SIZE         = 0x%04X\n", s_pq_setting_line);

    for (int i = 0; i < MAX_NUM_OF_NETWORKS; i++) {
        if (table[E_BINNING_OPTION].value != E_BINNING_OPT_MONO) {
            for (int j = 0; j < 3; j++) {
                imx500::i2c_access::I2cRead(((REG_ADDR_DNN_INPUT_NORM + (i * REG_OFST_DNN_INPUT_NORM)) + (j * REG_OFST_DNN_INPUT_NORM_CH)), &inputNormInfo.norm_val[i][j], REG_SIZE_DNN_INPUT_NORM);
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[APP] $ DNN%d_INPUT_NORM_CH%02d       = 0x%04X\n", i, j, inputNormInfo.norm_val[i][j]);
                imx500::i2c_access::I2cRead(((REG_ADDR_DNN_INPUT_NORM_SHIFT + (i * REG_OFST_DNN_INPUT_NORM)) + (j * REG_OFST_DNN_INPUT_NORM_CH)), &inputNormInfo.norm_shift[i][j], REG_SIZE_DNN_INPUT_NORM_SHIFT);
                DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[APP] $ DNN%d_INPUT_NORM_SHIFT_CH%02d = 0x%02X\n", i, j, inputNormInfo.norm_shift[i][j]);
            }
        }
        else {
            imx500::i2c_access::I2cRead(REG_ADDR_LEV_PL_NORM_YM_YADD, &inputNormInfo.norm_val[i][0], REG_SIZE_LEV_PL_NORM_YM_YADD);
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[APP] $ DNN%d_INPUT_NORM_YCH        = 0x%04X\n", i, inputNormInfo.norm_val[i][0]);
            imx500::i2c_access::I2cRead(REG_ADDR_LEV_PL_NORM_YM_YSFT, &inputNormInfo.norm_shift[i][0], REG_SIZE_LEV_PL_NORM_YM_YSFT);
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[APP] $ DNN%d_INPUT_NORM_SHIFT_YCH  = 0x%02X\n", i, inputNormInfo.norm_shift[i][0]);
        }

        imx500::i2c_access::I2cRead(REG_ADDR_DNN_INPUT_FORMAT_BASE + i, &inputNormInfo.input_format[i], REG_SIZE_DNN_INPUT_FORMAT);
        DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[APP] $ DNN%d_INPUT_FORMAT          = 0x%02X\n", i, inputNormInfo.input_format[i]);
        if ((inputNormInfo.input_format[i] != DNN_INPUT_FORMAT_RGB) && (inputNormInfo.input_format[i] != DNN_INPUT_FORMAT_BGR)) {
            DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] $ DNN%d_INPUT_FORMAT          = 0x%02X not supported\n", i, inputNormInfo.input_format[i]);
            return -1;
        }
    }

    uint32_t tmp_lev_gain = 0;
    imx500::i2c_access::I2cRead(REG_ADDR_LEV_PL_GAIN_VALUE, &tmp_lev_gain, REG_SIZE_LEV_PL_GAIN_VALUE);
    if (table[E_BINNING_OPTION].value == E_BINNING_OPT_MONO) {
        if (table[E_ES_VER].value == E_ES_VER_ES1) {
            /* In the case of ES1, correct gain value /= 2 */
            s_lev_gain = tmp_lev_gain / 2;
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[APP] $ LEV_PL_GAIN_VALUE          = 0x%02X -> 0x%02X (*)Correction by ES1\n", tmp_lev_gain, s_lev_gain);
        }
        else {
            s_lev_gain = tmp_lev_gain;
            DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[APP] $ LEV_PL_GAIN_VALUE          = 0x%02X\n", s_lev_gain);
        }
        if (s_lev_gain == 0) {
            DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] $ LEV_PL_GAIN_VALUE          = 0x%02X setting prohibited\n", s_lev_gain);
            return -1;
        }
    }
    else {
        /* In the case of RGB, gain is not corrected, so set 0x20(1.000) */
        s_lev_gain = 0x20;
    }

    s_imx500_state = E_IMX500_STATE_STREAM;
    DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "[APP] IMX500Lib start done\n");

    if (table[E_DATA_INJECTION].value == 1) {
        if ((table[E_MODULE_TYPE].value == E_MODULE_TYPE_KIND_IU500) || 
           ((table[E_MODULE_TYPE].value == E_MODULE_TYPE_KIND_TYPE2) &&
            (table[E_TYPE2_GPIO_ADAPTER].value == 1))) {
           imx500::control::DataInjection::DataInjectionMain();
        }
    }

    while(1){}


    //===========================================
    // stop
    ret_code = imx500::control::stop();
    if (ret_code != imx500::E_OK) {
        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] Couldn't IMX500LIB stop. ret=0x%08x", ret_code);
        return -1;
    }

    s_imx500_state = E_IMX500_STATE_STANDBY;
    DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "[APP] IMX500Lib stop done\n");


    //===========================================
    // close
    ret_code = imx500::control::close();
    if (ret_code != imx500::E_OK) {
        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] Couldn't IMX500LIB close. ret=0x%08x", ret_code);
        return -1;
    }

    s_imx500_state = E_IMX500_STATE_INIT;
    DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "[APP] IMX500Lib close done\n");
    DBG_LOG_PRINT(DBG_LOG_LVL_MAX, "================= End =================\n");

    return 0;
}
//end

int streaming(uint32_t image_width, uint32_t raw_data_line, uint8_t* p_pixcel_data)
{
    if(table[E_IMAGE_ONLY].value == 1) {
        return 1;
    }
    if (s_imx500_state != E_IMX500_STATE_STREAM) {
        return 1;
    }

    DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "\n[APP] FRAME START !!\n");

    //TODO MMAL can't be sized
    //imx500::i2c_access::I2cRead(REG_ADDR_Y_OUT_SIZE,&raw_data_line,REG_SIZE_Y_OUT_SIZE);

    //===========================================
    // data splitter
    //printf("Streaming ! %p %d\n", p_pixcel_data, pixcel_data_size);
    const clock_t begintime = clock();
    static float dnnRuntime = 0;
    static float dspRuntime = 0;
    int ret;
    data_splitter::InputDataInfo  inputInfo1;
    data_splitter::OutputDataInfo outputInfo1[data_splitter::SPLITTER_ARRAY_MAX];
    data_splitter::whcInfo        whc1;

    uint32_t image_height = raw_data_line + s_kpi_line + s_input_tensor_line + s_output_tensor_line + s_pq_setting_line;
    inputInfo1.addr = p_pixcel_data;
    inputInfo1.raw_data_line = raw_data_line;
    inputInfo1.kpi_line = s_kpi_line;
    inputInfo1.input_tensor_line = s_input_tensor_line;
    inputInfo1.output_tensor_line = s_output_tensor_line;
    inputInfo1.pq_setting_line = s_pq_setting_line;
    inputInfo1.size = image_height * image_width;

    whc1.width = image_width;
    whc1.height = image_height;
    whc1.pitch = image_width;
    DBG_LOG_PRINT(DBG_LOG_LVL_DETAIL, "[APP] width=%d pitch = %d\n", whc1.width, whc1.pitch);
    data_splitter::OutputDataInfo* outInfo[data_splitter::SPLITTER_ARRAY_MAX];
    outInfo[data_splitter::SPLITTER_ARRAY_KPI] = &outputInfo1[data_splitter::SPLITTER_ARRAY_KPI];
    outInfo[data_splitter::SPLITTER_ARRAY_INPUTTENSOR] = &outputInfo1[data_splitter::SPLITTER_ARRAY_INPUTTENSOR];
    outInfo[data_splitter::SPLITTER_ARRAY_OUTPUTTENSOR] = &outputInfo1[data_splitter::SPLITTER_ARRAY_OUTPUTTENSOR];
    outInfo[data_splitter::SPLITTER_ARRAY_PQ_SETTING] = &outputInfo1[data_splitter::SPLITTER_ARRAY_PQ_SETTING];
    outInfo[data_splitter::SPLITTER_ARRAY_RAW] = &outputInfo1[data_splitter::SPLITTER_ARRAY_RAW];

    ret = data_splitter::data_split_8bit_packed(&inputInfo1, outInfo, &whc1);
    if (ret != 0) {
        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] Error data splitter\n");
        return -1;
    }
    DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[APP] data split out\n");


    //===========================================
    // KPI Parser */
    if (s_kpi_line == 1) {
       uint8_t* kpi_start = (uint8_t*)outInfo[data_splitter::SPLITTER_ARRAY_KPI]->addr;
       dnnRuntime = (float) ((uint32_t)(kpi_start[4* DNN_RUNTIME_OFFSET + 3]<< 24 | kpi_start[4*DNN_RUNTIME_OFFSET + 2] << 16 | kpi_start[4*DNN_RUNTIME_OFFSET + 1] << 8 | kpi_start[4*DNN_RUNTIME_OFFSET]))/(1000.0f);
       dspRuntime = (float) ((uint32_t)(kpi_start[4* DSP_RUNTIME_OFFSET + 3]<< 24 | kpi_start[4*DSP_RUNTIME_OFFSET + 2] << 16 | kpi_start[4*DSP_RUNTIME_OFFSET + 1] << 8 | kpi_start[4*DSP_RUNTIME_OFFSET]))/(1000.0f);
       //printf("3:%x 2:%x 1:%x 0:%x\n",kpi_start[4* DSP_RUNTIME_OFFSET + 3], kpi_start[4*DSP_RUNTIME_OFFSET + 2], kpi_start[4*DSP_RUNTIME_OFFSET + 1], kpi_start[4*DSP_RUNTIME_OFFSET]);

       DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[APP] DNN runtime:%f ,DSP_Runtime:%f\n",dnnRuntime,dspRuntime);
    }
    else if (s_kpi_line > 1) {
        DBG_LOG_PRINT(DBG_LOG_LVL_WARNING, "[APP] Not support KPI line=%d\n", s_kpi_line);
    }
    else { /* s_kpi_line == 0 */
        /* do nothing */
    }


    /*================*/
    if (outInfo[data_splitter::SPLITTER_ARRAY_INPUTTENSOR]->size == 0) {
        return 0;
    }

    //===========================================
    // input tensor parser
    void* input_tensor_parse_mem;
    imx500::input_tensor_parser::InputDataInfo inputInfo2;
    imx500::input_tensor_parser::OutputDataInfo outputInfo2;
    imx500::input_tensor_parser::FrameInputTensorInfo InputTensorParseInfo = {0};

    // allocate the new memory.
    if((outInfo[data_splitter::SPLITTER_ARRAY_INPUTTENSOR]->size / inputInfo1.input_tensor_line) !=
        outInfo[data_splitter::SPLITTER_ARRAY_INPUTTENSOR]->line) { //CodeSonarCheck
        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] Error overflow input tensor malloc size)\n");
        return -1;
    }

    input_tensor_parse_mem = malloc(outInfo[data_splitter::SPLITTER_ARRAY_INPUTTENSOR]->size);
    if (input_tensor_parse_mem == NULL) {
        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] Malloc error(input tensor memory)\n");
        //streaming_stop(instance);
        return 0;
    }

    inputInfo2.addr = outInfo[data_splitter::SPLITTER_ARRAY_INPUTTENSOR]->addr;
    inputInfo2.size = outInfo[data_splitter::SPLITTER_ARRAY_INPUTTENSOR]->size;
    inputInfo2.pitch = whc1.pitch;
    inputInfo2.gain = s_lev_gain;
    if (table[E_BINNING_OPTION].value == E_BINNING_OPT_MONO) {
        inputInfo2.ych_en = 1;
    }
    else {
        inputInfo2.ych_en = 0;
    }

    outputInfo2.addr = (uint8_t*)input_tensor_parse_mem;

    ret = imx500::input_tensor_parser::parseInputTensor(&inputInfo2, &outputInfo2, (const imx500::input_tensor_parser::InputNormInfo *)&inputNormInfo, &InputTensorParseInfo);
    
    /* -------------------------------------------------------------------------------- */
    /* [NOTE]                                                                           */
    /*  Hold input tensor memory in consideration of the following frame straddling.    */
    /*                        +----------+----------+----------+----------+             */
    /*                        |    V1    |    V2    |    V3    |    V4    |             */
    /*          +-------------+----------+----------+----------+----------+             */
    /*          |InputTensor  | Valid(*1)| Invalid  | Valid(*2)|  Inalid  |             */
    /*          +-------------+----------+----------+----------+----------+             */
    /*          |OutputTensor | Invalid  |  Valid   | Invalid  |  Valid   |             */
    /*          +-------------+----------+----------+----------+----------+             */
    /*          (*1) Need to hold up to V2 for use in V2 result output                  */
    /*          (*2) Need to hold up to V4 for use in V4 result output                  */
    /* -------------------------------------------------------------------------------- */
    if (ret != 0) {
        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] Error parsing Input Tensor ret=%d\n", ret);
        free(input_tensor_parse_mem);
        if (input_tensor_memory == NULL) {
            return -1;
        }
    }
    else {
        if (input_tensor_memory != NULL) {
            free(input_tensor_memory);
        }
        input_tensor_memory = (uint8_t*)input_tensor_parse_mem;
        memcpy(&frameInputTensorInfo, &InputTensorParseInfo, sizeof(frameInputTensorInfo));
        DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[APP] input_tensor_parser out\n");
    }


    //===========================================
    // output tensor parser
    imx500::output_tensor_parser::InputDataInfo inputInfo3;
    inputInfo3.addr = outInfo[data_splitter::SPLITTER_ARRAY_OUTPUTTENSOR]->addr;
    inputInfo3.size = outInfo[data_splitter::SPLITTER_ARRAY_OUTPUTTENSOR]->size;
    inputInfo3.pitch = whc1.pitch;
    imx500::output_tensor_parser::FrameOutputInfo frameOutputInfo = {0};
    imx500::objectdetection_analyser::SSDMobilenetV1Data outputSSDMobilenetData;
    imx500::classification_analyser::ClassificationData outputClassificationData;
	
    // debug

    const clock_t parse_start_time = clock();
    ret = imx500::output_tensor_parser::parseOutputTensor(&inputInfo3, &frameOutputInfo);

    DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[APP] parseOutputTensor out\n");
    if (ret != 0) {
        DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] Error parsing Output Tensor ret=%d\n", ret);
        imx500::output_tensor_parser::cleanupFrameData(&frameOutputInfo);
        return -1;
    }

    DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[APP] Output Tensor Parsing time: %f sec\n", float(clock() - parse_start_time)/ CLOCKS_PER_SEC);


    //===========================================
    // PQ Setting parser
    imx500::pq_setting_parser::pqSettingInfo* pq_setting = NULL;
    if (s_pq_setting_line == 1) {
        if (outInfo[data_splitter::SPLITTER_ARRAY_PQ_SETTING]->size > sizeof(imx500::pq_setting_parser::pqSettingInfo)) {
            pq_setting = (imx500::pq_setting_parser::pqSettingInfo*)outInfo[data_splitter::SPLITTER_ARRAY_PQ_SETTING]->addr;
        }
        else {
            DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] Error PQ Setting info size = %d\n", outInfo[data_splitter::SPLITTER_ARRAY_PQ_SETTING]->size);
        }
    }
    else if (s_pq_setting_line > 1) {
        DBG_LOG_PRINT(DBG_LOG_LVL_WARNING, "[APP] Not support PQ setting line=%d\n", s_pq_setting_line);
    }
    else { /* s_pq_setting_line == 0 */
        /* do nothing */
    }


    //===========================================
    // count up (for save file)
    fil_count++;


    //===========================================
    // PPL Process
    
    

    if (HooksConfig.bSaveRawTensorOutputFiles == 1 && (fil_count % outputFileRate.outputTensorCustom) == 0)
    {
        DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[APP] network type supported=%s\n", frameOutputInfo.networkType.c_str());

        /* output file */
        FILE *file;
        char filename[1087];

        /* output tensor */
        float* data = frameOutputInfo.outputBodyInfo->address;
        createDirIfNotExist(inTensorSavePath);
        for (uint32_t i = 0; i < frameOutputInfo.outputBodyInfo->tensorNum; i++) {
            snprintf(filename, sizeof(filename), "%s/%d_outputTensor_%09d_%02d.txt", inTensorSavePath, frameOutputInfo.dnnHeaderInfo->networkId, fil_count, i);
            file = fopen(filename, "w");
            if (file)
            {
                for (uint32_t j = 0; j < frameOutputInfo.outputBodyInfo->tensorDataNum[i]; j++) {
                    fprintf(file, "%f\n", data[j]);
                }
                data += frameOutputInfo.outputBodyInfo->tensorDataNum[i];
                fclose(file);
            }
            else {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] Failed Save Output Tensor flieName %s\n", filename);
                break;
            }
        }
    }
    
    
    DBG_LOG_PRINT(DBG_LOG_LVL_TRACE, "[APP] Parsing and  Post Processing time: %f sec\n", float(clock() - begintime)/ CLOCKS_PER_SEC);
	  
    //===========================================
    // display image data
    cv::Mat colors[3];
    uint32_t pictureSize = frameInputTensorInfo.height * frameInputTensorInfo.width;
    uint32_t format = frameInputTensorInfo.format;

    if (table[E_BINNING_OPTION].value != E_BINNING_OPT_MONO) {
        if ( format == DNN_INPUT_FORMAT_RGB) {
            colors[2] = cv::Mat(frameInputTensorInfo.height, frameInputTensorInfo.width, CV_8UC1, reinterpret_cast<uint8_t*>(input_tensor_memory));
            colors[1] = cv::Mat(frameInputTensorInfo.height, frameInputTensorInfo.width, CV_8UC1, (reinterpret_cast<uint8_t*>(input_tensor_memory) + pictureSize));
            colors[0] = cv::Mat(frameInputTensorInfo.height, frameInputTensorInfo.width, CV_8UC1, (reinterpret_cast<uint8_t*>(input_tensor_memory) + pictureSize*2));
        } else if ( format == DNN_INPUT_FORMAT_BGR) {
            colors[0] = cv::Mat(frameInputTensorInfo.height, frameInputTensorInfo.width, CV_8UC1, reinterpret_cast<uint8_t*>(input_tensor_memory));
            colors[1] = cv::Mat(frameInputTensorInfo.height, frameInputTensorInfo.width, CV_8UC1, (reinterpret_cast<uint8_t*>(input_tensor_memory) + pictureSize));
            colors[2] = cv::Mat(frameInputTensorInfo.height, frameInputTensorInfo.width, CV_8UC1, (reinterpret_cast<uint8_t*>(input_tensor_memory) + pictureSize*2));

        } else {
           DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] Invalid Input Tensor Format\n");
        }
    } else {
        colors[2] = cv::Mat(frameInputTensorInfo.height, frameInputTensorInfo.width, CV_8UC1, reinterpret_cast<uint8_t*>(input_tensor_memory));
        colors[1] = cv::Mat(frameInputTensorInfo.height, frameInputTensorInfo.width, CV_8UC1, reinterpret_cast<uint8_t*>(input_tensor_memory));
        colors[0] = cv::Mat(frameInputTensorInfo.height, frameInputTensorInfo.width, CV_8UC1, reinterpret_cast<uint8_t*>(input_tensor_memory));
    }

    cv::Mat dnnrgb;
    cv::Mat resizeWin;
    cv::merge(colors, 3, resizeWin);
    std::string str;

	/* Scale Up for Display */
    float xScale = 1;
    float yScale = 1;

    cv::Rect roi(cv::Point(0,0), cv::Size(frameInputTensorInfo.width,frameInputTensorInfo.height));
    cv::resize(resizeWin(roi), dnnrgb, cv::Size(), xScale , yScale);
    
    //Create and fill the object to hand off to the end user's callback
    struct IMX500Hooks::ImageData euImageData;
    euImageData.width = frameInputTensorInfo.width;
    euImageData.height = frameInputTensorInfo.height;
    //Get the object detection data and load it up
    float *data = frameOutputInfo.outputBodyInfo->address;                                // bboxes
    float *labelData = data + frameOutputInfo.outputBodyInfo->tensorDataNum[0];           // labels
    float *confidenceData = labelData + frameOutputInfo.outputBodyInfo->tensorDataNum[1]; // confidence 
    
    for (uint32_t j = 0; j < frameOutputInfo.outputBodyInfo->tensorDataNum[2]; j++) {
        if (j > IMX500Hooks::TrainedObjects.size() || (labelData[j] == 0.0f && confidenceData[j] == 0.0f)) { break; }
        if (confidenceData[j] == 0.0f) { continue; }
        euImageData.detectedObjects.push_back({static_cast<int>(labelData[j]), confidenceData[j]});
    }
    
    cv::imencode(getDisplayImageExtension(), resizeWin, euImageData.imageBytes);       
    std::async(callback_onFrame,euImageData);
    


    /* for save PQ file */
    if ((fil_count %  table[E_SAVE_PQFILE_RATE].value) == 0)
    {
        /* output file */
        FILE *file;
        char filename[2048];

        /* PQ Setting */
        if ((table[E_SAVE_PQFILE].value == 1) && (pq_setting != NULL)) {
            snprintf(filename, sizeof(filename), "%s/PqSetting_%09d.txt", pqSavePath, fil_count);
            file = fopen(filename, "w");
            if (file) {
                fprintf(file, "Frame Count           = %d\n", pq_setting->frame_count);
                fprintf(file, "DWP_APOUT_SCL_HOFFSET = %d\n", pq_setting->dwp_apout_scl_hoffset);
                fprintf(file, "DWP_APOUT_SCL_VOFFSET = %d\n", pq_setting->dwp_apout_scl_voffset);
                fprintf(file, "DWP_APOUT_SCL_HSIZE   = %d\n", pq_setting->dwp_apout_scl_hsize);
                fprintf(file, "DWP_APOUT_SCL_VSIZE   = %d\n", pq_setting->dwp_apout_scl_vsize);
                fclose(file);
            }
            else {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[APP] Failed Save PQ Setting flieName %s\n", filename);
            }
        }
    }

    //===========================================
    // cleanup Frame Data
    free(input_tensor_memory);
    input_tensor_memory = NULL;
    imx500::output_tensor_parser::cleanupFrameData(&frameOutputInfo);

    return 0;

}

int32_t image_file_read(FILE* fp, uint8_t* p_dst, uint32_t* p_size) {

    if (fp == NULL) {
        return -1;
    }

    /* read image file */
    *p_size = 0;
    while(1) {
        int getf;
        getf = fgetc(fp);
        if (getf == EOF && !feof(fp)) {
            return -1;
        }
        if (!feof(fp)) {
            *p_dst++ = (uint8_t)getf;
            *p_size += 1;
        } else {
            break;
        }
    }
    return 0;
}

static void  cleanup_image_data(imx500::control::BootParam* p_boot_param) {

    if(p_boot_param->m_pImageDataLoader != NULL) {
        free(p_boot_param->m_pImageDataLoader);
    }
    if(p_boot_param->m_pImageDataMainFw != NULL) {
        free(p_boot_param->m_pImageDataMainFw);
    }
    if(p_boot_param->m_pImageDataNetWk != NULL) {
        free(p_boot_param->m_pImageDataNetWk);
    }
    if(p_boot_param->m_pImageDataDwpCp != NULL) {
        free(p_boot_param->m_pImageDataDwpCp);
    }
}

static void  copy_byteswap_buffer(uint8_t* data, uint32_t size) {
    uint32_t pos;
    uint8_t tmp[4];

    pos = 0;

    while ( pos < size) {
        tmp[0] = data[0 + pos];
        tmp[1] = data[1 + pos];
        tmp[2] = data[2 + pos];
        tmp[3] = data[3 + pos];

        data[3 + pos] = tmp[0];
        data[2 + pos] = tmp[1];
        data[1 + pos] = tmp[2];
        data[0 + pos] = tmp[3];
        pos = pos + 4;
    }
}

int createDirIfNotExist(char *path) {
    int status = 1;
    if (std::string(path) != "." && std::string(path) != "./") {
        char* p = path;
        //S_IRWXU: Write permission bit for the owner of the file. Usually 0200. S_IWRITE is an obsolete synonym provided for BSD compatibility.
        //S_IRGRP: Read permission bit for the group owner of the file. Usually 040.
        //S_IXGRP: Execute or search permission bit for the group owner of the file. Usually 010.
        //S_IROTH: Read permission bit for other users. Usually 04.
        //S_IXOTH: Execute or search permission bit for other users. Usually 01.
        //mode_t mode = S_IRWXU | S_IRGRP |  S_IXGRP | S_IROTH | S_IXOTH;
        mode_t mode = S_IRWXU | S_IRGRP | S_IROTH;

        // Do mkdir for each slash until end of string or error
        while (*p != '\0') {
            // Skip first character
            p++;

            // Find first slash or end
            while(*p != '\0' && *p != '/') p++;

            // Remember value from p
            char v = *p;

            // Write end of string at p
            *p = '\0';

            // Create folder from path to '\0' inserted at p
            if(mkdir(path, mode) == -1 && errno != EEXIST) {
                *p = v;
                status = 0;
                break;
            }

            // Restore path to it's former glory
            *p = v;
        }
    }
    return status;
}
