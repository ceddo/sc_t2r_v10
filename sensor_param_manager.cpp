/*
* Copyright 2017-2018 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/

#include <stdint.h>
#include "return_code.h"
#include "common_define.h"
#include "i2c_access.h"
#include "sensor_control.h"
#include "sensor_param_manager.h"
#include "imx500lib_sample.h"
#include "dbg_log.h"


/* define MACROs */
/*
 * IMX500 Register default value
 */
#define DEF_VAL_NOTIMAGING_ONLY_MODE_FW         (0x00)          /* not image only */
#define DEF_VAL_IMAGING_ONLY_MODE_FW            (0x01)          /* image only */
#define DEF_VAL_DD_X_OUT_SIZE_V2H2              (2028)
#define DEF_VAL_DD_Y_OUT_SIZE_V2H2              (1520)
#define DEF_VAL_DD_X_OUT_SIZE_FULLSIZE          (4056)
#define DEF_VAL_DD_Y_OUT_SIZE_FULLSIZE          (3040)
#define DEF_VAL_SCALE_MODE                      (0x02)
#define DEF_VAL_SCALE_MODE_EXT                  (0x01)
#define DEF_VAL_SCALE_M                         (16)
#define DEF_VAL_SCALE_M_EXT                     (DEF_VAL_SCALE_M)
#define DEF_VAL_BINNING_MODE                    (0x01)
#define DEF_VAL_BINNING_TYPE                    (0x22)          /* [7:4]: Horizontal, [3:0]: Vertical */
#define DEF_VAL_BINNING_PRIORITY                (0x80)          /* [7]: Binning options(0: Bayer pixcel binning, 1: Adjacent pixel binning) */
#define DEF_VAL_DNN_MAX_RUN_TIME                (33)
#define DEF_VAL_WBGAIN_THMODE                   (0x01)          /* 0: normal, 1: gain through */
#define DEF_VAL_BINNING_WEIGHTING_BAYER         (0x02)          /* 0: additional average, 1: addition (summed), 2: weighting additional average, 3: module-specific */
#define DEF_VAL_BINNING_WEIGHTING_MONO          (0x00)          /* 0: additional average, 1: addition (summed), 2: weighting additional average, 3: module-specific */
#define DEF_VAL_SUB_WEIGHTING_BAYER             (0x03)          /* [0]: Vertical, [1]: Horizontal */
#define DEF_VAL_SUB_WEIGHTING_MONO              (0x00)          /* [0]: Vertical, [1]: Horizontal */

#define IMX500_HW_ALIGN                         (16)
#define IMX500_HW_LIMIT_LINE_PIX                (2560)

//#define RAW_IMAGE_WIDHT_MIN                     (176)
//#define RAW_IMAGE_HEIGHT_MIN                    (144)

#define BINNING_OPT_BAYER                       (0)
#define BINNING_OPT_MONO                        (1)

/*
 * Frame rate define
 */
#define TOTAL_NUM_OF_IVTPX_CH                   (4)
#define IVTPXCK_SYCK_DIV                        (2)
#define IVTPXCK_PXCK_DIV                        (5)
#define IVTPXCK_CLOCK_DIVISION_RATIO            (IVTPXCK_SYCK_DIV * IVTPXCK_PXCK_DIV)
#define FRM_LENGTH_LINES_SIZE_MAX               (65535)
#define PIXEL_RATE_FOR_CALC_FRM_RATE            ((210000000 - 500000) * TOTAL_NUM_OF_IVTPX_CH)


extern nw_info networkInfoArray[];


/* structures */
typedef struct tagImx500Param {
    uint32_t    m_inckFreq;
    uint16_t    m_rawImgWidth;
    uint16_t    m_rawImgHeight;
    uint16_t    m_rawImgWidthMax;
    uint16_t    m_rawImgHeightMax;
} Imx500Param;

/* static variables */
static Imx500Param s_imx500Param;


namespace imx500 {
    namespace param_manager {

        /* static function */
        static E_RESULT_CODE SetMipiBitRate(uint32_t mipiLane);

        /**
         * @brief SetInck.
         */
        E_RESULT_CODE SetInck(uint32_t inckFreq, uint32_t mipiDataRate, uint8_t imgOnlyMode, uint8_t flashType) {

            uint32_t    inck;
            uint32_t    inck_decimal;
            uint8_t     prepllckDiv;
            uint16_t    pllMpy;

            /* Setting IMAGING_ONLY_MODE_FW */
            if(imgOnlyMode == 1) {
                I2C_ACCESS_WRITE(REG_ADDR_IMAGING_ONLY_MODE_FW, DEF_VAL_IMAGING_ONLY_MODE_FW, REG_SIZE_IMAGING_ONLY_MODE_FW);
            } else {
                I2C_ACCESS_WRITE(REG_ADDR_IMAGING_ONLY_MODE_FW, DEF_VAL_NOTIMAGING_ONLY_MODE_FW, REG_SIZE_IMAGING_ONLY_MODE_FW);
            }

            /* calc INCK */
            inck = ((inckFreq >> 8) & 0xFF) * FREQ_1MHZ;
            inck_decimal = inckFreq & 0xFF;
            for (uint8_t i = 0; i < 8; i++) {
                inck += ((inck_decimal >> (7 - i)) & 1) * (FREQ_1MHZ >> (i + 1));
            }

            /* Setting IVT_PREPLLCK_DIV, IOP_PREPLLCK_DIV */
            if ((inck < INCK_12MHZ) || (inck > INCK_27MHZ)) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][SetInck] Failed INCK value : val=%d\n", inck);
                return E_INVALID_PARAM;
            }
            else if (inck <= INCK_23MHZ) {
                prepllckDiv = 1;
            }
            else {
                prepllckDiv = 2;
            }
            s_imx500Param.m_inckFreq = inck;

            I2C_ACCESS_WRITE(REG_ADDR_IVT_PREPLLCK_DIV_C03, prepllckDiv, REG_SIZE_IVT_PREPLLCK_DIV_C03);
            I2C_ACCESS_WRITE(REG_ADDR_IOP_PREPLLCK_DIV_C03, prepllckDiv, REG_SIZE_IOP_PREPLLCK_DIV_C03);

            /* Setting IVT_PLL_MPY, IOP_PLL_MPY */
            pllMpy = TARGET_PLL_FREQ / (s_imx500Param.m_inckFreq / prepllckDiv);
            I2C_ACCESS_WRITE(REG_ADDR_IVT_PLL_MPY_C03, pllMpy, REG_SIZE_IVT_PLL_MPY_C03);
            pllMpy = (mipiDataRate * FREQ_1MHZ) / (s_imx500Param.m_inckFreq / prepllckDiv);
            I2C_ACCESS_WRITE(REG_ADDR_IOP_PLL_MPY_C03, pllMpy, REG_SIZE_IOP_PLL_MPY_C03);

            /* Setting EXCK_FREQ */
            I2C_ACCESS_WRITE(REG_ADDR_EXCK_FREQ_C01, inckFreq, REG_SIZE_EXCK_FREQ_C01);

            /* Setting FLASH_TYPE */
            I2C_ACCESS_WRITE(REG_ADDR_FLASH_TYPE, flashType, REG_SIZE_FLASH_TYPE);

            return E_OK;
        }

        /**
         * @brief SetDefaultParam.
         */
        E_RESULT_CODE SetDefaultParam() {

            I2C_ACCESS_WRITE(REG_ADDR_SCALE_MODE, DEF_VAL_SCALE_MODE, REG_SIZE_SCALE_MODE);
            I2C_ACCESS_WRITE(REG_ADDR_SCALE_MODE_EXT, DEF_VAL_SCALE_MODE_EXT, REG_SIZE_SCALE_MODE_EXT);
            return E_OK;
        }

        /**
         * @brief SetValiableParam.
         */
        E_RESULT_CODE SetValiableParam(uint8_t raw_full_size_ena, uint8_t frame_rate, uint8_t binning_opt) {

            E_RESULT_CODE ret_code;
            uint32_t mipiLane;
            uint32_t xOutSize;
            uint32_t yOutSize;

            /* get register value */
            I2C_ACCESS_READ(REG_ADDR_CSI_LANE_MODE, &mipiLane, REG_SIZE_CSI_LANE_MODE);
            I2C_ACCESS_READ(REG_ADDR_X_OUT_SIZE, &xOutSize, REG_SIZE_X_OUT_SIZE);
            I2C_ACCESS_READ(REG_ADDR_Y_OUT_SIZE, &yOutSize, REG_SIZE_Y_OUT_SIZE);

            /* --- MIPI Global timing ----------------------------- */
            ret_code = SetMipiBitRate(mipiLane + 1);
            if (ret_code != E_OK) { 
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][SetValPrm] called error SetMimiBitRate()\n");
                return ret_code;
            }

            /* ---  image size ------------------------------------ */
            if (mipiLane == 1) {
                if(raw_full_size_ena == 1) { /* FULL size */
                    s_imx500Param.m_rawImgWidthMax = DEF_VAL_DD_X_OUT_SIZE_FULLSIZE;
                    s_imx500Param.m_rawImgHeightMax = DEF_VAL_DD_Y_OUT_SIZE_FULLSIZE;

                } else { /* V2H2 size */
                    s_imx500Param.m_rawImgWidthMax = DEF_VAL_DD_X_OUT_SIZE_V2H2;
                    s_imx500Param.m_rawImgHeightMax = DEF_VAL_DD_Y_OUT_SIZE_V2H2;

                    /* set binning register */
                    I2C_ACCESS_WRITE(REG_ADDR_BINNING_MODE, DEF_VAL_BINNING_MODE, REG_SIZE_BINNING_MODE);
                    I2C_ACCESS_WRITE(REG_ADDR_BINNING_TYPE, DEF_VAL_BINNING_TYPE, REG_SIZE_BINNING_TYPE);

                    /* ---  binning option -------------------------------- */
                    if (binning_opt == BINNING_OPT_MONO) {
                        /* set binning priority register */
                        I2C_ACCESS_WRITE(REG_ADDR_BINNING_PRIORITY_V, DEF_VAL_BINNING_PRIORITY, REG_SIZE_BINNING_PRIORITY_V);
                        I2C_ACCESS_WRITE(REG_ADDR_BINNING_PRIORITY_H, DEF_VAL_BINNING_PRIORITY, REG_SIZE_BINNING_PRIORITY_H);
                        /* set WB gain through mode */
                        I2C_ACCESS_WRITE(REG_ADDR_WBGAIN_THMODE, DEF_VAL_WBGAIN_THMODE, REG_SIZE_WBGAIN_THMODE);
                        /* set binning weighting */
                        I2C_ACCESS_WRITE(REG_ADDR_BINNING_WEIGHTING, DEF_VAL_BINNING_WEIGHTING_MONO, REG_SIZE_BINNING_WEIGHTING);
                        I2C_ACCESS_WRITE(REG_ADDR_SUB_WEIGHTING, DEF_VAL_SUB_WEIGHTING_MONO, REG_SIZE_SUB_WEIGHTING);
                    }
                    else {
                        /* set binning weighting */
                        I2C_ACCESS_WRITE(REG_ADDR_BINNING_WEIGHTING, DEF_VAL_BINNING_WEIGHTING_BAYER, REG_SIZE_BINNING_WEIGHTING);
                        I2C_ACCESS_WRITE(REG_ADDR_SUB_WEIGHTING, DEF_VAL_SUB_WEIGHTING_BAYER, REG_SIZE_SUB_WEIGHTING);
                    }
                }
            }
            else { /* == 4lane */
                s_imx500Param.m_rawImgWidthMax = DEF_VAL_DD_X_OUT_SIZE_FULLSIZE;
                s_imx500Param.m_rawImgHeightMax = DEF_VAL_DD_Y_OUT_SIZE_FULLSIZE;
            }
            s_imx500Param.m_rawImgWidth = xOutSize;
            s_imx500Param.m_rawImgHeight = yOutSize;

            ret_code = SetScalingRawImg(xOutSize, yOutSize);
            if (ret_code != E_OK) { 
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][SetValPrm] called error SetScalingRawImg()\n");
                return ret_code;
            }

            /* --- framerate -------------------------------------- */
            ret_code = SetFrameRate(frame_rate);
            if (ret_code != E_OK) { 
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][SetValPrm] called error SetFrameRate()\n");
                return ret_code;
            }

            return E_OK;
        }

        /**
         * @brief SetExecDnnIndex
         */
        E_RESULT_CODE SetExecDnnIndex(uint8_t dnnIndex) {
            if (dnnIndex >= 3) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][SetExecDnnIndex] Error: Invalid dnnIndex\n");
                return E_INVALID_PARAM; 
            }
             /* set register */
            I2C_ACCESS_WRITE(REG_ADDR_DNN_EXEC_NETWORK, dnnIndex, REG_SIZE_DNN_EXEC_NETWORK);

            return E_OK;
        }

        /**
         * @brief SetCroppingDnnImg.
         */
        E_RESULT_CODE SetCroppingDnnImg(uint16_t left, uint16_t top, uint16_t width, uint16_t height) {
             uint32_t dnnIndex;
             I2C_ACCESS_READ(REG_ADDR_DNN_EXEC_NETWORK, &dnnIndex, REG_SIZE_DNN_EXEC_NETWORK);
             
             if (dnnIndex >= MAX_NUM_OF_NETWORKS) {
                 DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][SetCrpDnnImg] Invalid dnnIndex %d\n", dnnIndex);
                 return E_OTHER;
             }

            /* --- Horizontal cropping --------------------------------------------------------------- */
            /* correct width */
            if (width < networkInfoArray[dnnIndex].inputTensorWidth) { 
                DBG_LOG_PRINT(DBG_LOG_LVL_WARNING, "[IMX500LIB][SetCrpDnnImg] correct width(under tensor size) : %d -> %d\n", width, networkInfoArray[dnnIndex].inputTensorWidth);
                width = networkInfoArray[dnnIndex].inputTensorWidth;
            }
            /* correct left */
            if ((left + width) > DNN_IMAGE_WIDTH_MAX) { 
                DBG_LOG_PRINT(DBG_LOG_LVL_WARNING, "[IMX500LIB][SetCrpDnnImg] correct left(over max): %d -> %d\n", left, DNN_IMAGE_WIDTH_MAX - width);
                left = DNN_IMAGE_WIDTH_MAX - width;
            }
            /* set register */
            I2C_ACCESS_WRITE(REG_ADDR_DWP_AP_VC_HOFFSET, left, REG_SIZE_DWP_AP_VC_HOFFSET);
            I2C_ACCESS_WRITE(REG_ADDR_DWP_AP_VC_HSIZE, width, REG_SIZE_DWP_AP_VC_HSIZE);

            /* --- Vertical cropping ----------------------------------------------------------------- */
            /* correct height */
            if (height < networkInfoArray[dnnIndex].inputTensorHeight) { 
                DBG_LOG_PRINT(DBG_LOG_LVL_WARNING, "[IMX500LIB][SetCrpDnnImg] correct height(under tensor size): %d -> %d\n", height, networkInfoArray[dnnIndex].inputTensorHeight);
                height = networkInfoArray[dnnIndex].inputTensorHeight;
            }
            /* correct top */
            if ((top + height) > DNN_IMAGE_HEIGHT_MAX) { 
                DBG_LOG_PRINT(DBG_LOG_LVL_WARNING, "[IMX500LIB][SetCrpDnnImg] correct top(over max) : %d -> %d\n", top, DNN_IMAGE_HEIGHT_MAX - height);
                top = DNN_IMAGE_HEIGHT_MAX - height;
            }
            /* set register */
            I2C_ACCESS_WRITE(REG_ADDR_DWP_AP_VC_VOFFSET, top, REG_SIZE_DWP_AP_VC_VOFFSET);
            I2C_ACCESS_WRITE(REG_ADDR_DWP_AP_VC_VSIZE, height, REG_SIZE_DWP_AP_VC_VSIZE);

            return E_OK;
        }

        /**
         * @brief SetCroppingRawImg.
         */
        E_RESULT_CODE SetCroppingRawImg(uint16_t left, uint16_t top, uint16_t width, uint16_t height) {

            E_RESULT_CODE return_code;

            /* --- Horizontal cropping --------------------------------------------------------------- */
            /* correct width */
            if (width > s_imx500Param.m_rawImgWidthMax) { 
                DBG_LOG_PRINT(DBG_LOG_LVL_WARNING, "[IMX500LIB][SetCrpRawImg] correct width(over max) : %d -> %d\n", width, s_imx500Param.m_rawImgWidthMax);
                width = s_imx500Param.m_rawImgWidthMax;
            }
            /* correct left */
            if ((left + width) > s_imx500Param.m_rawImgWidthMax) { 
                DBG_LOG_PRINT(DBG_LOG_LVL_WARNING, "[IMX500LIB][SetCrpRawImg] correct left(over max) : %d -> %d\n", left, s_imx500Param.m_rawImgWidthMax - width);
                left = s_imx500Param.m_rawImgWidthMax - width;
            }
            /* set register */
            I2C_ACCESS_WRITE(REG_ADDR_DIG_CROP_X_OFFSET, left, REG_SIZE_DIG_CROP_X_OFFSET);
            I2C_ACCESS_WRITE(REG_ADDR_DIG_CROP_IMAGE_WIDTH, width, REG_SIZE_DIG_CROP_IMAGE_WIDTH);

            /* --- Vertical cropping ----------------------------------------------------------------- */
            /* correct height */
            if (height > s_imx500Param.m_rawImgHeightMax) { 
                DBG_LOG_PRINT(DBG_LOG_LVL_WARNING, "[IMX500LIB][SetCrpRawImg] correct height(over max)  : %d -> %d\n", height, s_imx500Param.m_rawImgHeightMax);
                height = s_imx500Param.m_rawImgHeightMax;
            }
            /* correct top */
            if ((top + height) > s_imx500Param.m_rawImgHeightMax) { 
                DBG_LOG_PRINT(DBG_LOG_LVL_WARNING, "[IMX500LIB][SetCrpRawImg] correct top(over max)  : %d -> %d\n", top, s_imx500Param.m_rawImgHeightMax - height);
                top = s_imx500Param.m_rawImgHeightMax - height;
            }
            /* set register */
            I2C_ACCESS_WRITE(REG_ADDR_DIG_CROP_Y_OFFSET, top, REG_SIZE_DIG_CROP_Y_OFFSET);
            I2C_ACCESS_WRITE(REG_ADDR_DIG_CROP_IMAGE_HEIGHT, height, REG_SIZE_DIG_CROP_IMAGE_HEIGHT);

            /* --- scaling resize -------------------------------------------------------------------- */
            return_code = SetScalingRawImg(s_imx500Param.m_rawImgWidth, s_imx500Param.m_rawImgHeight);

            return return_code;
        }

        /**
         * @brief SetScalingRawImg.
         */
        E_RESULT_CODE SetScalingRawImg(uint16_t width, uint16_t height) {

            uint32_t crop_width;
            uint32_t crop_height;
            uint16_t scale_ratio;
            uint16_t image_size;
            E_RESULT_CODE return_code;

            /* store requeset value */
            s_imx500Param.m_rawImgWidth = width;
            s_imx500Param.m_rawImgHeight = height;

            /* get crop size */
            I2C_ACCESS_READ(REG_ADDR_DIG_CROP_IMAGE_WIDTH, &crop_width, REG_SIZE_DIG_CROP_IMAGE_WIDTH);
            I2C_ACCESS_READ(REG_ADDR_DIG_CROP_IMAGE_HEIGHT, &crop_height, REG_SIZE_DIG_CROP_IMAGE_HEIGHT);

            /* --- Horizontal scaling ---------------------------------------------------------------- */
            if (width > s_imx500Param.m_rawImgWidthMax) { 
                DBG_LOG_PRINT(DBG_LOG_LVL_WARNING, "[IMX500LIB][SetSclRawImg] correct width(over max)  : %d -> %d\n", width, s_imx500Param.m_rawImgWidthMax);
                width = s_imx500Param.m_rawImgWidthMax;
            }
            if (width >= crop_width) {
                if (width > crop_width) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_WARNING, "[IMX500LIB][SetSclRawImg] correct width(over crop) : %d -> %d\n", width, crop_width);
                }
                scale_ratio = DEF_VAL_SCALE_M;
                width = crop_width;
            }
            else {
                /* calc ratio */
                scale_ratio = (crop_width * DEF_VAL_SCALE_M) / width;
                image_size = ((crop_width * DEF_VAL_SCALE_M) / scale_ratio) & 0xFFFC;  /* multiple of 4 */
                if (image_size < RAW_IMAGE_WIDTH_MIN) {
                    image_size = RAW_IMAGE_WIDTH_MIN;
                }
                if (image_size != width) {  /* for debug */
                    DBG_LOG_PRINT(DBG_LOG_LVL_WARNING, "[IMX500LIB][SetSclRawImg] (crop_width x ratio) : width = %d : %d\n", image_size, width);
                }
            }
            /* set register */
            I2C_ACCESS_WRITE(REG_ADDR_SCALE_M, scale_ratio, REG_SIZE_SCALE_M);
            I2C_ACCESS_WRITE(REG_ADDR_X_OUT_SIZE, width, REG_SIZE_X_OUT_SIZE);
            return_code = SetOutSizeDnnCH(width);
            if (return_code != E_OK) {
                DBG_LOG_PRINT(DBG_LOG_LVL_ERROR, "[IMX500LIB][SetSclRawImg] called error SetOutSizeDnnCH()\n");
                return return_code;
            }

            /* --- Vertical scaling ------------------------------------------------------------------ */
            if (height > s_imx500Param.m_rawImgHeightMax) { 
                DBG_LOG_PRINT(DBG_LOG_LVL_WARNING, "[IMX500LIB][SetSclRawImg] correct height(over max) : %d -> %d\n", height, s_imx500Param.m_rawImgHeightMax);
                height = s_imx500Param.m_rawImgHeightMax;
            }
            if (height >= crop_height) {
                if (height > crop_height) {
                    DBG_LOG_PRINT(DBG_LOG_LVL_WARNING, "[IMX500LIB][SetSclRawImg] correct height(over crop): %d -> %d\n", height, crop_height);
                }
                scale_ratio = DEF_VAL_SCALE_M_EXT;
                height = crop_height;
            }
            else {
                /* calc ratio */
                scale_ratio = (crop_height * DEF_VAL_SCALE_M_EXT) / height;
                image_size = ((crop_height * DEF_VAL_SCALE_M_EXT) / scale_ratio) & 0xFFFE;    /* multiple of 2 */
                if (image_size < RAW_IMAGE_HEIGHT_MIN) {
                    image_size = RAW_IMAGE_HEIGHT_MIN;
                }
                if (image_size != height) {  /* for debug */
                    DBG_LOG_PRINT(DBG_LOG_LVL_WARNING, "[IMX500LIB][SetSclRawImg] (crop_height x ratio) : height = %d : %d\n", image_size, height);
                }
            }
            /* set register */
            I2C_ACCESS_WRITE(REG_ADDR_SCALE_M_EXT, scale_ratio, REG_SIZE_SCALE_M_EXT);
            I2C_ACCESS_WRITE(REG_ADDR_Y_OUT_SIZE, height, REG_SIZE_Y_OUT_SIZE);

            return E_OK;
        }

        /**
         * @brief SetOutSizeDnnCH.
         */
        E_RESULT_CODE SetOutSizeDnnCH(uint16_t xOutSize) {

            uint16_t correct_size;
            uint32_t header_line, in_body_line, out_body_line,tmp_out_body_line;
            uint32_t header_size = networkInfoArray[0].dnnHeaderSize;
            uint32_t input_tensor_size = networkInfoArray[0].inputTensorSize;
        
            for (uint32_t i = 0; i< MAX_NUM_OF_NETWORKS; i++) {
                if( networkInfoArray[i].inputTensorSize > input_tensor_size) {
                     input_tensor_size = networkInfoArray[i].inputTensorSize;
                }
            } 


            /* calc correct size */
            correct_size = (xOutSize / IMX500_HW_ALIGN) * IMX500_HW_ALIGN;
            if (correct_size > IMX500_HW_LIMIT_LINE_PIX) {
                correct_size = IMX500_HW_LIMIT_LINE_PIX;
            }

            /* calc line */
            header_line = (header_size + (correct_size - 1)) / correct_size;
            in_body_line = header_line + ((input_tensor_size + (correct_size - 1)) / correct_size);
            out_body_line = header_line;
            
            for (uint32_t i = 0; i< MAX_NUM_OF_NETWORKS; i++) {
                tmp_out_body_line = header_line;
                for (uint32_t j = 0; j < networkInfoArray[i].outputTensorNum; j++) {
                    tmp_out_body_line += ((networkInfoArray[i].p_outputTensorSize[j] + (correct_size - 1)) / correct_size);
                }

                if (tmp_out_body_line > out_body_line) {
                    out_body_line = tmp_out_body_line;
                }
            }

            /* set register */
            I2C_ACCESS_WRITE(REG_ADDR_DD_CH06_X_OUT_SIZE, xOutSize, REG_SIZE_DD_CH06_X_OUT_SIZE);
            I2C_ACCESS_WRITE(REG_ADDR_DD_CH07_X_OUT_SIZE, xOutSize, REG_SIZE_DD_CH07_X_OUT_SIZE);
            I2C_ACCESS_WRITE(REG_ADDR_DD_CH08_X_OUT_SIZE, xOutSize, REG_SIZE_DD_CH08_X_OUT_SIZE);
            I2C_ACCESS_WRITE(REG_ADDR_DD_CH09_X_OUT_SIZE, xOutSize, REG_SIZE_DD_CH09_X_OUT_SIZE);
            /* I2C_ACCESS_WRITE(REG_ADDR_DD_CH06_Y_OUT_SIZE, 1, REG_SIZE_DD_CH06_Y_OUT_SIZE); *//* set with property::set_reg_value */
            I2C_ACCESS_WRITE(REG_ADDR_DD_CH07_Y_OUT_SIZE, in_body_line, REG_SIZE_DD_CH07_Y_OUT_SIZE);
            I2C_ACCESS_WRITE(REG_ADDR_DD_CH08_Y_OUT_SIZE, out_body_line, REG_SIZE_DD_CH08_Y_OUT_SIZE);
            /* I2C_ACCESS_WRITE(REG_ADDR_DD_CH09_Y_OUT_SIZE, 1, REG_SIZE_DD_CH09_Y_OUT_SIZE); *//* set with property::set_reg_value */

            return E_OK;
        }

        /**
         * @brief SetFrameRate.
         */
        E_RESULT_CODE SetFrameRate(uint8_t frmRate) {

            uint32_t lineLenPck;
            uint32_t lineLenInck;
            uint32_t prepllckDiv;
            uint32_t pllMpy;
            uint32_t pixelRate;
            uint32_t frmLineLen;
            uint32_t fllLshift = 0;
            uint32_t dnnMaxRunTime;

            /* [NOTE]
               Frame Rate[frame/s] = Pixel_rate[pixels/s] / Total_number_of_pixels[pixels/frame]
                 Pixel_rate[pixels/s] = IVTPXCK[MHz] * 4(Total number of IVTPX channel)
                 Total_number_of_pixels[pixels/frame] = (FRM_LENGTH_LINES[lines/frame] * 2^FLL_LSHIFT) * LINE_LENGTH_PCK[pixels/line]
            */

            /* get LINE_LENGTH_PCK */
            I2C_ACCESS_READ(REG_ADDR_LINE_LENGTH_PCK_C03, &lineLenPck, REG_SIZE_LINE_LENGTH_PCK_C03);

            /* calc LINE_LENGTH_INCK */
            I2C_ACCESS_READ(REG_ADDR_IVT_PREPLLCK_DIV_C03, &prepllckDiv, REG_SIZE_IVT_PREPLLCK_DIV_C03);
            I2C_ACCESS_READ(REG_ADDR_IVT_PLL_MPY_C03, &pllMpy, REG_SIZE_IVT_PLL_MPY_C03);
            lineLenInck = ((lineLenPck + 4) / TOTAL_NUM_OF_IVTPX_CH) * IVTPXCK_CLOCK_DIVISION_RATIO * prepllckDiv;
            lineLenInck = (lineLenInck + (pllMpy - 1)) / pllMpy;
            I2C_ACCESS_WRITE(REG_ADDR_LINE_LENGTH_INCK_M0F, lineLenInck, REG_SIZE_LINE_LENGTH_INCK_M0F);

            /* calc Pixel_rate */
            pixelRate = PIXEL_RATE_FOR_CALC_FRM_RATE;   /* Use fixed value for frame rate calculation */

            /* calc FRM_LENGTH_LINES & FLL_LSHIFT */
            frmLineLen = pixelRate / (frmRate * lineLenPck);
            frmLineLen = (frmLineLen / 4) * 4;
            while (frmLineLen > FRM_LENGTH_LINES_SIZE_MAX) {
                fllLshift++;
                frmLineLen >>= 1;
            }

            /* calc DNN_MAX_RUN_TIME */
            dnnMaxRunTime = (1000 / frmRate) - DEF_VAL_DNN_MAX_RUN_TIME;

            /* set register */
            I2C_ACCESS_WRITE(REG_ADDR_FRM_LENGTH_LINES_C03, frmLineLen, REG_SIZE_FRM_LENGTH_LINES_C03);
            I2C_ACCESS_WRITE(REG_ADDR_FLL_LSHIFT_M02, fllLshift, REG_SIZE_FLL_LSHIFT_M02);
            I2C_ACCESS_WRITE(REG_ADDR_DNN_MAX_RUN_TIME, dnnMaxRunTime, REG_SIZE_DNN_MAX_RUN_TIME);

            return E_OK;
        }

        /**
         * @brief SetMipiBitRate.
         */
        static E_RESULT_CODE SetMipiBitRate(uint32_t mipiLane) {

            uint32_t prepllckDiv;
            uint32_t pllMpy;
            uint32_t outBitRate;
            uint32_t outBitRateInt;
            uint32_t outBitRateDec;
            uint32_t outBitRateTmp;

            /* [NOTE]
               Output Bit Rate[Mbps] = (PLL_OUT / IOP_SYCK_DIV) * Lane number
                                        PLL_OUT = (INCK / IOP_PREPLLCK_DIV) * IOP_PLL_MPY
                                        IOP_SYCK_DIV = 1
            */

            /* calc Output Bit Rate */
            I2C_ACCESS_READ(REG_ADDR_IOP_PREPLLCK_DIV_C03, &prepllckDiv, REG_SIZE_IVT_PREPLLCK_DIV_C03);
            I2C_ACCESS_READ(REG_ADDR_IOP_PLL_MPY_C03, &pllMpy, REG_SIZE_IOP_PLL_MPY_C03);
            outBitRate = (s_imx500Param.m_inckFreq / prepllckDiv) * pllMpy * mipiLane;
            outBitRateInt = outBitRate / FREQ_1MHZ;
            outBitRateTmp = outBitRate % FREQ_1MHZ;
            outBitRateDec = 0;
            for (uint32_t i = 0; i < 16; i++) {
                outBitRateDec <<= 1;
                outBitRateTmp *= 2;
                if (outBitRateTmp >= FREQ_1MHZ) {
                    outBitRateDec |= 1;
                    outBitRateTmp -= FREQ_1MHZ;
                }
            }
            I2C_ACCESS_WRITE(REG_ADDR_REQ_LINK_BIT_RATE_MBPS_C08, (outBitRateInt << 16) | outBitRateDec, REG_SIZE_REQ_LINK_BIT_RATE_MBPS_C08);

            return E_OK;
        }

    } /* namespace param_manager */
} /* namespace imx500 */
