/*
* Copyright 2017-2018 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/
#ifndef SENSOR_CONTROL_PRIVATE_H_
#define SENSOR_CONTROL_PRIVATE_H_

#include <stdint.h>
#include "return_code.h"

/*
* Input Tensor Namespace
*/
namespace imx500 {
    namespace control {

        /* enums */
        typedef enum tagSensoStatus {
            SENSOR_INIT = 0,
            SENSOR_STNDBY,
            SENSOR_STREAM
        } SensoStatus;

        SensoStatus get_status();

    } /* namespace control */
} /* namespace imx500 */

#endif /* SENSOR_CONTROL_PRIVATE_H_ */
