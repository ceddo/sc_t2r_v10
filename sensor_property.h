/*
* Copyright 2017-2018 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/
#ifndef SENSOR_PROPERTY_H_
#define SENSOR_PROPERTY_H_

#include <stdint.h>
#include "return_code.h"

/*
* Input Tensor Namespace
*/
namespace imx500 {
    namespace property {

        typedef enum tagE_PIXEL_FMT {
            E_PIXEL_FMT_SGBRG10,
            E_PIXEL_FMT_RGB888PLANAR
        } E_PIXEL_FMT;

        typedef enum tagE_AE_MODE {
            E_AE_MODE_AUTO,
            E_AE_MODE_GAINFIX,
            E_AE_MODE_TIMEFIX,
            E_AE_MODE_HOLD,
            E_AE_MODE_MANUAL
        } E_AE_MODE;

        typedef enum tagE_AE_METERING {
            E_AE_METERING_NONE,
            E_AE_METERING_AVERAGE,
            E_AE_METERING_WEIGHT,
            E_AE_METERING_SPOT,
            E_AE_METERING_MATRIX
        } E_AE_METERING;

        typedef enum tagE_AWB_MODE {
            E_AWB_MODE_AUTO,
            E_AWB_MODE_MANUAL
        } E_AWB_MODE;

        typedef struct tagFrameRateParam {
            uint32_t        m_num;
            uint32_t        m_denom;
        } FrameRateParam;

        typedef struct tagImageParam {
            uint32_t        m_width;
            uint32_t        m_height;
            uint32_t        m_stride;
            E_PIXEL_FMT     m_pixelFmt;
        } ImageParam;

        typedef struct tagAeParam {
            E_AE_MODE       m_mode;
            float           m_evCompensation;
            uint32_t        m_exposureTime;
            uint32_t        m_isoSensitivity;
            E_AE_METERING   m_metering;
        } AeParam;

        typedef struct tagAwbParam {
            E_AWB_MODE      m_mode;
        } AwbParam;

        typedef struct tagCropParam {
            uint32_t        m_left;
            uint32_t        m_top;
            uint32_t        m_width;
            uint32_t        m_height;
        } CropParam;

        typedef struct tagPsvParam {
            uint32_t        m_level;
        } PsvParam;

        typedef struct tagIspSptParam {
            bool            m_ae;
            bool            m_awb;
            bool            m_brightness;
            bool            m_iso;
            bool            m_expTime;
            bool            m_expMetering;
            bool            m_gamma;
            bool            m_gain;
            bool            m_hue;
            bool            m_saturation;
            bool            m_sharpness;
            bool            m_wb;
        } IspSptParam;

        typedef struct tagRegSetting {
            uint16_t    m_addr;
            uint8_t     m_size;
            uint8_t     m_rsvd;
            uint32_t    m_value;
        } RegSetting;

        /* set function */
        E_RESULT_CODE set_framerate(FrameRateParam *param);
        E_RESULT_CODE set_dnn_image_size(ImageParam *param);
        E_RESULT_CODE set_raw_image_size(ImageParam *param);
        E_RESULT_CODE set_auto_exposure(AeParam *param);
        E_RESULT_CODE set_auto_white_balance(AwbParam *param);
        E_RESULT_CODE set_dnn_crop_size(CropParam *param);
        E_RESULT_CODE set_raw_crop_size(CropParam *param);
        E_RESULT_CODE set_power_save_mode(PsvParam *param);
        E_RESULT_CODE set_reg_value(RegSetting *param);
        E_RESULT_CODE set_init_param(uint8_t raw_full_size_ena, uint8_t frame_rate, uint8_t binning_opt);
        /* get function */
        E_RESULT_CODE get_framerate(FrameRateParam *param);
        E_RESULT_CODE get_dnn_image_size(ImageParam *param);
        E_RESULT_CODE get_raw_image_size(ImageParam *param);
        E_RESULT_CODE get_auto_exposure(AeParam *param);
        E_RESULT_CODE get_auto_white_balance(AwbParam *param);
        E_RESULT_CODE get_dnn_crop_size(CropParam *param);
        E_RESULT_CODE get_raw_crop_size(CropParam *param);
        E_RESULT_CODE get_power_save_mode(PsvParam *param);
        E_RESULT_CODE get_isp_support(IspSptParam *param);

    } /* namespace control */
} /* namespace imx500 */

#endif /* SENSOR_PROPERTY_ */
