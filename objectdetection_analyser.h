/*
 * Copyright 2017-2018 Sony Semiconductor Solutions Corporation.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
 * Solutions Corporation.
 * No part of this file may be copied, modified, sold, and distributed in any
 * form or by any means without prior explicit permission in writing from
 * Sony Semiconductor Solutions Corporation.
 *
 */

#ifndef OBJETDETECTION_ANALYSER_H
#define OBJECTDETECTION_ANALYSER_H

#include "output_tensor_parser.h"
#include "objectdetection_interface.h"

#define INPUT_TENSOR_WIDTH 300
#define INPUT_TENSOR_HEIGHT 300

 /**
 * SSDMobilenetV1 Analyser Namespace
 */
namespace imx500 {
	namespace objectdetection_analyser {

		struct Bbox {
			uint16_t x_min;
			uint16_t y_min;
			uint16_t x_max;
			uint16_t y_max;
		};

		struct SSDMobilenetV1Data {
			uint8_t numOfDetections = 0;
			std::vector<Bbox> v_bbox;
			std::vector<float> v_scores;
			std::vector<uint8_t> v_classes;
		};

		int32_t analyseObjectDetectionOutput(const struct imx500::objectdetection_interface::ObjectDetectionOutputTensor outTensor, struct SSDMobilenetV1Data* outputSSDMobilenetData, uint16_t numOfScores);

	} // namespace objectdetection_analyser
}
#endif //OBJECT_DETECTION_ANALYSER
