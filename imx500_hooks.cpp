/*
* Copyright 2018-2019 Sony Semiconductor Solutions Corporation.
*
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Sony Semiconductor
* Solutions Corporation.
* No part of this file may be copied, modified, sold, and distributed in any
* form or by any means without prior explicit permission in writing from
* Sony Semiconductor Solutions Corporation.
*
*/

#include <stdint.h>
#include <stdio.h>
#include <string>
#include <string.h>
#include <cstring>
#include <iostream>
#include "imx500_hooks.h"
#include "imx500_hooks_config.h"
#include "sensor_control.h"
#include "sensor_property.h"

struct hooks_config HooksConfig = {
	"CustomNet",
	0,//Don't save raw tensor files
	"./rawTensorOutput",//Output path for raw .bin files
	2,//Bitmap image output
	0,//Don't do flash update
	30//Frame Rate
};

namespace IMX500Hooks {

    std::vector<trained_object> TrainedObjects;
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Callback
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    Hooks* Hooks::instance = nullptr;
    Hooks::Hooks(){ Hooks::instance = this; }
    void Hooks::onFrame(ImageData imgData) { }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Public Methods
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    void setConfig(struct Config config) {
        switch (config.displayImageExtension) {
            case E_JPEG: HooksConfig.displayImageExtension = 0; break;
            case E_PNG: HooksConfig.displayImageExtension = 1; break;
            case E_BMP: HooksConfig.displayImageExtension = 2; break;
            default: break;
        }
        memset(HooksConfig.computerVisionModelsFolder, 0, sizeof(HooksConfig.computerVisionModelsFolder));
        strncpy(HooksConfig.computerVisionModelsFolder, config.computerVisionModelsFolder, sizeof(HooksConfig.computerVisionModelsFolder) - 1);
        HooksConfig.bSaveRawTensorOutputFiles = config.bSaveRawTensorOutputFiles ? 1 : 0;
        memset(HooksConfig.rawTensorSavePath, 0, sizeof(HooksConfig.rawTensorSavePath));
        strncpy(HooksConfig.rawTensorSavePath, config.rawTensorSavePath, sizeof(HooksConfig.rawTensorSavePath) - 1);
        HooksConfig.bDoFlashUpdate = config.bDoFlashUpdate ? 1 : 0;
        HooksConfig.frameRate = config.frameRate;
    }
    void getCurrentDetectionCropSizeAndPosition(uint32_t* leftEdgePixelPosition, uint32_t *topEdgePixelPosition, uint32_t *width, uint32_t *height) {
        imx500::property::CropParam param;
        imx500::property::get_dnn_crop_size(&param);
        *leftEdgePixelPosition = param.m_left;
        *topEdgePixelPosition = param.m_top;
        *width = param.m_width;
        *height = param.m_height;
    }
    void adjustDetectionCropSizeAndPosition(uint32_t leftEdgePixelPosition, uint32_t topEdgePixelPosition, uint32_t width, uint32_t height) {
        imx500::property::CropParam param { leftEdgePixelPosition, topEdgePixelPosition, width, height };
        imx500::property::set_dnn_crop_size(&param);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    // Properties
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    bool getConfig_bSaveRawTensorOutputFiles() {
        return HooksConfig.bSaveRawTensorOutputFiles == 1;
    }
    E_DISPLAY_IMAGE_EXTENSION getConfig_displayImageExtension() {
        E_DISPLAY_IMAGE_EXTENSION ret = E_BMP;
        switch (HooksConfig.displayImageExtension) {
            case 0: ret = E_JPEG; break;
            case 1: ret = E_PNG; break;
            case 2: ret = E_BMP; break;
            default: break;
        }
        return ret;
    }
}
